-- Your SQL goes here
CREATE TABLE IF NOT EXISTS member_queue (
    id SERIAL PRIMARY KEY,
    member_id SERIAL UNIQUE,
    m_action varchar(100) NOT NULL,

    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    changed TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

SELECT diesel_manage_updated_at('member_queue');