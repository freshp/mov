-- Your SQL goes here-- This file was automatically created by Diesel to setup helper functions
-- and other internal bookkeeping. This file is safe to edit, any future
-- changes will be added to existing projects as new migrations.




-- Sets up a trigger for the given table to automatically set a column called
-- `updated_at` whenever the row is modified (unless `updated_at` was included
-- in the modified columns)
--
-- # Example
--
-- ```sql
-- CREATE TABLE users (id SERIAL PRIMARY KEY, updated_at TIMESTAMP NOT NULL DEFAULT NOW());
--
-- SELECT diesel_manage_updated_at('users');
-- ```
CREATE OR REPLACE FUNCTION diesel_manage_updated_at(_tbl regclass) RETURNS VOID AS $$
BEGIN
    EXECUTE format('CREATE TRIGGER set_updated_at BEFORE UPDATE ON %s
                    FOR EACH ROW EXECUTE PROCEDURE diesel_set_updated_at()', _tbl);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION diesel_set_updated_at() RETURNS trigger AS $$
BEGIN
    IF (
        NEW IS DISTINCT FROM OLD AND
        NEW.changed IS NOT DISTINCT FROM OLD.changed
    ) THEN
        NEW.changed := current_timestamp;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TABLE IF NOT EXISTS address (
    id SERIAL PRIMARY KEY,
    street varchar(1000),
    zip varchar(10),
    city varchar(1000),
    country varchar(100),

    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    changed TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

SELECT diesel_manage_updated_at('address');

CREATE TABLE IF NOT EXISTS roles (
    id SERIAL PRIMARY KEY,
    name varchar(100) NOT NULL,

    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    changed TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

SELECT diesel_manage_updated_at('roles');

CREATE TABLE IF NOT EXISTS newsletter (
    id SERIAL PRIMARY KEY,
    name varchar(100) NOT NULL,

    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    changed TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

SELECT diesel_manage_updated_at('newsletter');

CREATE TABLE IF NOT EXISTS mov_user (
    id SERIAL PRIMARY KEY,
    email varchar(100) NOT NULL,
    username varchar(100) NOT NULL,
    firstname varchar(100) NOT NULL,
    lastname varchar(100) NOT NULL,
    title varchar(100),
    gender varchar(100) NOT NULL,
    password varchar(1000) NOT NULL,
    salt varchar(1000) NOT NULL,

    address_id INTEGER NOT NULL REFERENCES address(id),
    role_id INTEGER NOT NULL REFERENCES roles(id),

    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    changed TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);


SELECT diesel_manage_updated_at('mov_user');

CREATE TABLE IF NOT EXISTS member (
    id SERIAL PRIMARY KEY,
    member_id varchar(100) UNIQUE,
    title varchar(100),
    email varchar(100) NOT NULL UNIQUE,
    firstname varchar(100) NOT NULL,
    lastname varchar(100) NOT NULL,
    gender varchar(100) NOT NULL,
    member_since TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    membership_due TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    changed_membership TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    active_email boolean DEFAULT false,
    reminder_sent boolean DEFAULT false,

    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    changed TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

    address_id INTEGER NOT NULL REFERENCES address (id),
    location_id INTEGER NOT NULL REFERENCES roles (id)
);

SELECT diesel_manage_updated_at('member');

CREATE TABLE IF NOT EXISTS newsletter_user (
    id SERIAL PRIMARY KEY,
    newsletter_id INTEGER NOT NULL REFERENCES newsletter (id),
    member_id INTEGER NOT NULL REFERENCES member (id),

    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    changed TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

SELECT diesel_manage_updated_at('newsletter_user');

CREATE TABLE IF NOT EXISTS settings (
    id SERIAL PRIMARY KEY,
    email_username VARCHAR(1000),
    email_password VARCHAR(1000),
    welcome_subject TEXT,
    welcome_text TEXT,
    reminder_subject TEXT,
    reminder_text TEXT,
    extend_subject TEXT,
    extend_text TEXT,

    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    changed TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

SELECT diesel_manage_updated_at('settings');