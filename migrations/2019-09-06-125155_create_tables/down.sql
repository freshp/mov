-- This file should undo anything in `up.sql`
DROP TABLE IF EXISTS newsletter_user cascade;
DROP TABLE member;
DROP TABLE mov_user;
DROP TABLE newsletter;
DROP TABLE IF EXISTS roles cascade;
DROP TABLE IF EXISTS address cascade;
DROP TABLE IF EXISTS settings cascade;

DROP FUNCTION IF EXISTS diesel_manage_updated_at(_tbl regclass);
DROP FUNCTION IF EXISTS diesel_set_updated_at();