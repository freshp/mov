-- Your SQL goes here
CREATE TABLE IF NOT EXISTS oauth (
    id SERIAL PRIMARY KEY,
    token varchar(100) NOT NULL,

    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    changed TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

SELECT diesel_manage_updated_at('oauth');