use rocket_contrib::json::{Json};
use rocket::Request;
use crate::models::not_found::{NotFound};
use crate::models::not_authorized::{NotAuthorized};
use crate::models::internal_server_error::{InternalServerError};

#[catch(404)]
pub fn not_found(_req: &Request) ->  Json<NotFound>{ 
    Json(NotFound {
        message: String::from("Nothing found on this path pal!")
    })
}

#[catch(401)]
pub fn not_authorized(_req: &Request) -> Json<NotAuthorized> {
    Json(NotAuthorized {
        message: String::from("U need a to pass a valid token!")
    })
}

#[catch(500)]
pub fn internal_server_error(_req: &Request) -> Json<InternalServerError> {
    Json(InternalServerError {
        message: String::from("Something went wrong on the rocket!")
    })
}