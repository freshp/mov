use rocket::{self, http::Status};
use rocket_contrib::json::{Json};
use log::{info};

use crate::models::settings::{Settings, InsertedSettings};
use crate::connection::DbConn;
use crate::guards::token::UserInfo;
use crate::repositories::settings_repository::{SettingsRepository};


#[post("/settings", data="<settings>")]
pub fn set_settings(settings: Json<InsertedSettings>, conn: DbConn, _api: UserInfo) -> Result<Json<Settings>, Status> { 
    
    
    match SettingsRepository::get_settings(&conn) {
        Ok(s) => {
            println!("{}", s.extend_text.unwrap());
            //update
            match SettingsRepository::update_settings(&settings.0, &conn) {
                Ok(s) => return Ok(Json(s)),
                Err(err) => {
                    info!("{}", err);
                    return Err(Status::InternalServerError)
                }
            }
        },
        Err(_) => {

            match SettingsRepository::create_settings(&settings.0, &conn) {
                Ok(s) => return Ok(Json(s)),
                Err(err) => {
                    info!("{}", err);
                    return Err(Status::InternalServerError)
                }
            }
        }
    }
}

#[get("/settings")]
pub fn get_settings(conn: DbConn, _api: UserInfo) -> Result<Json<Settings>, Status> {
    match SettingsRepository::get_settings(&conn) {
        Ok(settings) => Ok(Json(settings)),
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

