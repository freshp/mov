use rocket::{self, http::Status};

use rocket_contrib::json::{Json};
use log::{info};

use crate::models::member_queue::{MemberQueue};
use crate::models::success::{Success};
use crate::connection::DbConn;
use crate::guards::token::UserInfo;
use crate::repositories::member_queue_repository::{ MemberQueueRepository };

#[delete("/m_queue/<id>")]
pub fn delete_element_from_member_queue(id: i32, conn: DbConn, _api: UserInfo) -> Result<Json<Success>, Status> {
    match MemberQueueRepository::delete_element_from_queue(id, &conn) {
        Ok(size) => {
            if size == 1 {
                Ok(Json(Success {
                    successful: true,
                    id: -1,
                    message: Some(String::from("element deleted from queue"))
                }))
            } else {
                Ok(Json(Success {
                    successful: false,
                    id: -1,
                    message: Some(String::from("there was a problem deleting element from queue."))
                }))
            }
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[get("/m_queue/list")]
pub fn get_all_elements_from_queue(conn: DbConn, _key: UserInfo) -> Result<Json<Vec<MemberQueue>>, Status> {
    match MemberQueueRepository::get_all_elements(&conn) {
        Ok(m_q) => Ok(Json(m_q)),
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}