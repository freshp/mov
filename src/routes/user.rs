use rocket::{self, http::Status};

use rocket_contrib::json::{Json};
use log::{info};

use crate::models::user::{InsertedMovUser, ResponseMovUser, RequestMovUser, UpdateMovUser, RequestUpdateMovUser, MovUser};
use crate::models::success::{Success};
use crate::models::password::{Password};
use crate::connection::DbConn;
use crate::guards::token::UserInfo;
use crate::repositories::user_repository::{ MovUserRepository };
use crate::repositories::address_repository::{ AddressRepository };
use crate::repositories::roles_repository::{ RoleRepository };
use crate::lib::hash::{ MovHash };

#[post("/user", data = "<user>")]
pub fn create_user(user: Json<RequestMovUser>, conn: DbConn, _api: UserInfo) -> Result<Json<Success>, Status> {
    let address = AddressRepository::create_address(&user.0.address, &conn);
    let role = RoleRepository::create_role(&user.0.role, &conn);

    let address_id: i32;
    let role_id: i32;

    match address {
        Ok(address) => address_id = address.id,
        Err(err) => {
            info!("{}", err);
            return Err(Status::InternalServerError)
        }
    }

    match role {
        Ok(role) => role_id = role.id,
        Err(err) => {
            info!("{}", err);
            return Err(Status::InternalServerError)
        }
    }

    let hashed: MovHash; 
    
    match MovHash::generate_hash(&user.0.password) {
        Ok(hash) => hashed = hash,
        Err(err) => {
            info!("{}", err);
            return Err(Status::InternalServerError) 
        }
    }

    match MovUserRepository::create_user(&InsertedMovUser::new(user.0, hashed.password_hash, hashed.salt, address_id, role_id), &conn) {
        Ok(user) => {
            Ok(Json(Success {
                successful: true,
                id: user.id,
                message: Some(String::from("The user was created successfuly"))
            }))
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[get("/user/<id>",)]
pub fn get_user(id: i32, conn: DbConn, _api: UserInfo) -> Result<Json<ResponseMovUser>, Status> {
    match MovUserRepository::get_by_user_id(id, &conn) {
        Ok(user) => {
             Ok(Json(user))
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[put("/user/<id>", data = "<user>")]
pub fn update_user(id: i32, user: Json<RequestUpdateMovUser>, conn: DbConn, _api: UserInfo) -> Result<Json<ResponseMovUser>, Status> {
    let address = AddressRepository::create_address(&user.0.address, &conn);
    let role = RoleRepository::create_role(&user.0.role, &conn);

    let address_id: i32;
    let role_id: i32;

    match address {
        Ok(address) => address_id = address.id,
        Err(err) => {
            info!("{}", err);
            return Err(Status::InternalServerError)
        }
    }

    match role {
        Ok(role) => role_id = role.id,
        Err(err) => {
            info!("{}", err);
            return Err(Status::InternalServerError)
        }
    }

    match MovUserRepository::update_user(id, &UpdateMovUser::new(&user.0, address_id, role_id), &conn) {
        Ok(size) => {
            if size == 1 {
                match MovUserRepository::get_by_user_id(id, &conn) {
                    Ok(user) => Ok(Json(user)),
                    Err(err) => {
                        info!("{}", err);
                        Err(Status::InternalServerError)
                    }
                }
            } else {
                Err(Status::InternalServerError)
            }
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[delete("/user/<id>")]
pub fn delete_user(id: i32, conn: DbConn, _api: UserInfo) -> Result<Json<Success>, Status> {
    match MovUserRepository::delete_user(id, &conn) {
        Ok(size) => {
            if size == 1 {
                Ok(Json(Success {
                    successful: true,
                    id: -1,
                    message: Some(String::from("User deleted successfuly"))
                }))
            } else {
                Ok(Json(Success {
                    successful: false,
                    id: -1,
                    message: Some(String::from("There was a problem deleting the user"))
                }))
            }
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}


#[get("/user/list")]
pub fn list_all_users(conn: DbConn, _api: UserInfo) -> Result<Json<Vec<ResponseMovUser>>, Status> {
    match MovUserRepository::get_all_user(&conn) {
        Ok(users) => Ok(Json(users)),
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[post("/user/set/password", data ="<password>")]
pub fn set_new_password(password: Json<Password>, conn: DbConn, _api: UserInfo) -> Result<Json<Success>, Status> {
    let user: MovUser;
    
    match MovUserRepository::get_user_with_password_by_username(&_api.username.unwrap(), &conn) {
        Ok(u) => user = u,
        Err(err) => {
            info!("{}", err);
            return Err(Status::Unauthorized)
        }
    }

    let user_hash = MovHash::new(user.password, user.salt);

    match user_hash.verify_hash(&password.0.old_password) {
        Ok(_) => (),
        Err(err) => {
            info!("{}", err);
            return Err(Status::Unauthorized)
        }
    };

    let hashed: MovHash; 
    
    match MovHash::generate_hash(&(&password.0).new_password) {
        Ok(hash) => hashed = hash,
        Err(err) => {
            info!("{}", err);
            return Err(Status::InternalServerError) 
        }
    }
    
    match MovUserRepository::user_update_password(user.id, &hashed.password_hash, &hashed.salt, &conn) {
        Ok(size) => {
            if size == 1 {
                Ok(Json(Success {
                    successful: true,
                    id: -1,
                    message: Some(String::from("The users password has been changed"))
                }))
            } else {
                Ok(Json(Success {
                    successful: false,
                    id: -1,
                    message: Some(String::from("There was a problem changing the users password"))
                }))
            }
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}
