use rocket::{self, http::Status};

use rocket_contrib::json::{Json};
use log::{info};

use crate::models::address::{Address, InsertedAddress};
use crate::models::success::{Success};
use crate::connection::DbConn;
use crate::guards::token::UserInfo;
use crate::repositories::address_repository::{ AddressRepository };

#[post("/address", data="<address>")]
pub fn create_address(address: Json<InsertedAddress>, conn: DbConn, _api: UserInfo) -> Result<Json<Success>, Status> {
    match AddressRepository::create_address(&address, &conn) {
        Ok(address) => {
            Ok(Json(Success {
                successful: true,
                id: address.id,
                message: Some(String::from("address created!"))
            }))
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[get("/address/<id>")]
pub fn get_address(id: i32, conn: DbConn, _api: UserInfo) -> Result<Json<Address>, Status> {
    match AddressRepository::get_address_by_id(id, &conn) {
        Ok(address) => Ok(Json(address)),
        Err(err) => {
                info!("{}", err);
                Err(Status::InternalServerError)
            }
    }
}

#[put("/address/<id>", data = "<address>")]
pub fn update_address(id: i32, address: Json<InsertedAddress>, conn: DbConn, _api: UserInfo) -> Result<Json<Address>, Status> {
    match AddressRepository::update_address(id, &address.0, &conn) {
        Ok(size) => {
            if size == 1 {
                match AddressRepository::get_address_by_id(id, &conn) {
                    Ok(address) => Ok(Json(address)),
                    Err(_) => Err(Status::InternalServerError)
                }
            } else {
                Err(Status::InternalServerError)
            }
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[delete("/address/<id>")]
pub fn delete_address(id: i32, conn: DbConn, _api: UserInfo) -> Result<Json<Success>, Status> {
    match AddressRepository::delete_address(id, &conn) {
        Ok(size) => {
            if size == 1 {
                Ok(Json(Success {
                    successful: true,
                    id: -1,
                    message: Some(String::from("address deleted"))
                }))
            } else {
                Ok(Json(Success {
                    successful: false,
                    id: -1,
                    message: Some(String::from("there was a problem deleting the address."))
                }))
            }
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[get("/address/list")]
pub fn get_all_addresses(conn: DbConn, _key: UserInfo) -> Result<Json<Vec<Address>>, Status> {
    match AddressRepository::get_all_addresses(&conn) {
        Ok(address) => Ok(Json(address)),
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}