use std::io;

use rocket::{self, http::Status};
use rocket_contrib::json::{Json, JsonValue};
use rocket::response::{NamedFile};
use crypto::sha2::Sha256;
use crate::guards::token::UserInfo;
use log::{info};
use chrono::{ Utc };

use crate::guards::token::jwt::{
    Header,
    Registered,
    Token,
};
use crate::repositories::user_repository::{ MovUserRepository };
use crate::repositories::roles_repository::{ RoleRepository };
use crate::connection::DbConn;
use crate::lib::hash::{ MovHash };


#[head("/")]
pub fn head_index() -> Result<Json<JsonValue>, Status> {
    Ok(Json(json!({})))
}

#[get("/")]
pub fn index() -> io::Result<NamedFile> {
    NamedFile::open("static/index.html")
}

#[derive(Serialize, Deserialize)]
pub struct LoginRequest {
    pub username: String,
    pub password: String
}

#[get("/check_logged_in")]
pub fn check_logged_in(_api: UserInfo) -> Result<Json<JsonValue>, Status> {
    Ok(Json(json!({})))
}

#[post("/login", data="<login>")]
pub fn login(login: Json<LoginRequest>, connection: DbConn) -> Result<Json<JsonValue>, Status> {
    let header: Header = Default::default();
    let username = login.username.to_string();
    let password = login.password.to_string();

    match MovUserRepository::get_by_username(username, &connection) {
            Ok(user) => {

                let user_hash = MovHash::new(user.password, user.salt);
                match user_hash.verify_hash(&password) {
                    Ok(_) => (),
                    Err(err) => {
                            info!("{:?}", err);
                            return Err(Status::Unauthorized)
                        }
                };

                let one_week = Utc::now().naive_utc();
                match RoleRepository::get_role_by_id(user.role_id, &connection) {
                    Ok(role) => {
                        let claims = Registered {
                            iss: Some(String::from("mov_sh")),
                            sub: Some(user.username),
                            aud: Some(role.name.clone()),
                            exp: Some(one_week.timestamp() as u64),

                            ..Default::default()
                        };
                        let token = Token::new(header, claims);
                        let role_name = role.name;
                        token.signed(b"secret_key", Sha256::new())
                            .map(|message| Json(json!({ "success": true, "token": message, "role": role_name })))
                            .map_err(|_| Status::InternalServerError)
                    },
                    Err(err) => {
                        info!("{}", err);
                        Err(Status::Unauthorized)
                    }
                }
            },
            Err(err) => {
                info!("{}", err);
                Err(Status::Unauthorized)
            }
        }


}