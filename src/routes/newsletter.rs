use rocket::{self, http::Status};
use rocket_contrib::json::{Json};
use log::{info};

use crate::models::newsletter::{Newsletter, InsertedNewsletter};
use crate::models::success::{Success};
use crate::connection::DbConn;
use crate::guards::token::UserInfo;
use crate::repositories::newsletter_repository::{NewsletterRepository};


#[post("/newsletter", data="<newsletter>")]
pub fn create_newsletter(conn: DbConn, newsletter: Json<InsertedNewsletter>, _key: UserInfo) -> Result<Json<Success>, Status> {
    match NewsletterRepository::create_newsletter(&newsletter, &conn) {
        Ok(newsletter) => {
            Ok(Json(Success {
                successful: true,
                id: newsletter.id,
                message: Some(String::from(""))
            }))
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[get("/newsletter/<id>")]
pub fn get_newsletter(id: i32, conn: DbConn, _api: UserInfo) -> Result<Json<Newsletter>, Status> {
    match NewsletterRepository::get_newsletter_by_id(id, &conn) {
        Ok(newsletter) => Ok(Json(newsletter)),
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[put("/newsletter/<id>", data = "<newsletter>")]
pub fn update_newsletter(id: i32, newsletter: Json<InsertedNewsletter>, conn: DbConn, _api: UserInfo) -> Result<Json<Newsletter>, Status> {
    match NewsletterRepository::update_newsletter(id, &newsletter.0, &conn) {
        Ok(size) => {
            if size == 1 {
                match NewsletterRepository::get_newsletter_by_id(id, &conn) {
                    Ok(newsletter) => Ok(Json(newsletter)),
                    Err(_) => Err(Status::InternalServerError)
                }
            } else {
                Err(Status::InternalServerError)
            }
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[delete("/newsletter/<id>")]
pub fn delete_newsletter(id: i32, conn: DbConn, _api: UserInfo) -> Result<Json<Success>, Status> {
    match NewsletterRepository::delete_newsletter(id, &conn) {
        Ok(size) => {
            if size == 1 {
                Ok(Json(Success {
                    successful: true,
                    id: -1,
                    message: Some(String::from("The newsletter was deleted successfully"))
                }))
            } else {
                Ok(Json(Success {
                    successful: false,
                    id: -1,
                    message: Some(String::from("There was a problem deleting the newsletter"))
                }))
            }
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[get("/newsletter/list")]
pub fn get_all_newsletter(conn: DbConn, _key: UserInfo) -> Result<Json<Vec<Newsletter>>, Status> {
    match NewsletterRepository::get_all_newsletters(&conn) {
        Ok(users) => Ok(Json(users)),
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[get("/newsletter/subscribed/<name>")]
pub fn get_all_that_are_subscribed_to_newsletter(name: String, conn: DbConn, _key: UserInfo) -> Result<Json<Vec<String>>, Status> {
    match NewsletterRepository::get_all_members_that_have_newsletter(&name, &conn) {
        Ok(res) => Ok(Json(res)),
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }   
}