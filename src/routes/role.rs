use rocket::{self, http::Status};
use rocket_contrib::json::{Json};
use log::{info};

use crate::models::roles::{Role, InsertedRole};
use crate::models::success::{Success};
use crate::connection::DbConn;
use crate::guards::token::UserInfo;
use crate::repositories::roles_repository::{ RoleRepository };


#[post("/roles", data="<role>")]
pub fn create_role(role: Json<InsertedRole>, conn: DbConn, _api: UserInfo) ->Result<Json<Success>, Status> { 

    let stat = match _api.role {
        Some(role) => {
            if role != "admin" {
                Status::Unauthorized
            } else {
                Status::Ok
            }
        }
        None => {
            Status::InternalServerError
        }
    };

    if stat != Status::Ok {
        return Err(stat);
    }

    match RoleRepository::create_role(&role, &conn) {
        Ok(role) => {
            Ok(Json(Success {
                successful: true,
                id: role.id,
                message: Some(String::from("The role was created successfuly"))
            }))
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[get("/roles/<id>")]
pub fn get_role(id: i32, conn: DbConn, _api: UserInfo) -> Result<Json<Role>, Status> {
    match RoleRepository::get_role_by_id(id, &conn) {
        Ok(role) => {
            if role.name != *(&_api.role).as_ref().unwrap() && *(&_api.role).as_ref().unwrap() != "admin" {
                info!("Not authorized");
                return Err(Status::Unauthorized);
            }

            return Ok(Json(role));
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[put("/roles/<id>", data = "<role>")]
pub fn update_role(id: i32, role: Json<InsertedRole>, conn: DbConn, _api: UserInfo) -> Result<Json<Role>, Status> {

    let api = &_api.role;

    let stat = match &_api.role {
        Some(r) => {
            if r != "admin" && &role.0.name != api.as_ref().unwrap() {
                Status::Unauthorized
            } else {
                Status::Ok
            }
        }
        None => {
            Status::InternalServerError
        }
    };

    if stat != Status::Ok {
        return Err(stat);
    }

    match RoleRepository::update_role(id, &role.0, &conn) {
        Ok(size) => {
            if size == 1 {
                match RoleRepository::get_role_by_id(id, &conn) {
                    Ok(role) => {
                            Ok(Json(role))
                        },
                    Err(err) => {
                        info!("{}", err);
                        Err(Status::InternalServerError)
                    }
                }
            } else {
                Err(Status::InternalServerError)
            }
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[delete("/roles/<id>")]
pub fn delete_role(id: i32, conn: DbConn, _api: UserInfo) -> Result<Json<Success>, Status> {
    match RoleRepository::delete_role(id, &conn) {
        Ok(size) => {
            if size == 1 {
                Ok(Json(Success {
                    successful: true,
                    id: -1,
                    message: Some(String::from("The role was deleted successfuly"))
                }))
            } else {
                Ok(Json(Success {
                    successful: false,
                    id: -1,
                    message: Some(String::from("There was a problem deleting the role"))
                }))
            }
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[get("/roles/list")]
pub fn get_all_roles(conn: DbConn, _api: UserInfo) -> Result<Json<Vec<Role>>, Status> {
     match RoleRepository::get_all_roles(&conn) {
        Ok(users) => Ok(Json(users)),
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[get("/roles/subscribed/<name>")]
pub fn get_all_that_have_that_role(name: String, conn: DbConn, _key: UserInfo) -> Result<Json<Vec<String>>, Status> {
    match RoleRepository::get_all_members_email_from_that_role(&name, &conn) {
        Ok(res) => Ok(Json(res)),
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }   
}

