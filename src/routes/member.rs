use rocket::{self, http::Status};
use rocket_contrib::json::{Json};
use log::{info, warn};

use crate::models::member::{InsertedMember, ResponseMember, RequestMember, UpdateMember, RequestUpdateMember, CheckMember};
use crate::models::newsletter::{ InsertedNewsletter, AddNewsletter };
use crate::models::success::{Success};
use crate::connection::DbConn;
use crate::guards::token::UserInfo;
use crate::repositories::member_repository::{MemberRepository};
use crate::repositories::address_repository::{ AddressRepository };
use crate::repositories::roles_repository::{ RoleRepository };
use crate::repositories::settings_repository::{ SettingsRepository };
use crate::lib::mail::{MovMail};
use crate::lib::create_pdf;
use crate::lib::create_pass;
use chrono::{ Duration, Utc };


#[post("/member/<send>", data="<member>")]
pub fn create_member(conn: DbConn, send: bool, member: Json<RequestMember>, _key: UserInfo) -> Result<Json<Success>, Status> {
    let address = AddressRepository::create_address(&member.0.address, &conn);
    let location = RoleRepository::create_role(&member.0.location, &conn);

    let address_id: i32;
    let location_id: i32;
    let location_name: String;

    match address {
        Ok(address) => address_id = address.id,
        Err(err) => {
            info!("{}", err);
            return Err(Status::InternalServerError)
        }
    }

    match location {
        Ok(location) => {
            location_id = location.id;
            location_name = location.name.clone();
        },
        Err(err) => {
            info!("{}", err);
            return Err(Status::InternalServerError);
        }
    }


    match MemberRepository::create_member(&InsertedMember::new(&member, address_id, location_id), &conn) {
        Ok(mem) => {
            let email: String;
            let password: String;
            let subject: String;
            let body: String;

            if mem.membership_due < Utc::now().naive_utc() || !send {
                return Ok(Json(Success {
                    successful: true,   
                    id: mem.id,
                    message: Some(String::from("Members was created and no welcome email was sent"))
                }));
            }

            match SettingsRepository::get_settings(&conn) {
                Ok(settings) => {
                    email = settings.email_username.unwrap_or(String::new());
                    password = settings.email_password.unwrap_or(String::new());
                    subject = settings.welcome_subject.unwrap_or(String::new());
                    body = settings.welcome_text.unwrap_or(String::new());

                    let mail = MovMail {
                        username: email.to_string(),
                        password: password.to_string()
                    };
                    
                    let f_path;
                    match create_pdf::create_pdf((&mem.member_id).as_ref().unwrap()) {
                        Ok(filepath) => f_path = filepath,
                        Err(err) => {
                            warn!("Could not create pdf settings: {}", err);
                            return Err(Status::InternalServerError);
                        }
                    }

                    let mut name = String::from(&mem.title.unwrap_or(String::from("")));
                    name.push_str(" ");
                    name.push_str(&mem.firstname);
                    name.push_str(" ");
                    name.push_str(&mem.lastname);
                    let pass_path;
                    match create_pass::create_pass((&mem.member_id).as_ref().unwrap(), &name, &mem.membership_due.date().format("%d. %m. %Y").to_string()) {
                        Ok(filepath) => pass_path = filepath,
                        Err(err) => {
                            warn!("Could not create pass: {}", err);
                            return Err(Status::InternalServerError);
                        }
                    }

                    match mail.send_mail(mem.email.to_string().as_ref(), subject.to_string().as_ref(), body.to_string().as_ref(), Some(&f_path) , Some("Membership_sh.pdf"), Some("assets/pdfs/Privacy.pdf"), Some("Privacy.pdf"), Some(&pass_path), Some("asus.sh.pkpass"), &location_name, &conn) {
                        Ok(_) => {
                            match MemberRepository::set_email_active(mem.id, true, &conn) {
                                Ok(_) => (),
                                Err(err) => warn!("Could not set email {} active: {}", mem.email, err)
                            }
                            Ok(Json(Success {
                                successful: true,
                                id: mem.id,
                                message: Some(String::from("Members was created and welcome email was sent"))
                            }))
                        }
                        Err(err) => {
                            warn!("Could not send welcome email: {}", err);
                            match MemberRepository::set_email_active(mem.id, false, &conn) {
                                Ok(_) => (),
                                Err(err) => warn!("Could not set email {} active: {}", mem.email, err)
                            }

                            Ok(Json(Success {
                                successful: false,
                                id: mem.id,
                                message: Some(String::from("Member was created but email was not send"))
                            }))
                        }
                    }
                },
                Err(err) => {
                    warn!("Could not retrive email settings: {}", err);
                    Err(Status::InternalServerError)
                }
            }
        },
        Err(err) => {
            info!("{}", err);
            //if err.message == "duplicate key value violates unique constraint \"member_email_key\"" {
            Err(Status::from_code(501).unwrap())
            //}

            
            //Err(Status::InternalServerError)
        }
    }
}

#[post("/member/newsletter/<id>", data="<newsletter>")]
pub fn add_newsletter(id: i32, newsletter: Json<Vec<AddNewsletter>>, conn: DbConn, _api: UserInfo) -> Result<Json<Success>, Status> {
    match MemberRepository::add_newsletters(id, &newsletter.0, &conn) {
        Ok(_) => {
            Ok(Json(Success {
                successful: true,
                id: -1,
                message: Some(String::from("Newsletter have been added"))
            }))
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[delete("/member/newsletter/<id>", data="<newsletter>")]
pub fn remove_newsletter(id: i32, newsletter: Json<Vec<AddNewsletter>>, conn: DbConn, _api: UserInfo) -> Result<Json<Success>, Status> {
    match MemberRepository::delete_newsletters(id, &newsletter.0, &conn) {
        Ok(_) => {
            Ok(Json(Success {
                successful: true,
                id: -1,
                message: Some(String::from("newsletter have been removed"))
            }))
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[get("/member/<id>")]
pub fn get_member(id: i32, conn: DbConn, _api: UserInfo) -> Result<Json<ResponseMember>, Status> {
    match MemberRepository::get_member_by_id(id, &conn) {
        Ok(member) => Ok(Json(member)),
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[put("/member/<id>", data= "<member>")]
pub fn update_member(id: i32, member: Json<RequestUpdateMember>, conn: DbConn, _api: UserInfo) -> Result<Json<ResponseMember>, Status> {
     let address = AddressRepository::create_address(&member.0.address, &conn);
    let location = RoleRepository::create_role(&member.0.location, &conn);

    let address_id: i32;
    let location_id: i32;

    match address {
        Ok(address) => address_id = address.id,
        Err(err) => {     
            info!("{}", err);
            return Err(Status::InternalServerError)
        }
    }

    match location {
        Ok(location) => location_id = location.id,
        Err(err) => {
            info!("{}", err);
            return Err(Status::InternalServerError)
        }
    }

    match MemberRepository::update_member(id, &UpdateMember::new(&member.0, address_id, location_id), &conn) {
        Ok(size) => {
            if size == 1 {
                match MemberRepository::get_member_by_id(id, &conn) {
                    Ok(member) => Ok(Json(member)), //TODO: can be optimized
                    Err(err) => {
                        info!("{}", err);
                        Err(Status::InternalServerError)
                    }
                }
            } else {
                Err(Status::InternalServerError)
            }
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[delete("/member/<id>")]
pub fn delete_member(id: i32, conn: DbConn, _api: UserInfo) -> Result<Json<Success>, Status> {
    match MemberRepository::delete_member(id, &conn) {
        Ok(size) => {
            if size == 1 {
                Ok(Json(Success {
                    successful: true,
                    id: -1,
                    message: Some(String::from("members has been deleted"))
                }))
            } else {
                Ok(Json(Success {
                    successful: false,
                    id: -1,
                    message: Some(String::from("there was a problem deleting the member"))
                }))
            }
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[get("/member/list")]
pub fn get_all_members(conn: DbConn, _key: UserInfo) -> Result<Json<Vec<ResponseMember>>,Status> {
   match MemberRepository::get_all_members(&conn) {
        Ok(members) => Ok(Json(members)),
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[post("/member/extend-membership/<id>")]
pub fn extend_membership(id: i32, conn: DbConn, _key: UserInfo) -> Result<Json<Success>, Status> {
    let mem;

    match MemberRepository::get_member_by_id(id, &conn) {
        Ok(m) => {
            let mut ref_date = m.membership_due;
            if ref_date < Utc::now().naive_utc() {
                ref_date = Utc::now().naive_utc();
            }

            match MemberRepository::change_membership_date(id, &(ref_date + Duration::days(365)), &conn) {
                Ok(_) => {

                    match MemberRepository::get_member_by_id(id, &conn) {
                        Ok(member) => mem = member,
                        Err(err) => {
                            info!("{}", err);
                            return Err(Status::InternalServerError)
                        }
                    }
                    info!("Date {}\n", &mem.membership_due.date().format("%d. %m. %Y").to_string());
                    let email;
                    let password;
                    let subject;
                    let body;
                    match SettingsRepository::get_settings(&conn) {
                        Ok(settings) => {
                            email = settings.email_username.unwrap_or(String::new());
                            password = settings.email_password.unwrap_or(String::new());
                            subject = settings.extend_subject.unwrap_or(String::new());
                            body = settings.extend_text.unwrap_or(String::new());

                            let mail = MovMail {
                                username: email.to_string(),
                                password: password.to_string()
                            };

                            let f_path;
                            match create_pdf::create_pdf((&mem.member_id).as_ref().unwrap()) {
                                Ok(filepath) => f_path = filepath,
                                Err(err) => {
                                    warn!("Could not create pdf settings: {}", err);
                                    return Err(Status::InternalServerError);
                                }
                            }

                            let mut name = String::from(&mem.title.unwrap_or(String::from("")));
                            name.push_str(" ");
                            name.push_str(&mem.firstname);
                            name.push_str(" ");
                            name.push_str(&mem.lastname);
                            let pass_path;
                            match create_pass::create_pass((&mem.member_id).as_ref().unwrap(), &name, &mem.membership_due.date().format("%d. %m. %Y").to_string()) {
                                Ok(filepath) => pass_path = filepath,
                                Err(err) => {
                                    warn!("Could not create pass: {}", err);
                                    return Err(Status::InternalServerError);
                                }
                            }
                            

                            match mail.send_mail(mem.email.to_string().as_ref(), subject.to_string().as_ref(), body.to_string().as_ref(), Some(&f_path), Some("Membership_sh.pdf"), Some("assets/pdfs/Privacy.pdf"), Some("Privacy.pdf"), Some(&pass_path), Some("asus.sh.pkpass"), &mem.location.name, &conn) {
                                Ok(_) => {
                                    match MemberRepository::set_email_active(mem.id, true, &conn) {
                                        Ok(_) => (),
                                        Err(err) => warn!("Could not set email {} active: {}", mem.email, err)
                                    }
                                    Ok(Json(Success {
                                        successful: true,
                                        id: mem.id,
                                        message: Some(String::from("Members was created and extend email was sent"))
                                    }))
                                }
                                Err(err) => {
                                    warn!("Could not send extend email: {}", err);
                                    match MemberRepository::set_email_active(mem.id, false, &conn) {
                                        Ok(_) => (),
                                        Err(err) => warn!("Could not set email {} active: {}", mem.email, err)
                                    }

                                    Ok(Json(Success {
                                        successful: false,
                                        id: mem.id,
                                        message: Some(String::from("Member was created but email was not send"))
                                    }))
                                }
                            }
                        },
                        Err(err) => {
                            warn!("Could not retrive email settings: {}", err);
                            Err(Status::InternalServerError)
                        }
                    }    
                },
                Err(err) => {
                    info!("{}", err);
                    Err(Status::InternalServerError)
                }
            }
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

#[post("/member/check-membership/<id>")]
pub fn check_membership(id: String, conn: DbConn, _key: UserInfo) -> Result<Json<ResponseMember>, Status> {
    match MemberRepository::get_member_by_member_id(id, &conn) {
        Ok(member) => {
            if member.membership_due > Utc::now().naive_utc() {
                Ok(Json(member))
            } else {
                Err(Status::Unauthorized)
            }
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::Unauthorized)
        }
    }
}

#[post("/member/membership", data= "<mem>")]
pub fn check_membership_third_parties(mem: Json<CheckMember>, conn: DbConn) -> Result<Json<bool>, Status> {
    match MemberRepository::get_member_by_member_id(mem.member_id.clone(), &conn) {
        Ok(member) => {
            if member.membership_due > Utc::now().naive_utc() {
                if mem.lastname == member.lastname {
                    Ok(Json(true))
                } else {
                    Ok(Json(false))
                }
            } else {
                Ok(Json(false))
            }
        },
        Err(err) => {
            info!("{}", err);
            Ok(Json(false))
        }
    }
}

#[post("/member/resend/<id>")]
pub fn resend_welcome_email_member(id: i32, conn: DbConn, _key: UserInfo) -> Result<Json<Success>, Status> {
     match MemberRepository::get_member_by_id(id, &conn) {
        Ok(mem) => {
            let email: String;
            let password: String;
            let subject: String;
            let body: String;

            match SettingsRepository::get_settings(&conn) {
                Ok(settings) => {
                    email = settings.email_username.unwrap_or(String::new());
                    password = settings.email_password.unwrap_or(String::new());
                    subject = settings.welcome_subject.unwrap_or(String::new());
                    body = settings.welcome_text.unwrap_or(String::new());

                    let mail = MovMail {
                        username: email.to_string(),
                        password: password.to_string()
                    };
                    
                    let f_path;
                    match create_pdf::create_pdf((&mem.member_id).as_ref().unwrap()) {
                        Ok(filepath) => f_path = filepath,
                        Err(err) => {
                            warn!("Could not create pdf settings: {}", err);
                            return Err(Status::InternalServerError);
                        }
                    }

                    let mut name = String::from(&mem.title.unwrap_or(String::from("")));
                    name.push_str(" ");
                    name.push_str(&mem.firstname);
                    name.push_str(" ");
                    name.push_str(&mem.lastname);
                    let pass_path;
                    match create_pass::create_pass((&mem.member_id).as_ref().unwrap(), &name, &mem.membership_due.date().format("%d. %m. %Y").to_string()) {
                        Ok(filepath) => pass_path = filepath,
                        Err(err) => {
                            warn!("Could not create pass: {}", err);
                            return Err(Status::InternalServerError);
                        }
                    }

                    match mail.send_mail(mem.email.to_string().as_ref(), subject.to_string().as_ref(), body.to_string().as_ref(), Some(&f_path), Some("Membership_sh.pdf"), Some("assets/pdfs/Privacy.pdf"), Some("Privacy.pdf"), Some(&pass_path), Some("asus.sh.pkpass"), &mem.location.name, &conn) {
                        Ok(_) => {
                            match MemberRepository::set_email_active(mem.id, true, &conn) {
                                Ok(_) => (),
                                Err(err) => warn!("Could not set email {} active: {}", mem.email, err)
                            }
                            Ok(Json(Success {
                                successful: true,
                                id: mem.id,
                                message: Some(String::from("Members was created and welcome email was sent"))
                            }))
                        }
                        Err(err) => {
                            warn!("Could not send welcome email: {}", err);
                            match MemberRepository::set_email_active(mem.id, false, &conn) {
                                Ok(_) => (),
                                Err(err) => warn!("Could not set email {} active: {}", mem.email, err)
                            }

                            Ok(Json(Success {
                                successful: false,
                                id: mem.id,
                                message: Some(String::from("Member was created but email was not send"))
                            }))
                        }
                    }
                },
                Err(err) => {
                    warn!("Could not retrive email settings: {}", err);
                    Err(Status::InternalServerError)
                }
            }
        },
        Err(err) => {
            warn!("{}", err);
            Err(Status::InternalServerError)
        }
     }
}

#[post("/member/resend")]
pub fn resend(conn: DbConn, _key: UserInfo) -> Result<Json<Success>, Status> {
    match MemberRepository::get_all_members_with_inactive_email(&conn) {
        Ok(list) => {
            for (m, r) in list {
                if m.email.contains("default@default") {
                    info!("Email address skipped");
                    continue;
                }


                let email: String;
                let password: String;
                let subject: String;
                let body: String;

                if m.membership_due < Utc::now().naive_utc() {
                    continue;
                }

                match SettingsRepository::get_settings(&conn) {
                    Ok(settings) => {
                        email = settings.email_username.unwrap_or(String::new());
                        password = settings.email_password.unwrap_or(String::new());
                        subject = settings.welcome_subject.unwrap_or(String::new());
                        body = settings.welcome_text.unwrap_or(String::new());

                        let mail = MovMail {
                            username: email.to_string(),
                            password: password.to_string()
                        };
                        
                        match create_pdf::create_pdf((&m.member_id).as_ref().unwrap()) {
                            Ok(filepath) => {

                                let mut name = String::from(&m.title.unwrap());
                                name.push_str(&m.firstname);
                                name.push_str(&m.lastname);
                                let pass_path;
                                match create_pass::create_pass((&m.member_id).as_ref().unwrap(), &name, &m.membership_due.date().format("%d. %m. %Y").to_string()) {
                                    Ok(filepath) => pass_path = filepath,
                                    Err(err) => {
                                        warn!("Could not create pass: {}", err);
                                        return Err(Status::InternalServerError);
                                    }
                                }

                                match mail.send_mail(m.email.to_string().as_ref(), subject.to_string().as_ref(), body.to_string().as_ref(), Some(&filepath), Some("Membership_sh.pdf"), Some("assets/pdfs/Privacy.pdf"), Some("Privacy.pdf"), Some(&pass_path), Some("asus.sh.pkpass"), &r.name, &conn) {
                                    Ok(_) => {
                                        match MemberRepository::set_email_active(m.id, true, &conn) {
                                            Ok(_) => (),
                                            Err(err) => warn!("Could not set email {} active: {}", m.email, err)
                                        }
                                        continue;
                                    },
                                    Err(err) => {
                                        warn!("Could not send welcome email: {}", err);
                                        match MemberRepository::set_email_active(m.id, false, &conn) {
                                            Ok(_) => (),
                                            Err(err) => warn!("Could not set email {} active: {}", m.email, err)
                                        }

                                        continue;
                                    }
                                }
                            },
                            Err(err) => {
                                warn!("Could not create pdf settings: {}", err);
                            }
                        }
                    },
                    Err(err) => {
                        warn!("Could not retrive email settings: {}", err);
                    }
                }
            }

            Ok(Json(Success {
                id: -1,
                successful: true,
                message: Some(String::from("Succesfull resend"))
            }))
        },
        Err(err) => {
            info!("{}", err);
            Err(Status::InternalServerError)
        }
    }
}

