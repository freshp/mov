use rocket::Outcome;
use rocket::{self, http::Status};
use rocket::request::{self, Request, FromRequest};

pub extern crate crypto;
pub extern crate jwt;

use crypto::sha2::Sha256;
use chrono::{ NaiveDateTime, Duration, Utc };

use self::jwt::{
    Header,
    Registered,
    Token,
};


pub struct UserInfo {
    pub issuer: Option<String>,
    pub username: Option<String>,
    pub role: Option<String>
}

pub fn read_token(key: &str) -> Result<UserInfo, String> {
    //TODO: check for Bearer substring
    let token = Token::<Header, Registered>::parse(key)
        .map_err(|_| "Unable to parse key".to_string())?;
    if token.verify(b"secret_key", Sha256::new()) && token.claims.exp.is_some() && NaiveDateTime::from_timestamp(token.claims.exp.unwrap() as i64, 0) > (Utc::now().naive_utc() - Duration::weeks(1)) {
        Ok(UserInfo {
            issuer: token.claims.iss,
            username: token.claims.sub,
            role: token.claims.aud
        })
    } else {
        Err("Token not valid".to_string())
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for UserInfo {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<UserInfo, ()> {
        let keys: Vec<_> = request.headers().get("Authorization").collect();
        if keys.len() != 1 {
            return Outcome::Failure((Status::Unauthorized, ()));
        }

        match read_token(&(keys[0])[7..]) {
            Ok(userinfo) => Outcome::Success(userinfo),
            Err(_) => Outcome::Failure((Status::Unauthorized, ()))
        }
    }
}

impl UserInfo {
    pub fn pass_if_role(self, roles: Vec<String>) -> Result<(), Status> {
        let role: String;

        match self.role {
            Some(r) => role = r,
            None => return Err(Status::InternalServerError)
        };

        if roles.contains(&role) {
            return Ok(());
        } else {
            return Err(Status::Unauthorized);
        }
    }
}