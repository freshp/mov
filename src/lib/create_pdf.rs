use log::{info, warn};


use printpdf::{PdfDocument, Mm, Image };
use std::fs::File;
use std::io::BufWriter;
use std::fs;
use qrcode::QrCode;
use image::Luma;
use uuid::Uuid;

pub fn create_pdf(qr_code: &str) -> Result<String, String> {
    //TODO create directory

    info!("Create temp folder");
    match fs::create_dir_all("temp/mov/pdfs/") {
        Ok(_) => {
            info!("Successfull created directory")
        },
        Err(err) => {
                warn!("Error creating directory");
                return Err(format!("{}", err));
            }
    }

    info!("Create Pdf");
    let (doc, page1, layer1) = PdfDocument::new("Membership", Mm(210.0), Mm(297.0), "Layer 1");
    let current_layer = doc.get_page(page1).get_layer(layer1);

   // Encode some data into bits.

    info!("Create Qr Code");
    let code; 
    match QrCode::new(qr_code) {
        Ok(c) => code = c,
        Err(err) => return Err(format!("{}", err))
    }
    let img = code.render::<Luma<u8>>().build();

    let uuid = Uuid::new_v4();
    let mut qr_code_file = String::from("temp/mov/pdfs/");
    qr_code_file.push_str(&uuid.to_hyphenated().to_string());
    qr_code_file.push_str(".png");

    // Save the image.
    match img.save(&qr_code_file) {
        Ok(_) => info!("qrcode saved!"),
        Err(err) => return Err(format!("{}", err))
    }

   
    let img: printpdf::image::DynamicImage;
    match printpdf::image::open(&qr_code_file) {
        Ok(i) => img = i,
        Err(err) => return Err(format!("{}", err))
    }

    let header: printpdf::image::DynamicImage;
    match printpdf::image::open("assets/images/pdf-header.jpg") {
        Ok(i) => header = i,
        Err(err) => return Err(format!("{}", err))
    }

    //let font = doc.add_external_font(File::open("assets/fonts/times-new-roman.ttf").unwrap()).unwrap();

    
    let image = Image::from_dynamic_image(&img);
    image.add_to_layer(current_layer.clone(), Some(Mm(60.0)), Some(Mm(100.0)), None, Some(1.0), Some(1.0), Some(100.0));

     //content
    let header = Image::from_dynamic_image(&header);
    header.add_to_layer(current_layer.clone(), Some(Mm(00.0)), Some(Mm(180.0)), None, Some(1.0), Some(1.0), Some(100.0));

    let mut pdf_filename = String::from("temp/mov/pdfs/");
    let uuid_member = Uuid::new_v4();
    pdf_filename.push_str(&uuid_member.to_hyphenated().to_string());

    match File::create(&pdf_filename) {
        Ok(el) => {
            match &doc.save(&mut BufWriter::new(el)) {
                Ok(_) => {
                        match fs::remove_file(&qr_code_file) {
                            Ok(_) => info!("File deleted!"),
                            Err(err) => warn!("Error deleting file {}" ,err)
                        }
                        Ok(pdf_filename.to_string())
                    },
                Err(err) => Err(format!("{}", err))
            }
        }
        Err(err) => Err(format!("{}", err))
    }
}