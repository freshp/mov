use log::{info, warn};

use std::fs;
use std::str;
use uuid::Uuid;
use std::fs::{File};
use std::path::Path;
use std::process::Command;
use std::env;
use std::io::prelude::*;

pub fn create_pass(qr_code: &str, member: &str, membership: &str) -> Result<String, String> {
    info!("Create pass");

    info!("date: {}", membership);

    let properties_folder = env::var("PROPERTIES_FOLDER").expect("PROPERTIES_FOLDER must be set");
    let wallet_password = env::var("WALLET_PASSWORD").expect("WALLET_PASSWORD must be set");

    let mut properties_wallet_folder = String::new();
    properties_wallet_folder.push_str(&properties_folder);
    properties_wallet_folder.push_str("wallet/");

    info!("Copy directory");
    let paths;
    
    match fs::read_dir(&properties_wallet_folder) {
        Ok(p) => paths = p,
        Err(e) => return Err(format!("{}", e))
    }

    let uuid_folder = Uuid::new_v4();
    let mut folder = String::from("/tmp/");
    folder.push_str(&uuid_folder.to_string());
    folder.push_str("/");
    
    info!("Create tmp wallet folder");
    match fs::create_dir_all(&folder) {
        Ok(_) => {
            info!("Successfull created wallet directory")
        },
        Err(err) => {
                warn!("Error creating wallet directory");
                return Err(format!("{}", err));
            }
    }

    info!("Copy to tmp folder");
    for path in paths {
        let mut tmp_path = (&folder).clone();
        tmp_path.push_str(&path.as_ref().unwrap().file_name().into_string().unwrap());

        if &path.as_ref().unwrap().file_name().into_string().unwrap() != "pass.json" {
            match fs::copy(&path.as_ref().unwrap().path(), &tmp_path) {
                Ok (f) => info!("Copied file {:?}", f),
                Err(err) => return Err(format!("Error while coping {:?}", err))
            }
        }
    }

    info!("Write pass.json to file");
    let mut pass_path = String::new();
    pass_path.push_str(&properties_wallet_folder);
    pass_path.push_str("pass.json");

    let mut pass;
    
    
    match fs::read_to_string(pass_path) {
        Ok(p) => pass = p,
        Err(e) => return Err(format!("{}", e))
    }
    
    
    
    pass = str::replace(&pass, "{qr-code}", qr_code);
    pass = str::replace(&pass, "{member}", member);
    pass = str::replace(&pass, "{membership}", membership);

    let mut fname = (&folder).clone();
    fname.push_str("pass.json");

    let p = Path::new(&fname);

    let mut f = match File::create(&p) {
        Ok(f) => f,
        Err(e) => return Err(format!("file error: {}", e)),
    };

    match f.write_all(pass.as_bytes()) {
        Ok(p) => info!("Wrote pass.json file {:?}", p),
        Err(e) => return Err(format!("Could not write pass.json file {:?}", e))
    }

    info!("Create pass with cmd-tool");
    let mut pass_name = String::from("/tmp/");
    pass_name.push_str(&Uuid::new_v4().to_string());
    pass_name.push_str(".pkpass");

    let mut cmd = String::from("");
    cmd.push_str("pass_wallet=$(echo \"");
    cmd.push_str(&wallet_password);
    cmd.push_str("\") && apple_wallet_cmd --name ");
    cmd.push_str(&pass_name);
    cmd.push_str(" --p $pass_wallet --f ");
    cmd.push_str(&folder);

    info!("{}", cmd);
    Command::new("bash")
            .arg("-c")
            .arg(cmd)
            .output()
            .expect("failed to execute process");

    Ok(String::from(pass_name))
}