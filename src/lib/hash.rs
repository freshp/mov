use data_encoding::HEXUPPER;
use ring::rand::SecureRandom;
use ring::{digest, pbkdf2, rand};
use log::{warn};

const CREDENTIAL_LEN: usize = digest::SHA512_OUTPUT_LEN;
const N_ITER: u32 = 100_000;

#[derive(Debug)]
pub struct MovHash {
    pub salt: String,
    pub password_hash: String
}

impl MovHash {
    pub fn new(password: String, salt: String) -> MovHash {
        MovHash {
            salt: salt,
            password_hash: password
        }
    }

    pub fn generate_hash(password: &String) -> Result<Self, String> {
        let rng = rand::SystemRandom::new();

        let mut salt = [0u8; CREDENTIAL_LEN];
        match rng.fill(&mut salt) {
            Ok(_) => (),
            Err(err) => {
                warn!("{}", err);
                return Err(String::from("Error while generating hash!"))
            }
        }

        let mut pbkdf2_hash = [0u8; CREDENTIAL_LEN];
        pbkdf2::derive(
            &digest::SHA512,
            N_ITER,
            &salt,
            password.as_bytes(),
            &mut pbkdf2_hash,
        );

        Ok(MovHash {
            salt: HEXUPPER.encode(&salt),
            password_hash: HEXUPPER.encode(&pbkdf2_hash)
        })
    }

    pub fn verify_hash(self, password: &String) -> Result<(), String> {
        let password_hash = HEXUPPER.decode(self.password_hash.as_bytes()).unwrap_or_else(|_| Vec::new());
        let salt = HEXUPPER.decode(self.salt.as_bytes()).unwrap_or_else(|_| Vec::new());

        if password_hash.len() == 0 || salt.len() == 0 {
            return Err(String::from("Could not verify password"));
        }

        match pbkdf2::verify(
            &digest::SHA512,
            N_ITER,
            salt.as_ref(),
            password.as_bytes(),
            password_hash.as_ref(),
        ) {
            Ok(_) => Ok(()),
            Err(err) => {
                warn!("{}", err);
                Err(String::from("Could not verify password"))
            }
        }
    }
}
