use diesel::pg::PgConnection;
use lettre::smtp::authentication::{Credentials, Mechanism};
use lettre::smtp::ConnectionReuseParameters;
use lettre::{SmtpClient, Transport};
use lettre_email::{error::Error, mime::TEXT_PLAIN, Email};
use log::{info, warn};
use regex::Regex;
use std::path::Path;

pub struct MovMail {
    pub username: String,
    pub password: String,
}

impl MovMail {
    pub fn send_mail(
        &self,
        to: &str,
        subject: &str,
        body: &str,
        file: Option<&str>,
        filename: Option<&str>,
        second_file: Option<&str>,
        second_filename: Option<&str>,
        third_file: Option<&str>,
        third_filename: Option<&str>,
        role: &str,
        conn: &PgConnection,
    ) -> Result<bool, Error> {
        let email: Result<Email, Error>;

        let reg = Regex::new(r"(\{location\})").unwrap();
        let result = reg.replace_all(body, role);

        println!("{}", result);

        info!("{}", result);

        if file.is_some() {
            email = match Email::builder()
                .to(to)
                .from((self.username.to_string(), "sh.asus"))
                .subject(subject)
                .html(result)
                .attachment_from_file(Path::new(file.unwrap()), filename, &TEXT_PLAIN)
            {
                Ok(builder) => {
                    if second_file.is_some() && second_filename.is_some() {
                        info!("{}:{}", second_file.unwrap(), second_filename.unwrap());
                        let temp;

                        match builder.attachment_from_file(
                            Path::new(second_file.unwrap()),
                            second_filename,
                            &TEXT_PLAIN,
                        ) {
                            Ok(b) => {
                                if third_file.is_some() && third_filename.is_some() {
                                    info!("{}:{}", third_file.unwrap(), third_filename.unwrap());
                                    match b.attachment_from_file(
                                        Path::new(third_file.unwrap()),
                                        third_filename,
                                        &TEXT_PLAIN,
                                    ) {
                                        Ok(tb) => temp = tb.build(),
                                        Err(error) => return Err(error),
                                    }
                                } else {
                                    temp = b.build()
                                }
                            }
                            Err(error) => return Err(error),
                        };

                        temp
                    } else {
                        builder.build()
                    }
                }
                Err(error) => return Err(error),
            }
        } else {
            email = Email::builder()
                .to(to)
                .from(self.username.to_string())
                .subject(subject)
                .html(body)
                .build();
        }
        match email {
            Ok(e) => {
                let mut mailer = SmtpClient::new_simple("smtp.gmail.com")
                    .unwrap()
                    // Set the name sent during EHLO/HELO, default is `localhost`
                    //.hello_name(ClientId::Domain("localhost".to_string()))
                    // Add credentials for authentication
                    .credentials(Credentials::new(
                        self.username.to_string(),
                        self.password.to_string(),
                    ))
                    // Enable SMTPUTF8 if the server supports it
                    .smtp_utf8(true)
                    // Configure expected authentication mechanism
                    .authentication_mechanism(Mechanism::Plain)
                    // Enable connection reuse
                    .connection_reuse(ConnectionReuseParameters::ReuseUnlimited)
                    .transport();

                let result = mailer.send(e.into());

                match result {
                    Ok(response) => {
                        info!("{}", response.code);
                        return Ok(true);
                    }
                    Err(err) => {
                        warn!("{}", err);
                        return Ok(false);
                    }
                }
            }
            Err(e) => return Err(e),
        }
    }

   /* pub async fn send_mail_gmail_api(
        &self,
        to: &str,
        subject: &str,
        body: &str,
        file: Option<&str>,
        filename: Option<&str>,
        second_file: Option<&str>,
        second_filename: Option<&str>,
        third_file: Option<&str>,
        third_filename: Option<&str>,
        role: &str,
        conn: &PgConnection,
    ) -> Result<String, Error> {
        match yup_oauth2::read_application_secret("client-auth.json").await {
            Ok(secret) => {
                match InstalledFlowAuthenticator::builder(
                    secret,
                    InstalledFlowReturnMethod::HTTPRedirect,
                )
                .build().await {
                    Ok (auth) => {
                        let mut hub = Gmail::new(hyper::Client::new(), auth);
                    },
                    Err(err) => warn!("Error creating Installed flow authenticator [{:?}]", err)
                }
            }
            Err(err) => warn!("Could not read client-auth.json file [{:?}]", err)
        }

        Ok(String::from(""))
    }*/
}
