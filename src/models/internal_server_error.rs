#[derive(Serialize, Deserialize)]
pub struct InternalServerError {
    pub message: String
}