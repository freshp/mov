#[derive(Serialize, Deserialize)]
pub struct Success {
    pub successful: bool,
    pub id: i32,
    pub message: Option<String>
}