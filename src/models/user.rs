use crate::schema::*;
use chrono::NaiveDateTime;
use crate::models::address::{ Address, InsertedAddress };
use crate::models::roles::{ Role, InsertedRole };

#[derive(Identifiable, Associations, Serialize, Deserialize, Queryable, PartialEq, Debug)]
#[table_name= "mov_user" ]
#[belongs_to(Address, foreign_key="address_id")]
#[belongs_to(Role, foreign_key="role_id")]
pub struct MovUser {
    pub id: i32,
    pub email: String,
    pub username: String,
    pub firstname: String,
    pub lastname: String,
    pub gender: String,
    pub title: Option<String>,
    pub password: String,
    pub salt: String,
    pub address_id: i32,
    pub role_id: i32,

    pub created: NaiveDateTime,
    pub changed: NaiveDateTime
}

#[derive(Deserialize, Insertable)]
#[table_name = "mov_user"]
pub struct InsertedMovUser {
    pub email: String,
    pub username: String,
    pub firstname: String,
    pub lastname: String,
    pub title: Option<String>,
    pub gender: String,
    pub password: String,
    pub salt: String,
    pub address_id: i32,
    pub role_id: i32,
}

impl InsertedMovUser {
    pub fn new(user: RequestMovUser, password: String, salt: String, address_id: i32, role_id: i32) -> InsertedMovUser {
        InsertedMovUser {
            email: user.email,
            username: user.username,
            firstname: user.firstname,
            lastname: user.lastname,
            title: user.title,
            gender: user.gender,
            password: password,
            salt: salt,
            address_id: address_id,
            role_id: role_id
        }
    }
}

#[derive(Deserialize)]
pub struct RequestMovUser {
    pub email: String,
    pub username: String,
    pub firstname: String, 
    pub lastname: String,
    pub title: Option<String>,
    pub gender: String,
    pub password: String,
    pub address: InsertedAddress,
    pub role: InsertedRole
}

#[derive(Serialize, Deserialize)]
pub struct ResponseMovUser {
    pub id: i32,
    pub email: String,
    pub username: String,
    pub firstname: String,
    pub lastname: String,
    pub gender: String,
    pub title: Option<String>,
    pub address: Address,
    pub role: Role,

    pub created: NaiveDateTime,
    pub changed: NaiveDateTime
}

#[derive(Deserialize)]
pub struct RequestUpdateMovUser {
    pub email: String,
    pub username: String,
    pub firstname: String, 
    pub lastname: String,
    pub title: Option<String>,
    pub gender: String,
    pub address: InsertedAddress,
    pub role: InsertedRole
}

#[derive(Deserialize)]
pub struct UpdateMovUser {
    pub email: String,
    pub username: String,
    pub firstname: String, 
    pub lastname: String,
    pub title: Option<String>,
    pub gender: String,
    pub address_id: i32,
    pub role_id: i32
}

impl UpdateMovUser {
    pub fn new(user: &RequestUpdateMovUser, address_id: i32, role_id: i32) -> UpdateMovUser {
        UpdateMovUser {
            email: user.email.clone(),
            username: user.username.clone(),
            firstname: user.firstname.clone(),
            lastname: user.lastname.clone(),
            gender: user.gender.clone(),
            title: user.title.clone(),
            address_id: address_id,
            role_id: role_id,
        }
    }
}

impl ResponseMovUser {
    pub fn new(user: &MovUser, address: &Address, role: &Role) -> ResponseMovUser {
        ResponseMovUser {
            id: user.id,
            email: user.email.clone(),
            username: user.username.clone(),
            firstname: user.firstname.clone(),
            lastname: user.lastname.clone(),
            gender: user.gender.clone(),
            title: user.title.clone(),
            address: Address::new(address),
            role: Role::new(role),
            created: user.created,
            changed: user.changed
        }
    }
}