#[derive(Serialize, Deserialize)]
pub struct NotAuthorized {
    pub message: String
}