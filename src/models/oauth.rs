use crate::schema::*;
use chrono::NaiveDateTime;

#[derive(Identifiable, Serialize, Deserialize, Queryable, PartialEq, Debug)]
#[table_name= "oauth" ]
pub struct OAuth {
    pub id: i32,
    pub token: String,

    pub created: NaiveDateTime,
    pub changed: NaiveDateTime

}

#[derive(Deserialize, Insertable)]
#[table_name = "oauth"]
pub struct InsertedOAuth {
    pub token: String,
}

impl OAuth {
    pub fn new(o: &OAuth) -> OAuth {
        OAuth {
            id: o.id,
            token: o.token.clone(),
            created: o.created,
            changed: o.changed
        }
    }
}