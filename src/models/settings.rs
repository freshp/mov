use crate::schema::*;
use chrono::NaiveDateTime;

#[derive(Identifiable, Serialize, Deserialize, Queryable, PartialEq, Debug)]
#[table_name = "settings"]
pub struct Settings {
    pub id: i32,
    pub email_username: Option<String>,
    pub email_password: Option<String>,
    pub welcome_subject: Option<String>,
    pub welcome_text: Option<String>,
    pub reminder_subject: Option<String>,
    pub reminder_text: Option<String>,
    pub extend_subject: Option<String>,
    pub extend_text: Option<String>,
    pub created: NaiveDateTime,
    pub changed: NaiveDateTime
}

#[derive(Deserialize, Insertable, Debug)]
#[table_name = "settings"]
pub struct InsertedSettings {
    pub email_username: Option<String>,
    pub email_password: Option<String>,
    pub welcome_subject: Option<String>,
    pub welcome_text: Option<String>,
    pub reminder_subject: Option<String>,
    pub reminder_text: Option<String>,
    pub extend_subject: Option<String>,
    pub extend_text: Option<String>
}