use crate::schema::*;
use chrono::NaiveDateTime;

#[derive(Identifiable, Associations, Serialize, Deserialize, Queryable, PartialEq, Debug)]
#[table_name = "roles"]
pub struct Role {
    pub id: i32,
    pub name: String,
    pub created: NaiveDateTime,
    pub changed: NaiveDateTime,
    pub email: String
}

#[derive(Deserialize, Insertable)]
#[table_name = "roles"]
pub struct InsertedRole {
    pub name: String,
    pub email: String
}

impl Role {
    pub fn new(role: &Role) -> Role {
        Role {
            id: role.id,
            name: role.name.clone(),
            created: role.created,
            changed: role.changed,
            email: role.email.clone()
        }
    }
}