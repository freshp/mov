use crate::schema::*;
use chrono::NaiveDateTime;

#[derive(Identifiable, Associations, Serialize, Deserialize, Queryable, PartialEq, Debug)]
#[table_name = "newsletter"]
pub struct Newsletter {
    pub id: i32,
    pub name: String,
    pub created: NaiveDateTime,
    pub changed: NaiveDateTime,
    pub email: String,
}

#[derive(Deserialize, Insertable)]
#[table_name = "newsletter"]
pub struct InsertedNewsletter {
    pub name: String,
    pub email: String
}

#[derive(Deserialize)]
pub struct AddNewsletter {
    pub name: String
}