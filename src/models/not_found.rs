#[derive(Serialize, Deserialize)]
pub struct NotFound {
    pub message: String
}