use crate::schema::*;
use uuid::Uuid;
use chrono::NaiveDateTime;
use serde::de;
use std::fmt;
use crate::models::address::{ Address, InsertedAddress };
use crate::models::roles::{ Role, InsertedRole };
use crate::models::newsletter::{ Newsletter };

struct NaiveDateTimeVisitor;

impl<'de> de::Visitor<'de> for NaiveDateTimeVisitor {
    type Value = NaiveDateTime;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "a string represents chrono::NaiveDateTime")
    }

    fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        match NaiveDateTime::parse_from_str(s, "%Y-%m-%dT%H:%M:%S.%f") {
            Ok(t) => Ok(t),
            Err(_) => Err(de::Error::invalid_value(de::Unexpected::Str(s), &self)),
        }
    }
}

fn from_timestamp<'de, D>(d: D) -> Result<NaiveDateTime, D::Error>
where
    D: de::Deserializer<'de>,
{
    d.deserialize_str(NaiveDateTimeVisitor)
}

#[derive(Identifiable, Associations, Serialize, Deserialize, Queryable, PartialEq, Debug)]
#[table_name= "member" ]
#[belongs_to(Address, foreign_key="address_id")]
#[belongs_to(Role, foreign_key="location_id")]
pub struct Member {
    pub id: i32,
    pub member_id: Option<String>,
    pub email: String,
    pub firstname: String,
    pub lastname: String,
    pub gender: String,
    pub title: Option<String>,
    pub active_email: Option<bool>,
    pub reminder_sent: Option<bool>,  

    pub address_id: i32,
    pub location_id: i32,

    pub member_since: NaiveDateTime,
    pub membership_due: NaiveDateTime,
    pub changed_membership: NaiveDateTime,
    pub created: NaiveDateTime,
    pub changed: NaiveDateTime,
    pub info: String,
}

#[derive(Deserialize, Insertable)]
#[table_name = "member"]
pub struct InsertedMember {
    pub member_id: Option<String>,
    pub email: String,
    pub firstname: String,
    pub lastname: String,
    pub gender: String,
    pub title: Option<String>,
    pub address_id: i32,
    pub location_id: i32,
    pub member_since: NaiveDateTime,
    pub membership_due: NaiveDateTime,
    pub info: String
}

#[derive(Deserialize)]
pub struct RequestMember {
    pub email: String,
    pub firstname: String,
    pub lastname: String,
    pub gender: String,
    pub title: Option<String>,
    pub address: InsertedAddress,
    pub location: InsertedRole,
    pub active_email: Option<bool>,
    #[serde(deserialize_with = "from_timestamp")]
    pub member_since: NaiveDateTime,
    #[serde(deserialize_with = "from_timestamp")]
    pub membership_due: NaiveDateTime,
    pub info: String
}

#[derive(Serialize, Deserialize)]
pub struct ResponseMember {
    pub id: i32,
    pub member_id: Option<String>,
    pub email: String,
    pub firstname: String,
    pub lastname: String,
    pub gender: String,
    pub title: Option<String>,
    pub active_email: Option<bool>,
    pub reminder_sent: Option<bool>,

    pub address: Address,
    pub location: Role,
    pub newsletters: Vec<Newsletter>,

    pub member_since: NaiveDateTime,
    pub membership_due: NaiveDateTime,
    pub changed_membership: NaiveDateTime,
    pub created: NaiveDateTime,
    pub changed: NaiveDateTime,
    pub info: String
}

#[derive(Deserialize)]
pub struct RequestUpdateMember {
    pub email: String,
    pub firstname: String,
    pub lastname: String,
    pub gender: String,
    pub title: Option<String>,
    pub address: InsertedAddress,
    pub location: InsertedRole,
    pub member_since: NaiveDateTime,
    pub membership_due: NaiveDateTime,
    pub info: String
}

#[derive(Deserialize)]
pub struct UpdateMember {
    pub email: String,
    pub firstname: String,
    pub lastname: String,
    pub gender: String,
    pub title: Option<String>,
    pub address_id: i32,
    pub location_id: i32,
    pub member_since: NaiveDateTime,
    pub membership_due: NaiveDateTime,
    pub info: String
}

#[derive(Deserialize)]
pub struct CheckMember {
    pub lastname: String,
    pub member_id: String
}


impl UpdateMember {
    pub fn new(member: &RequestUpdateMember, address_id: i32, location_id: i32) -> UpdateMember {
        UpdateMember {
            email: member.email.clone(),
            firstname: member.firstname.clone(),
            lastname: member.lastname.clone(),
            gender: member.gender.clone(),
            title: member.title.clone(),
            address_id: address_id,
            location_id: location_id,
            member_since: member.member_since,
            membership_due: member.membership_due,
            info: member.info.clone()
        }
    }
}

impl InsertedMember {
    pub fn new(member: &RequestMember, address_id: i32, location_id: i32) -> InsertedMember {
        let uuid = Uuid::new_v4();

        InsertedMember {
            member_id: Some(uuid.to_hyphenated().to_string()),
            email: member.email.clone(),
            firstname: member.firstname.clone(),
            lastname: member.lastname.clone(),
            gender: member.gender.clone(),
            title: member.title.clone(),
            address_id: address_id,
            location_id: location_id,
            member_since: member.member_since,
            membership_due: member.membership_due,
            info: member.info.clone()
        }
    }
}

impl ResponseMember {
    pub fn new_with_newsletter(member: &Member, address: &Address, role: &Role, newsletter: Vec<Newsletter>) -> ResponseMember {
        ResponseMember {
            id: member.id,
            member_id: member.member_id.clone(), 
            email: member.email.clone(),
            firstname: member.firstname.clone(),
            lastname: member.lastname.clone(),
            gender: member.gender.clone(),
            title: member.title.clone(),
            active_email: member.active_email.clone(),
            reminder_sent: member.reminder_sent.clone(),
            address: Address::new(address),
            location: Role::new(role),
            newsletters: newsletter,
            member_since: member.member_since,
            membership_due: member.membership_due,
            changed_membership: member.changed_membership,
            created: member.created,
            changed: member.changed,
            info: member.info.clone()
        }
    }
    pub fn new(member: &Member, address: &Address, role: &Role) -> ResponseMember {
        ResponseMember {
            id: member.id,
            member_id: member.member_id.clone(), 
            email: member.email.clone(),
            firstname: member.firstname.clone(),
            lastname: member.lastname.clone(),
            gender: member.gender.clone(),
            title: member.title.clone(),
            active_email: member.active_email.clone(),
            reminder_sent: member.reminder_sent.clone(),
            address: Address::new(address),
            location: Role::new(role),
            newsletters: vec![],
            member_since: member.member_since,
            membership_due: member.membership_due,
            changed_membership: member.changed_membership,
            created: member.created,
            changed: member.changed,
            info: member.info.clone()
        }
    }
}
