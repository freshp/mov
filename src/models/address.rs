use crate::schema::*;
use chrono::NaiveDateTime;

#[derive(Identifiable, Serialize, Deserialize, Queryable, PartialEq, Debug)]
#[table_name= "address" ]
pub struct Address {
    pub id: i32,
    pub street: Option<String>,
    pub zip: Option<String>,
    pub city: Option<String>,
    pub country: Option<String>,

    pub created: NaiveDateTime,
    pub changed: NaiveDateTime

}

#[derive(Deserialize, Insertable)]
#[table_name = "address"]
pub struct InsertedAddress {
    pub street: Option<String>,
    pub zip: Option<String>,
    pub city: Option<String>,
    pub country: Option<String>,
    
}

impl Address {
    pub fn new(address: &Address) -> Address {
        Address {
            id: address.id,
            street: address.street.clone(),
            zip: address.zip.clone(),
            city: address.city.clone(),
            country: address.country.clone(),
            created: address.created,
            changed: address.changed
        }
    }
}