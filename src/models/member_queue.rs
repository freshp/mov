use crate::schema::*;
use chrono::NaiveDateTime;

#[derive(Identifiable, Serialize, Deserialize, Queryable, PartialEq, Debug)]
#[table_name= "member_queue" ]
pub struct MemberQueue {
    pub id: i32,
    pub member_id: i32,
    pub m_action: String,

    pub created: NaiveDateTime,
    pub changed: NaiveDateTime

}

#[derive(Deserialize, Insertable)]
#[table_name = "member_queue"]
pub struct InsertedMemberQueue {
    pub member_id: i32,
    pub m_action: String
    
}

impl MemberQueue {
    pub fn new(m_q: &MemberQueue) -> MemberQueue {
        MemberQueue {
            id: m_q.id,
            member_id: m_q.member_id,
            m_action: m_q.m_action.clone(),
            created: m_q.created,
            changed: m_q.changed
        }
    }
}