#[derive(Serialize, Deserialize)]
pub struct Password {
    pub old_password: String,
    pub new_password: String
}