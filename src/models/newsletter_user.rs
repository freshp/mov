use crate::schema::*;
use chrono::NaiveDateTime;
use crate::models::member::{Member};
use crate::models::newsletter::{Newsletter};

#[derive(Identifiable, Associations, Serialize, Deserialize, Queryable, PartialEq, Debug)]
#[table_name= "newsletter_user" ]
#[belongs_to(Member, foreign_key="member_id")]
#[belongs_to(Newsletter, foreign_key="newsletter_id")]
pub struct NewsletterUser{
    pub id: i32,
    pub newsletter_id: i32,
    pub member_id: i32,
    pub created: NaiveDateTime,
    pub changed: NaiveDateTime
}

#[derive(Deserialize, Insertable)]
#[table_name = "newsletter_user"]
pub struct InsertedNewsletterUser {
    pub newsletter_id: i32,
    pub member_id: i32,
}