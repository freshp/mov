#![feature(proc_macro_hygiene, decl_macro)]
use bastion::prelude::*;

extern crate chrono;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate job_scheduler;

extern crate qrcode;
extern crate image;
extern crate printpdf;


extern crate lettre;
extern crate lettre_email;

extern crate ring;
extern crate data_encoding;
extern crate uuid;

use rocket::fairing::AdHoc;

pub mod cors;
pub mod models;
pub mod routes;
pub mod schema;
pub mod guards;
pub mod repositories;
pub mod lib;
pub mod catchers;

pub mod connection;
pub mod fairings;


fn main() {
    Bastion::platform();

    let message = String::from("Start Rocket");

    Bastion::spawn(|context: BastionContext, _msg: Box<dyn Message>| {
        
        rocket::ignite()
        .mount("/", routes![
            routes::main::head_index,
            routes::main::index,
            routes::main::login,
            routes::main::check_logged_in,

            routes::user::create_user,
            routes::user::get_user,
            routes::user::update_user,
            routes::user::delete_user,
            routes::user::list_all_users,
            routes::user::set_new_password,

            routes::role::create_role,
            routes::role::get_role,
            routes::role::update_role,
            routes::role::delete_role,
            routes::role::get_all_roles,
            routes::role::get_all_that_have_that_role,

            routes::newsletter::create_newsletter,
            routes::newsletter::get_newsletter,
            routes::newsletter::update_newsletter,
            routes::newsletter::delete_newsletter,
            routes::newsletter::get_all_newsletter,
            routes::newsletter::get_all_that_are_subscribed_to_newsletter,

            routes::address::create_address,
            routes::address::get_address,
            routes::address::update_address,
            routes::address::delete_address,
            routes::address::get_all_addresses,

            routes::member::create_member,
            routes::member::get_member,
            routes::member::add_newsletter,
            routes::member::remove_newsletter,
            routes::member::update_member,
            routes::member::delete_member,
            routes::member::get_all_members,
            routes::member::extend_membership,
            routes::member::check_membership,
            routes::member::check_membership_third_parties,
            routes::member::resend,
            routes::member::resend_welcome_email_member,

            routes::settings::get_settings,
            routes::settings::set_settings,

            routes::member_queue::delete_element_from_member_queue,
            routes::member_queue::get_all_elements_from_queue
            
            ])
        .mount("/resources", rocket_contrib::serve::StaticFiles::from("static/resources"))
        .manage(connection::init_pool())
        .attach(AdHoc::on_launch("Create admin user", fairings::startup::create_admin_user))
        .attach(AdHoc::on_launch("Job scheduler for memberships", fairings::job_scheduler::check_if_membership_due))
        .attach(cors::CorsFairing)
        .register(catchers![catchers::catchers::not_found, catchers::catchers::not_authorized, catchers::catchers::internal_server_error])
        .launch();
        
        context.hook();
    }, message);

    Bastion::start();
}