use rocket::Rocket;
use rocket::{self};

use diesel::pg::PgConnection;
use diesel::*;
use diesel::{self};
use dotenv::dotenv;
use log::{info, warn};
use std::env;

use crate::lib::hash::MovHash;
use crate::models::address::InsertedAddress;
use crate::models::roles::InsertedRole;
use crate::models::user::{InsertedMovUser, RequestMovUser};
use crate::models::oauth::{InsertedOAuth};
use crate::repositories::address_repository::AddressRepository;
use crate::repositories::roles_repository::RoleRepository;
use crate::repositories::user_repository::MovUserRepository;
use crate::repositories::oauth_repository::OAuthRepository;

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url).expect(&format!("Error connecting to {}", database_url))
}

pub fn create_admin_user(_rocket: &Rocket) {
    info!("Begin creating the admin user!");

    let conn = establish_connection();
    let user: RequestMovUser = RequestMovUser {
        email: String::from("test@test.test"),
        username: String::from("admin"),
        firstname: String::from("admin"),
        lastname: String::from("admin"),
        title: Some(String::from("BSc")),
        gender: String::from("F"),
        password: String::from("password"),
        address: InsertedAddress {
            street: Some(String::from("test")),
            zip: Some(String::from("test")),
            city: Some(String::from("test")),
            country: Some(String::from("It")),
        },
        role: InsertedRole {
            name: String::from("admin"),
            email: String::from("bz@asus.sh"),
        },
    };

    match MovUserRepository::get_by_username(String::from("admin"), &conn) {
        Ok(_) => return info!("Admin does already exist"),
        Err(_) => (),
    };

    let address = AddressRepository::create_address(&user.address, &conn);
    let role = RoleRepository::create_role(&user.role, &conn);

    let address_id: i32;
    let role_id: i32;

    match address {
        Ok(address) => address_id = address.id,
        Err(err) => {
            warn!(
                "An error ocurred while trying to create the address for the admin user!\n {}",
                err
            );

            return;
        }
    }

    match role {
        Ok(role) => role_id = role.id,
        Err(err) => {
            warn!(
                "An error ocurred while trying to create the role for the admin user!\n{}",
                err
            );

            return;
        }
    }

    let hashed: MovHash;
    match MovHash::generate_hash(&user.password) {
        Ok(hash) => hashed = hash,
        Err(err) => {
            println!(
                "An error ocurred while trying to hash the password for the admin user!\n {}",
                err
            );

            return;
        }
    }

    match MovUserRepository::create_user(
        &InsertedMovUser::new(user, hashed.password_hash, hashed.salt, address_id, role_id),
        &conn,
    ) {
        Ok(_) => {
            info!("Admin user has been successfuly created!");
            ()
        }
        Err(err) => {
            warn!(
                "An error ocurred while trying to create the the admin user!\n {}",
                err
            );

            ()
        }
    }
}
