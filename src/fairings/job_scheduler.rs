use rocket::{self};
use rocket::Rocket;

use diesel::*;
use diesel::{self};
use diesel::pg::PgConnection;
use dotenv::dotenv;
use std::env;
use log::{info, warn};

use std::thread;

use job_scheduler::{JobScheduler, Job};
use std::time::Duration;

use crate::lib::mail::MovMail;
use crate::repositories::member_repository::MemberRepository;
use crate::repositories::settings_repository::SettingsRepository;

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}

fn task() {
    info!("Begin checking for memberships");

            let conn = establish_connection();

            match MemberRepository::get_all_members_that_have_to_be_reminded_and_have_active_mail_address(&conn) {
                Ok(members) => {
                    let mut iter = members.into_iter();

                    let email: String;
                    let password: String;
                    let subject: String;
                    let body: String;

                    match SettingsRepository::get_settings(&conn) {
                        Ok(settings) => {
                            email = settings.email_username.unwrap_or(String::new());
                            password = settings.email_password.unwrap_or(String::new());
                            subject = settings.reminder_subject.unwrap_or(String::new());
                            body = settings.reminder_text.unwrap_or(String::new());
                        },
                        Err(err) => {
                            warn!("Could not retrive email settings: {}", err);
                            return ();
                        }
                    }

                    loop {
                        match iter.next() {
                            Some((x, role)) => {
                                let mail = MovMail {
                                    username: email.to_string(),
                                    password: password.to_string()
                                };

                                
                                
                                match mail.send_mail(&x.email.to_string(), subject.to_string().as_ref(), body.to_string().as_ref(), None, None, None, None, None, None, &role.name, &conn) {
                                    Ok(_) => {
                                        match MemberRepository::set_reminder_sent(x.id, true, &conn) {
                                            Ok(_) => (),
                                            Err(err) => warn!("Could not set email {} active: {}", x.email, err)
                                        }


                                        match MemberRepository::set_email_active(x.id, true, &conn) {
                                            Ok(_) => (),
                                            Err(err) => warn!("Could not set email {} active: {}", x.email, err)
                                        }
                                    },
                                    Err(err) => {
                                        match MemberRepository::set_reminder_sent(x.id, false, &conn) {
                                            Ok(_) => (),
                                            Err(err) => warn!("Could not set email {} active: {}", x.email, err)
                                        }

                                        match MemberRepository::set_email_active(x.id, false, &conn) {
                                            Ok(_) => (),
                                            Err(err) => warn!("Could not set email {} active: {}", x.email, err)
                                        }
                                        warn!("Failed to send email to {} ---- {}", &x.email.to_string(), err);
                                    }
                                }
                            },
                            None => break
                        }
                    }
                },
                Err(err) => {
                    warn!("Could not retrive the members to send the reminders: {}", err);
                }
            }


            info!("End checking for memberships");
}

pub fn check_if_membership_due(_rocket: &Rocket) {
    thread::spawn(move || {
        let mut sched = JobScheduler::new();

        sched.add(Job::new("0 0 6 * * Sun,Sat,Mon,Tue,Wed,Thu,Fri".parse().unwrap(), task));

        loop {
            sched.tick();

            std::thread::sleep(Duration::from_millis(15000));
        }
    });
}