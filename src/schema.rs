table! {
    address (id) {
        id -> Int4,
        street -> Nullable<Varchar>,
        zip -> Nullable<Varchar>,
        city -> Nullable<Varchar>,
        country -> Nullable<Varchar>,
        created -> Timestamp,
        changed -> Timestamp,
    }
}

table! {
    member (id) {
        id -> Int4,
        member_id -> Nullable<Varchar>,
        title -> Nullable<Varchar>,
        email -> Varchar,
        firstname -> Varchar,
        lastname -> Varchar,
        gender -> Varchar,
        member_since -> Timestamp,
        membership_due -> Timestamp,
        changed_membership -> Timestamp,
        active_email -> Nullable<Bool>,
        reminder_sent -> Nullable<Bool>,
        created -> Timestamp,
        changed -> Timestamp,
        address_id -> Int4,
        location_id -> Int4,
        info -> Text,
    }
}

table! {
    member_queue (id) {
        id -> Int4,
        member_id -> Int4,
        m_action -> Varchar,
        created -> Timestamp,
        changed -> Timestamp,
    }
}

table! {
    mov_user (id) {
        id -> Int4,
        email -> Varchar,
        username -> Varchar,
        firstname -> Varchar,
        lastname -> Varchar,
        title -> Nullable<Varchar>,
        gender -> Varchar,
        password -> Varchar,
        salt -> Varchar,
        address_id -> Int4,
        role_id -> Int4,
        created -> Timestamp,
        changed -> Timestamp,
    }
}

table! {
    newsletter (id) {
        id -> Int4,
        name -> Varchar,
        created -> Timestamp,
        changed -> Timestamp,
        email -> Varchar,
    }
}

table! {
    newsletter_user (id) {
        id -> Int4,
        newsletter_id -> Int4,
        member_id -> Int4,
        created -> Timestamp,
        changed -> Timestamp,
    }
}

table! {
    oauth (id) {
        id -> Int4,
        token -> Varchar,
        created -> Timestamp,
        changed -> Timestamp,
    }
}

table! {
    roles (id) {
        id -> Int4,
        name -> Varchar,
        created -> Timestamp,
        changed -> Timestamp,
        email -> Varchar,
    }
}

table! {
    settings (id) {
        id -> Int4,
        email_username -> Nullable<Varchar>,
        email_password -> Nullable<Varchar>,
        welcome_subject -> Nullable<Text>,
        welcome_text -> Nullable<Text>,
        reminder_subject -> Nullable<Text>,
        reminder_text -> Nullable<Text>,
        extend_subject -> Nullable<Text>,
        extend_text -> Nullable<Text>,
        created -> Timestamp,
        changed -> Timestamp,
    }
}

joinable!(member -> address (address_id));
joinable!(member -> roles (location_id));
joinable!(mov_user -> address (address_id));
joinable!(mov_user -> roles (role_id));
joinable!(newsletter_user -> member (member_id));
joinable!(newsletter_user -> newsletter (newsletter_id));

allow_tables_to_appear_in_same_query!(
    address,
    member,
    member_queue,
    mov_user,
    newsletter,
    newsletter_user,
    oauth,
    roles,
    settings,
);
