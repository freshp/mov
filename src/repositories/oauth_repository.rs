use diesel::{self, prelude::*};
use diesel::result::Error;
use log::{info};

use crate::models::oauth::{OAuth, InsertedOAuth};
use crate::schema::oauth;

pub struct OAuthRepository;

impl OAuthRepository {
    pub fn create_oauth_entry(oauth: &InsertedOAuth, conn: &PgConnection) -> Result<OAuth, Error> {
        match OAuthRepository::get_all_oauth_entries(conn) {
            Ok(entries) => {
                if entries.len() == 1 {
                    match OAuthRepository::update_oauth_entry(entries[0].id, oauth, conn) {
                        Ok(_) => {
                            return OAuthRepository::get_oauth_entry_by_id(entries[0].id, conn);
                        },
                        Err(err) => return Err(err)
                    }
                }
            },
            Err(err) => {
                info!("Error retriving OAuth Entries [{}]", err);
            }
        }

        diesel::insert_into(oauth::table)
            .values(oauth)
            .get_result::<OAuth>(&*conn)
    }

    pub fn get_oauth_entry_by_id(oauth_id: i32, conn: &PgConnection) -> Result<OAuth, Error> {
        oauth::table
            .filter(oauth::id.eq(oauth_id))
            .first(&*conn)
    }

    pub fn update_oauth_entry(oauth_id: i32, oauth_i: &InsertedOAuth, conn: &PgConnection) -> Result<usize, Error> {
        diesel::update(oauth::table.filter(oauth::id.eq(oauth_id)))
            .set(oauth::token.eq(&oauth_i.token)
            ).execute(&*conn)
    }

    pub fn delete_oauth_entry(oauth_id: i32, conn: &PgConnection) -> Result<usize, Error> {
        diesel::delete(oauth::table.filter(oauth::id.eq(oauth_id))).execute(&*conn)
    }

    pub fn get_all_oauth_entries(conn: &PgConnection) -> Result<Vec<OAuth>,Error> {
        oauth::table.load::<OAuth>(&*conn)
    }
}