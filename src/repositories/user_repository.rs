use diesel::{self, prelude::*};
use diesel::result::Error;

use crate::models::user::{MovUser, InsertedMovUser, ResponseMovUser, UpdateMovUser};
use crate::models::address::{Address};
use crate::models::roles::{Role};
use crate::schema::*;

pub struct MovUserRepository;

impl MovUserRepository {

    pub fn get_by_username(_username: String, conn: &PgConnection) -> Result<MovUser, Error> {
        mov_user::table
            .select(         
                    (mov_user::id, mov_user::email, mov_user::username, mov_user::firstname, mov_user::lastname, mov_user::gender, mov_user::title, 
                    mov_user::password, mov_user::salt, mov_user::address_id, mov_user::role_id, mov_user::created, mov_user::changed))
            .filter(mov_user::username.eq(_username))
            .first::<MovUser>(&*conn)
    }

    pub fn create_user(user: &InsertedMovUser, conn: &PgConnection) -> Result<MovUser, Error> {
        diesel::insert_into(mov_user::table)
            .values(user)
            .returning((mov_user::id, mov_user::email, mov_user::username, mov_user::firstname, mov_user::lastname, mov_user::gender, mov_user::title, 
                    mov_user::password, mov_user::salt, mov_user::address_id, mov_user::role_id, mov_user::created, mov_user::changed))
            .get_result::<MovUser>(&*conn)
    }

    pub fn get_by_user_id(user_id: i32, conn: &PgConnection) -> Result<ResponseMovUser, Error> {
        let res = mov_user::table
            .inner_join(address::table)
            .inner_join(roles::table)
            .select((          
                    (mov_user::id, mov_user::email, mov_user::username, mov_user::firstname, mov_user::lastname, mov_user::gender, mov_user::title, 
                    mov_user::password, mov_user::salt, mov_user::address_id, mov_user::role_id, mov_user::created, mov_user::changed),

                    (address::id, address::street, address::zip, address::city, address::country, address::created, address::changed),
                
                    (roles::id, roles::name, roles::created, roles::changed, roles::email)

                    ))
            .filter(mov_user::id.eq(user_id))
            .first::<(MovUser, Address, Role)>(&*conn);

        match res {
            Ok((user, address, role)) => {
                    Ok(ResponseMovUser::new(&user, &address, &role))
                },
            Err(err) => Err(err)
        }
    }

    

    pub fn get_user_with_password_by_id(user_id: i32, conn: &PgConnection) -> Result<MovUser, Error> {
        mov_user::table
            .select( (mov_user::id, mov_user::email, mov_user::username, mov_user::firstname, mov_user::lastname, mov_user::gender, mov_user::title, 
                    mov_user::password, mov_user::salt, mov_user::address_id, mov_user::role_id, mov_user::created, mov_user::changed))
            .filter(mov_user::id.eq(user_id))
            .first::<MovUser>(&*conn)
    }

    pub fn get_user_with_password_by_username(username: &String, conn: &PgConnection) -> Result<MovUser, Error> {
        mov_user::table
            .select( (mov_user::id, mov_user::email, mov_user::username, mov_user::firstname, mov_user::lastname, mov_user::gender, mov_user::title, 
                    mov_user::password, mov_user::salt, mov_user::address_id, mov_user::role_id, mov_user::created, mov_user::changed))
            .filter(mov_user::username.eq(username))
            .first::<MovUser>(&*conn)
    }

    pub fn update_user(user_id: i32, user: &UpdateMovUser, conn: &PgConnection) -> Result<usize, Error> {
        diesel::update(mov_user::table.find(user_id))
            .set(
                (mov_user::email.eq(&user.email), mov_user::firstname.eq(&user.username), 
                mov_user::lastname.eq(&user.lastname), mov_user::gender.eq(&user.gender), 
                mov_user::title.eq(&user.title), mov_user::address_id.eq(&user.address_id),
                mov_user::role_id.eq(&user.role_id)
                ))
            .execute(&*conn)
    }

    pub fn delete_user(user_id: i32, conn: &PgConnection) -> Result<usize, Error> {
        diesel::delete(mov_user::table.filter(mov_user::id.eq(user_id))).execute(&*conn)
    }

    pub fn get_all_user(conn: &PgConnection) -> Result<Vec<ResponseMovUser>, Error> {
        match mov_user::table
            .inner_join(address::table)
            .inner_join(roles::table)
            .select((          
                    (mov_user::id, mov_user::email, mov_user::username, mov_user::firstname, mov_user::lastname, mov_user::gender, mov_user::title, 
                    mov_user::password, mov_user::salt, mov_user::address_id, mov_user::role_id, mov_user::created, mov_user::changed),

                    (address::id, address::street, address::zip, address::city, address::country, address::created, address::changed),
                
                    (roles::id, roles::name, roles::created, roles::changed, roles::email)

                    ))
            .load::<(MovUser, Address, Role)>(&*conn) {
                Ok(list) => {
                    Ok(list.iter().map(|(user, address, role)| ResponseMovUser::new(user, address, role)).collect::<Vec<ResponseMovUser>>())
                },
                Err(err) => Err(err)
            }
    }

    pub fn user_update_password(user_id: i32, password: &String, salt: &String, conn: &PgConnection) -> Result<usize, Error> {
        diesel::update(mov_user::table.find(user_id))
            .set((mov_user::password.eq(password), (mov_user::salt.eq(salt))))
            .execute(&*conn)
    }
}