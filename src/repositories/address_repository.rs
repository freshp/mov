use diesel::{self, prelude::*};
use diesel::result::Error;

use crate::models::address::{Address, InsertedAddress};
use crate::schema::address;

pub struct AddressRepository;

impl AddressRepository {
    pub fn create_address(address: &InsertedAddress, conn: &PgConnection) -> Result<Address, Error> {
        match address::table
            .filter(address::street.eq(&address.street).and(address::city.eq(&address.city).and(address::zip.eq(&address.zip).and(address::country.eq(&address.country)))))
            .first::<Address>(&*conn) {
                Ok(address) => return Ok(address),
                Err(_) => ()
        };

        diesel::insert_into(address::table)
            .values(address)
            .get_result::<Address>(&*conn)
    }

    pub fn get_address_by_id(address_id: i32, conn: &PgConnection) -> Result<Address, Error> {
        let res = address::table
                    .filter(address::id.eq(address_id))
                    .first(&*conn);
        match res {
            Ok(address) => Ok(address),
            Err(error) => Err(error)
        }
    }
    pub fn update_address(address_id: i32, address: &InsertedAddress, conn: &PgConnection) -> Result<usize, Error> {
        diesel::update(address::table.filter(address::id.eq(address_id)))
            .set(
                (address::street.eq(&address.street), address::zip.eq(&address.zip),
                address::city.eq(&address.city), address::country.eq(&address.country))
            ).execute(&*conn)
    }

    pub fn delete_address(address_id: i32, conn: &PgConnection) -> Result<usize, Error> {
        diesel::delete(address::table.filter(address::id.eq(address_id))).execute(&*conn)
    }

    pub fn get_all_addresses(conn: &PgConnection) -> Result<Vec<Address>,Error> {
        address::table.load::<Address>(&*conn)
    }
}