use diesel::{self, prelude::*};
use diesel::result::Error;
use chrono::{ Duration, Utc };
use crate::models::roles::{Role, InsertedRole};
use crate::schema::*;

pub struct RoleRepository;

impl RoleRepository {
	pub fn create_role(role: &InsertedRole, conn: &PgConnection) -> Result<Role, Error> {
		match RoleRepository::get_role_by_name(&role.name, conn) {
					Ok(role) => return Ok(role),
					Err(_) => ()
				};

		diesel::insert_into(roles::table)
			.values(role)
			.get_result::<Role>(&*conn)
	}
	
    pub fn get_role_by_id(role_id: i32, conn: &PgConnection) -> Result<Role, Error> {
        roles::table
            .filter(roles::id.eq(role_id))
            .first(&*conn)
    }

	pub fn get_role_by_name(name: &String, conn: &PgConnection) -> Result<Role, Error> {
		roles::table
			.filter(roles::name.eq(name))
			.first::<Role>(&*conn)
	}

	pub fn update_role(role_id: i32, role: &InsertedRole, conn: &PgConnection) -> Result<usize, Error> {
		diesel::update(roles::table.find(role_id))
			.set(roles::name.eq(&role.name))
			.execute(&*conn)
	}

	pub fn delete_role(role_id: i32, conn: &PgConnection) -> Result<usize, Error> {
		diesel::delete(roles::table.filter(roles::id.eq(role_id))).execute(&*conn)
	}

	pub fn get_all_roles(conn: &PgConnection) -> Result<Vec<Role>, Error> {
		roles::table.load::<Role>(&*conn)
	}

	pub fn get_all_members_email_from_that_role(name: &String, conn: &PgConnection) -> Result<Vec<String>, Error> {
		let one_half_year = Utc::now().naive_utc() - Duration::weeks(52 + 25);

		member::table.inner_join(roles::table)
			.select(member::email)
			.filter(roles::name.eq(name).and(member::active_email.eq(true).and(member::membership_due.gt(one_half_year))))
			.load::<String>(&*conn)
	
	}
}