use diesel::{self, prelude::*};
use diesel::result::Error;

use crate::models::member_queue::{MemberQueue, InsertedMemberQueue};
use crate::schema::member_queue;

pub struct MemberQueueRepository;

impl MemberQueueRepository {
    pub fn add_element_to_queue(member_e: &InsertedMemberQueue, conn: &PgConnection) -> Result<MemberQueue, Error> {
        diesel::insert_into(member_queue::table)
            .values(member_e)
            .get_result::<MemberQueue>(&*conn)
    }

    pub fn delete_element_from_queue(queue_id: i32, conn: &PgConnection) -> Result<usize, Error> {
        diesel::delete(member_queue::table.filter(member_queue::id.eq(queue_id))).execute(&*conn)
    }

    pub fn get_all_elements(conn: &PgConnection) -> Result<Vec<MemberQueue>,Error> {
        member_queue::table.load::<MemberQueue>(&*conn)
    }
}