use diesel::{self, prelude::*};
use diesel::result::Error;
use chrono::{ Duration, Utc };
use crate::models::newsletter::{Newsletter, InsertedNewsletter};
use crate::schema::*;

pub struct NewsletterRepository();

impl NewsletterRepository {
    pub fn create_newsletter(newsletter: &InsertedNewsletter, conn: &PgConnection) -> Result<Newsletter, Error> {
		match newsletter::table
				.select((newsletter::id, newsletter::name, newsletter::created, newsletter::changed, newsletter::email))
				.filter(newsletter::name.eq(&newsletter.name))
				.first::<Newsletter>(&*conn) {
					Ok(newsletter) => return Ok(newsletter),
					Err(_) => ()
				};

		diesel::insert_into(newsletter::table)
			.values(newsletter)
			.get_result::<Newsletter>(&*conn)
	}
	
    pub fn get_newsletter_by_id(newsletter_id: i32, conn: &PgConnection) -> Result<Newsletter, Error> {
        newsletter::table
            .filter(newsletter::id.eq(newsletter_id))
            .first(&*conn)
    }

	pub fn get_newsletter_by_name(name: &String, conn: &PgConnection) -> Result<Newsletter, Error> {
		newsletter::table
			.filter(newsletter::name.eq(name))
			.first(&*conn)
	}

	pub fn update_newsletter(newsletter_id: i32, newsletter: &InsertedNewsletter, conn: &PgConnection) -> Result<usize, Error> {
		diesel::update(newsletter::table.find(newsletter_id))
			.set((newsletter::name.eq(&newsletter.name), newsletter::email.eq(&newsletter.email)))
			.execute(&*conn)
	}

	pub fn delete_newsletter(newsletter_id: i32, conn: &PgConnection) -> Result<usize, Error> {
		diesel::delete(newsletter::table.filter(newsletter::id.eq(newsletter_id))).execute(&*conn)
	}

	pub fn get_all_newsletters(conn: &PgConnection) -> Result<Vec<Newsletter>, Error> {
		newsletter::table.load::<Newsletter>(&*conn)
	}

	pub fn get_all_members_that_have_newsletter(newsletter: &String, conn: &PgConnection) -> Result<Vec<String>, Error> {
		let newsletter = NewsletterRepository::get_newsletter_by_name(newsletter, conn);

		let one_half_year = Utc::now().naive_utc() - Duration::weeks(52 + 25);

		match newsletter {
			Ok(newsletter) => {
				member::table.inner_join(newsletter_user::table)
					.select(member::email)
					.filter(newsletter_user::newsletter_id.eq(newsletter.id).and(member::active_email.eq(true).and(member::membership_due.gt(one_half_year))))
					.load::<String>(&*conn)
			},
			Err(err) => Err(err)
		}
	}
}