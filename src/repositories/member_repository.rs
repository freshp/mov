use diesel::{self, prelude::*};
use diesel::result::Error;
use chrono::{ NaiveDateTime, Duration, Utc };

use crate::models::member::{Member, InsertedMember, ResponseMember, UpdateMember};
use crate::models::address::{Address};
use crate::models::roles::{Role};
use crate::models::newsletter::{Newsletter, InsertedNewsletter, AddNewsletter};
use crate::models::newsletter_user::{NewsletterUser, InsertedNewsletterUser};
use crate::repositories::newsletter_repository::{NewsletterRepository};
use crate::schema::*;

pub struct MemberRepository;

impl MemberRepository {
    pub fn get_member_by_id(member_id: i32, conn: &PgConnection) -> Result<ResponseMember, Error> {
        let res = member::table
            .inner_join(address::table)
            .inner_join(roles::table)
            .select(
                (
                    (member::id, member::member_id ,member::email, member::firstname, member::lastname, member::gender, member::title, member::active_email, member::reminder_sent,
                    member::address_id, member::location_id, member::member_since, member::membership_due, member::changed_membership, member::created, member::changed, member::info),
                
                    (address::id, address::street, address::zip, address::city, address::country, address::created, address::changed),
                
                    (roles::id, roles::name, roles::created, roles::changed, roles::email)
                )
                )
            .filter(member::id.eq(member_id))
            .first::<(Member, Address, Role)>(&*conn);
        
        let newsletter_res = newsletter_user::table
            .inner_join(newsletter::table)
            .select((newsletter::id, newsletter::name, newsletter::created, newsletter::changed, newsletter::email))
            .filter(newsletter_user::member_id.eq(member_id))
            .load::<Newsletter>(&*conn);

        match res {
            Ok((member, address, role)) => {
                    match newsletter_res {
                        Ok(newsletter) => {
                            Ok(ResponseMember::new_with_newsletter(&member, &address, &role, newsletter))
                        },
                        Err(error) => Err(error)
                    }
                    
                },
            Err(error) => Err(error)
        }
    }

    pub fn get_member_by_member_id(member_id: String, conn: &PgConnection) -> Result<ResponseMember, Error> {
        match member::table
            .inner_join(address::table)
            .inner_join(roles::table)
            .select(
                (
                    (member::id, member::member_id ,member::email, member::firstname, member::lastname, member::gender, member::title, member::active_email, member::reminder_sent,
                    member::address_id, member::location_id, member::member_since, member::membership_due, member::changed_membership, member::created, member::changed, member::info),
                
                    (address::id, address::street, address::zip, address::city, address::country, address::created, address::changed),
                
                    (roles::id, roles::name, roles::created, roles::changed, roles::email)
                )
                )
            .filter(member::member_id.eq(&member_id).and(member::membership_due.gt(Utc::now().naive_utc())))
            .first::<(Member, Address, Role)>(&*conn) {
                Ok((member, add, role)) => {
                    Ok(ResponseMember::new(&member,&add, &role))
                },
                Err(err) => Err(err)
            }
    }

    pub fn create_member(member: &InsertedMember, conn: &PgConnection) -> Result<Member, Error> {
        diesel::insert_into(member::table)
            .values(member)
            .returning((member::id, member::member_id ,member::email, member::firstname, member::lastname, member::gender, member::title, member::active_email, member::reminder_sent, member::address_id,
                    member::location_id, member::member_since, member::membership_due, member::changed_membership, member::created, member::changed, member::info))
            .get_result::<Member>(&*conn)
    }

    pub fn update_member(member_id: i32, member: &UpdateMember, conn: &PgConnection) -> Result<usize, Error> {
        diesel::update(member::table.find(member_id))
            .set(
                (member::email.eq(&member.email), member::firstname.eq(&member.firstname),
                member::lastname.eq(&member.lastname), member::gender.eq(&member.gender), member::title.eq(&member.title),
                member::address_id.eq(&member.address_id), member::location_id.eq(&member.location_id), member::info.eq(&member.info))
            ).execute(&*conn)
    }

    pub fn delete_member(member_id: i32, conn: &PgConnection) -> Result<usize, Error> {
        match diesel::delete(newsletter_user::table.filter(newsletter_user::member_id.eq(member_id))).execute(&*conn) {
            Ok(_) => (),
            Err(err) => return Err(err)
        }

        diesel::delete(member::table.filter(member::id.eq(member_id))).execute(&*conn)
    }

    pub fn get_all_members(conn: &PgConnection) -> Result<Vec<ResponseMember>, Error> {
        match member::table.inner_join(address::table)
            .inner_join(roles::table)
            .select(
                (
                    (member::id, member::member_id ,member::email, member::firstname, member::lastname, member::gender, member::title, member::active_email, member::reminder_sent,
                    member::address_id, member::location_id, member::member_since, member::membership_due, member::changed_membership, member::created, member::changed, member::info),
                
                    (address::id, address::street, address::zip, address::city, address::country, address::created, address::changed),
                
                    (roles::id, roles::name, roles::created, roles::changed, roles::email)
                )
                )
                .order(member::membership_due.asc())
                .load::<(Member, Address, Role)>(&*conn) {
                    Ok(list) => {
                        Ok(list.iter().map(|(member, address, role)| ResponseMember::new(member, address, role)).collect::<Vec<ResponseMember>>())
                    },
                    Err(err) => Err(err)
                }
    }

    pub fn add_newsletters(member_id: i32, newsletters: &Vec<AddNewsletter>, conn: &PgConnection) -> Result<usize, String> {
        let mut number: usize = 0;
        for i in newsletters {
            let newsletter_id: i32;
            match NewsletterRepository::get_newsletter_by_name(&i.name, &*conn) {
                Ok(newsletter) => newsletter_id = newsletter.id,
                Err(_) => {
                    return Err(format!("Something went wrong retriving a newsletter {}", i.name));
                }
            }

            match newsletter_user::table
                .filter(newsletter_user::member_id.eq(member_id).and(newsletter_user::newsletter_id.eq(newsletter_id)))
                .load::<NewsletterUser>(&*conn) {
                    Ok(newsletter) => {
                        if newsletter.len() > 0 {
                            number += 1;
                            continue;
                        }
                    }
                    Err(_) => {
                            return Err(String::from("An error occurred!"));
                        }
                }
            


            number += diesel::insert_into(newsletter_user::table)
                .values(InsertedNewsletterUser { 
                        newsletter_id: newsletter_id,
                        member_id: member_id
                    }).execute(&*conn).unwrap_or_else(|_| 0);
        }

       

        Ok(number)
    }

    pub fn delete_newsletters(member_id: i32, newsletters: &Vec<AddNewsletter>, conn: &PgConnection) -> Result<usize, String> {
        let mut number: usize = 0;
        for i in newsletters {
            let newsletter_id: i32;
            match NewsletterRepository::get_newsletter_by_name(&i.name, &*conn) {
                Ok(newsletter) => newsletter_id = newsletter.id,
                Err(_) => {
                    return Err(format!("Something went wrong retriving a newsletter {}", i.name));
                }
            }

            number += diesel::delete(newsletter_user::table
                    .filter(newsletter_user::member_id.eq(member_id).and(newsletter_user::newsletter_id.eq(newsletter_id))))
                    .execute(&*conn).unwrap_or_else(|_| 0);
        }

        if number < newsletters.len() {
            return Err(String::from("Something went wrong while trying to add newsletters"));
        }

        Ok(number)
    }

    pub fn change_membership_date(member_id: i32, date: &NaiveDateTime, conn: &PgConnection) -> Result<usize, Error> {
        diesel::update(member::table.find(member_id))
            .set((
                member::membership_due.eq(date), 
                member::changed_membership.eq(Utc::now().naive_utc()),
                member::reminder_sent.eq(false)
                ))
            .execute(&*conn)
    }

    pub fn set_email_active(member_id: i32, active: bool, conn: &PgConnection) -> Result<usize, Error> {
        diesel::update(member::table.find(member_id))
            .set(member::active_email.eq(active))
            .execute(&*conn)
    }

    pub fn set_reminder_sent(member_id: i32, sent: bool, conn: &PgConnection) -> Result<usize, Error> {
        diesel::update(member::table.find(member_id))
            .set(member::reminder_sent.eq(sent))
            .execute(&*conn)
    }

    //function that gets all members to send the reminder, check for date, active, reminder_sent
    pub fn get_all_members_that_have_to_be_reminded_and_have_active_mail_address(conn: &PgConnection) -> Result<Vec<(Member, Role)>, Error> {
        let one_week = Utc::now().naive_utc() + Duration::weeks(1);

        member::table.inner_join(address::table)
            .inner_join(roles::table)
            .select((
                    (member::id, member::member_id ,member::email, member::firstname, member::lastname, member::gender, member::title, member::active_email, member::reminder_sent,
                    member::address_id, member::location_id, member::member_since, member::membership_due, member::changed_membership, member::created, member::changed, member::info),
                    (roles::id, roles::name, roles::created, roles::changed, roles::email))
            )
            .filter(member::reminder_sent.eq(false).and(member::active_email.eq(true).and(member::membership_due.lt(one_week))))
            .load::<(Member, Role)>(&*conn)
                
    }

    pub fn get_all_members_with_inactive_email(conn: &PgConnection) -> Result<Vec<(Member, Role)>, Error> {
        member::table.inner_join(roles::table).select((
            (member::id, member::member_id ,member::email, member::firstname, member::lastname, member::gender, member::title, member::active_email, member::reminder_sent,
            member::address_id, member::location_id, member::member_since, member::membership_due, member::changed_membership, member::created, member::changed, member::info),
            (roles::id, roles::name, roles::created, roles::changed, roles::email))
            ).filter(member::active_email.eq(false).and(member::membership_due.gt(Utc::now().naive_utc())))
            .load::<(Member, Role)>(&*conn)
    }
}