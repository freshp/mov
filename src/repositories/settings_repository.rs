use diesel::{self, prelude::*};
use diesel::result::Error;

use crate::models::settings::{InsertedSettings, Settings};
use crate::schema::*;

pub struct SettingsRepository;

impl SettingsRepository {
    pub fn create_settings(settings: &InsertedSettings, conn: &PgConnection) -> Result<Settings, Error> {
        diesel::insert_into(settings::table)
            .values(settings)
            .get_result::<Settings>(&*conn)
    }

    pub fn get_settings(conn: &PgConnection) -> Result<Settings, Error> {
        settings::table
            .select((settings::id, settings::email_username, settings::email_password, settings::welcome_subject, settings::welcome_text, settings::reminder_subject,
                    settings::reminder_text, settings::extend_subject, settings::extend_text, settings::created, settings::changed))
            .first::<Settings>(&*conn)
    }
     pub fn update_settings(settings: &InsertedSettings, conn: &PgConnection) -> Result<Settings, Error> {
         let old: Settings;

         match settings::table
            .select((settings::id, settings::email_username, settings::email_password, settings::welcome_subject, settings::welcome_text, settings::reminder_subject,
                    settings::reminder_text, settings::extend_subject, settings::extend_text, settings::created, settings::changed))
            .first::<Settings>(&*conn) {
             Ok(set) => old = set,
             Err(err) => return Err(err)
         }

         diesel::update(settings::table.find(old.id))
            .set((settings::email_username.eq(&settings.email_username), settings::email_password.eq(&settings.email_password),
                settings::welcome_subject.eq(&settings.welcome_subject), settings::welcome_text.eq(&settings.welcome_text),
                settings::reminder_subject.eq(&settings.reminder_subject), settings::reminder_text.eq(&settings.reminder_text), 
                settings::extend_subject.eq(&settings.extend_subject), settings::extend_text.eq(&settings.extend_text)))
            .get_result(&*conn)
     }
}