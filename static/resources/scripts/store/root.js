define(function (require, exports, module) {
  let Vuex = require('vuex');

  let axios = require('axios');
  let axios_config = require('axios_config');

  Vue.use(Vuex);

  const store = new Vuex.Store({
    state: {
      authorization: {
        logged_in: localStorage.getItem('logged_in'),
        token: localStorage.getItem('token'),
        role: localStorage.getItem('role')
      },
      gender: [{ value: 'M', text: 'M' }, { value: 'F', text: 'F' }, { value: 'unkonw', text: 'unknown' }],
      login_component: {
        username: { placeholder: 'Benutzername' },
        password: { placeholder: '************' },
      },
      members: {
        fields: [
          { key: 'gender', sortable: true },
          { key: 'email', sortable: true },
          { key: 'firstname', sortable: true },
          { key: 'lastname', sortable: true },
          { key: 'location.name', sortable: true },
          { key: 'membership_due', sortable: true },
        ],
        data: [],
        changed: false,
        filter_worker: null
      },
      users: {
        fields: [
          { key: 'id', sortable: true }, { key: 'username', sortable: true },
          { key: 'email', sortable: true }, { key: 'firstname', sortable: true },
          { key: 'lastname', sortable: true }, { key: 'role.name', sortable: true }
        ],
        data: []
      },
      roles: {
        fields: [
          { key: 'id', sortable: true },
          { key: 'name', sortable: true },
        ],
        data: []
      },
      newsletters: {
        fields: [
          { key: 'id', sortable: true },
          { key: 'name', sortable: true },
        ],
        data: []
      }
    },
    getters: {
      is_logged_in: state => {
        return state.authorization.logged_in;
      },
      get_token: state => {
        return state.authorization.token;
      },
      get_genders: state => {
        return state.gender;
      },
      get_role: state => {
        return state.authorization.role;
      },
      get_role_with_id: state => id => {
        let list = state.rols.data.filter(function (element) {
          if (Number(element.id) == Number(id)) {
            return element;
          }
        });

        if (list.length > 0) {
          return list[0];
        } else {
          return null;
        }
      },
      get_login_component: state => {
        return state.login_component;
      },
      get_members: state => {
        return state.members;
      },
      get_members_data: state => (show) => {
        if (show == 1) {
          let now = new Date();
          return state.members.data.filter(function (currentValue, index, arr) {
            let ts = new Date(currentValue.membership_due);

            if (ts > now) {
              return currentValue;
            }
          });
        } else if (show == 2) {
          let now = new Date();
          return state.members.data.filter(function (currentValue, index, arr) {
            let ts = new Date(currentValue.membership_due);

            if (ts < now) {
              return currentValue;
            }
          });
        } else {
          return state.members.data;
        }
      },
      get_member_with_id: state => id => {
        let list = state.members.data.filter(function (element) {
          if (Number(element.id) == Number(id)) {
            return element;
          }
        });

        if (list.length > 0) {
          return list[0];
        } else {
          return null;
        }
      },
      get_users: state => {
        return state.users;
      },
      get_users_data: state => {
        return state.users.data;
      },
      get_user_with_id: state => id => {
        if (state.users.data.length === 0) {
          return [];
        }

        let list = state.users.data.filter(function (element) {
          if (Number(element.id) == Number(id)) {
            return element;
          }
        });

        if (list.length > 0) {
          return list[0];
        } else {
          return null;
        }
      },
      get_roles: state => {
        return state.roles;
      },
      get_roles_data: state => {
        return state.roles.data;
      },
      get_roles_as_select_options: state => {
        let options = [];

        for (let o of state.roles.data) {
          options.push({ value: o.name, text: o.name });
        }

        return options;
      },
      get_role_with_id: state => id => {
        let list = state.roles.data.filter(function (element) {
          if (Number(element.id) == Number(id)) {
            return element;
          }
        });

        if (list.length > 0) {
          return list[0];
        } else {
          return null;
        }
      },
      get_newsletters: state => {
        return state.newsletters;
      },
      get_newsletters_data: state => {
        return state.newsletters.data;
      },
      get_newsletter_with_id: state => id => {
        let list = state.newsletters.data.filter(function (element) {
          if (Number(element.id) == Number(id)) {
            return element;
          }
        });

        if (list.length > 0) {
          return list[0];
        } else {
          return null;
        }
      },
      get_newsletters_as_options: state => {
        let options = [];

        for (let o of state.newsletters.data) {
          options.push({ value: o.name, text: o.name });
        }

        return options;
      }
    },
    mutations: {
      set_logged_in(state, value) {
        localStorage.setItem('logged_in', value);
        state.authorization.logged_in = value;
      },
      set_token(state, value) {
        localStorage.setItem('token', value);
        state.authorization.token = value;
      },
      set_role(state, value) {
        localStorage.setItem('role', value);
        state.authorization.role = value;
      },
      set_members(state, value) {
        state.members.data = value;
      },
      set_users(state, value) {
        state.users.data = value;
      },
      set_roles(state, value) {
        state.roles.data = value;
      },
      set_newsletters(state, value) {
        state.newsletters.data = value;
      }
    },
    actions: {
      ajax_call({ getters }, { method, url, payload }) {
        return new Promise((resolve, reject) => {
          if (method === 'delete') {
            let temp =
              Object.assign({ data: payload }, axios_config(getters.get_token));
            axios[method](url, temp)
              .then((response) => {
                resolve(response);
              })
              .catch((err) => {
                reject(err);
              });
          } else if (method === 'get') {
            axios[method](url, axios_config(getters.get_token))
              .then((response) => {
                resolve(response);
              })
              .catch((err) => {
                reject(err);
              });
          } else {
            axios[method](url, payload, axios_config(getters.get_token))
              .then((response) => {
                resolve(response);
              })
              .catch((err) => {
                reject(err);
              });
          }
        });
      },
      check_logged_in({ dispatch, commit, getters }) {
        dispatch(
          'ajax_call', { method: 'get', url: '/check_logged_in', payload: {} })
          .then((response) => {
            commit('set_logged_in', true);
          })
          .catch((error) => {
            commit('set_logged_in', false);
          });
      },
      logout({ commit, getters }) {
        return new Promise((resolve, reject) => {
          commit('set_logged_in', false);
          commit('set_token', '');
          resolve();
        });
      },
      download_members({ dispatch, commit, state }) {
        dispatch('ajax_call', { method: 'get', url: '/member/list', payload: {} })
          .then((response) => {
            if (response.status === 200) {
              state.members.changed = true;
              commit('set_members', Object.freeze(response.data));
            }
          })
          .catch((error) => {
            console.log(error);
          })
      },
      create_member({ dispatch }, payload) {
        return dispatch(
          'ajax_call', { method: 'post', url: '/member/' + payload.send, payload: payload });
      },
      get_member({ dispatch, state }, payload) {
        return new Promise((resolve, reject) => {
          dispatch(
            'ajax_call',
            { method: 'get', url: '/member/' + payload.id, payload: payload }).then((result) => {
              resolve(result);
            }).catch((error) => {
              reject(error);
            })
        });
      },
      update_member({ dispatch }, payload) {
        return dispatch(
          'ajax_call',
          { method: 'put', url: '/member/' + payload.id, payload: payload });
      },
      delete_member({ dispatch }, payload) {
        return dispatch(
          'ajax_call',
          { method: 'delete', url: '/member/' + payload.id, payload: payload });
      },
      extend_membership({ dispatch }, payload) {
        return dispatch('ajax_call', {
          method: 'post',
          url: '/member/extend-membership/' + payload.id,
          payload: payload
        });
      },
      check_membership({ dispatch }, payload) {
        return dispatch('ajax_call', {
          method: 'post',
          url: '/member/check-membership/' + payload.id,
          payload: payload
        });
      },
      create_member_filter_worker({ state }) {
        state.members.filter_worker = new Worker('resources/scripts/webworker/member-filter.js');
        state.members.filter_worker.onerror = function (e) {
          console.error(e);
        }
      },
      filter_members({ state }, payload) {
        return new Promise((resolve) => {
          state.members.filter_worker.onmessage = function (e) {
            state.members.changed = false;
            resolve(e.data[0]);
          }
          payload.members = state.members.changed ? state.members.data : [];
          payload.changed = state.members.changed;
          state.members.filter_worker.postMessage([payload])
        });
      },
      add_newsletters_to_user({ dispatch }, payload) {
        return dispatch('ajax_call', {
          method: 'post',
          url: '/member/newsletter/' + payload.id,
          payload: payload.data
        });
      },
      delete_newsletters_from_user({ dispatch }, payload) {
        return dispatch('ajax_call', {
          method: 'delete',
          url: '/member/newsletter/' + payload.id,
          payload: payload.data
        })
      },
      get_newsletters_email({ dispatch }, payload) {
        return dispatch('ajax_call', {
          method: 'get',
          url: '/newsletter/subscribed/' + payload.name,
          payload: payload
        });
      },
      download_users({ dispatch, commit, getters }) {
        dispatch('ajax_call', { method: 'get', url: '/user/list', payload: {} })
          .then((response) => {
            if (response.status === 200) {
              commit('set_users', response.data);
            }
          })
          .catch((error) => {
            console.log(error);
          });
      },
      create_user({ dispatch }, payload) {
        return dispatch(
          'ajax_call', { method: 'post', url: '/user', payload: payload });
      },
      get_user({ dispatch }, payload) {
        return dispatch(
          'ajax_call',
          { method: 'get', url: '/user/' + payload.id, payload: payload });
      },
      update_user({ dispatch }, payload) {
        return dispatch(
          'ajax_call',
          { method: 'put', url: '/user/' + payload.id, payload: payload });
      },
      delete_user({ dispatch }, payload) {
        return dispatch(
          'ajax_call',
          { method: 'delete', url: '/user/' + payload.id, payload: payload });
      },
      download_roles({ dispatch, commit, getters }) {
        dispatch('ajax_call', { method: 'get', url: '/roles/list', payload: {} })
          .then((response) => {
            if (response.status === 200) {
              commit('set_roles', response.data);
            }
          })
          .catch((error) => {
            console.log(error);  // TODO: log in other location maybe
          });
      },
      create_role({ dispatch }, payload) {
        return dispatch(
          'ajax_call', { method: 'post', url: '/roles', payload: payload })
      },
      get_role({ dispatch }, payload) {
        return dispatch(
          'ajax_call',
          { method: 'get', url: '/roles/' + payload.id, payload: payload });
      },
      update_role({ dispatch }, payload) {
        return dispatch(
          'ajax_call',
          { method: 'put', url: '/roles/' + payload.id, payload: payload });
      },
      delete_role({ dispatch }, payload) {
        return dispatch(
          'ajax_call',
          { method: 'delete', url: '/roles/' + payload.id, payload: payload });
      },
      get_roles_email({ dispatch }, payload) {
        return dispatch(
          'ajax_call',
          { method: 'get', url: '/roles/subscribed/' + payload.name, payload: payload }
        );
      },
      download_newsletters({ dispatch, commit, getters }) {
        dispatch(
          'ajax_call', { method: 'get', url: '/newsletter/list', payload: {} })
          .then((response) => {
            if (response.status === 200) {
              commit('set_newsletters', response.data);
            }
          })
          .catch((error) => {
            console.log(error);
          });
      },
      create_newsletter({ dispatch }, payload) {
        return dispatch(
          'ajax_call',
          { method: 'post', url: '/newsletter/', payload: payload });
      },
      get_newsletter({ dispatch }, payload) {
        return dispatch('ajax_call', {
          method: 'get',
          url: '/newsletter/' + payload.id,
          payload: payload
        });
      },
      update_newsletter({ dispatch }, payload) {
        return dispatch('ajax_call', {
          method: 'put',
          url: '/newsletter/' + payload.id,
          payload: payload
        });
      },
      delete_newsletter({ dispatch }, payload) {
        return dispatch('ajax_call', {
          method: 'delete',
          url: '/newsletter/' + payload.id,
          payload: payload
        })
      },
      set_password({ dispatch }, payload) {
        return dispatch(
          'ajax_call',
          { method: 'post', url: '/user/set/password', payload: payload });
      },
      get_settings({ dispatch }, payload) {
        return dispatch(
          'ajax_call', { method: 'get', url: '/settings', payload: payload });
      },
      save_settings({ dispatch }, payload) {
        return dispatch(
          'ajax_call', { method: 'post', url: '/settings', payload: payload });
      },
      resend_emails({ dispatch }, payload) {
        return dispatch(
          'ajax_call', { method: 'post', url: 'member/resend', payload: payload }
        );
      },
      resend_email({ dispatch }, payload) {
        return dispatch(
          'ajax_call', { method: 'post', url: '/member/resend/' + payload.id, payload: payload }
        );
      }
    }
  });

  return store;
});