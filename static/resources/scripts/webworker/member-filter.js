let members = [];

onmessage = function (e) {
    let payload = e.data[0];

    if (payload.changed) {
        data = Object.freeze(payload.members.map(function (e) {
            return Object.freeze(e);
        }));
    }

    let value = payload;
    let now = new Date();
    let temp = data.filter(function (currentValue, index, arr) {
        if (value.location != null && value.location != currentValue.location.id) {
            return false;
        }

        if (value.show_members == 1) {
            let ts = new Date(currentValue.membership_due);
            if (ts < now) {
              return false;
            }
        } else if (value.show_members == 2) {
            let ts = new Date(currentValue.membership_due);
            if (ts > now) {
              return false;
            }
        }

        if (value.selected == 'location') {
            return (currentValue[value.selected].name.toLocaleLowerCase() + "").startsWith(value.term.toLocaleLowerCase());
        } else {
            if (value.selected == 'name') {
                return (currentValue.firstname.toLocaleLowerCase()).startsWith(value.term.toLocaleLowerCase()) && (currentValue.lastname.toLocaleLowerCase()).startsWith(value.sec_term.toLocaleLowerCase());
            } else {
                return (currentValue[value.selected] + "").toLocaleLowerCase().startsWith(value.term.toLocaleLowerCase());
            }
        }

    });

    postMessage([temp]);
}