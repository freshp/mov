'use strict';


define(function (require, exports, module) {
    let de = require('de');
    return function(language) {
        switch (language) {
            case 'de':
                return de;
            default:
                return de;
        }
    };
});