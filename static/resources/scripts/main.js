require.config({
    baseUrl: 'resources/',
    paths: {
        //vue: 'lib/vue',
        vuex: 'lib/vuex',
        //bootstrap: 'lib/bootstrap-vue.min',
        router: 'lib/vue-router',
        i18n: 'lib/vue-i18n',
        axios: 'lib/axios.min',
        //veeValidate: 'lib/vee-validate.min',

        //config
        axios_config: 'scripts/configurations/axios',

        //store
        store: 'scripts/store/root',

        //routes
        routes: 'scripts/routes/routes',

        //i18n
        messages: 'scripts/translations/messages',
        de: 'scripts/translations/languages/de',

        //sites
        login_site: 'scripts/sites/login',
        dashboard_site: 'scripts/sites/dashboard',
        members_site: 'scripts/sites/members',
        members_edit_site: 'scripts/sites/members_edit',
        newsletter_site: 'scripts/sites/newsletter',
        newsletter_edit_site: 'scripts/sites/newsletter_edit',
        users_site: 'scripts/sites/users',
        users_edit_site: 'scripts/sites/users_edit',
        roles_site: 'scripts/sites/roles',
        roles_edit_site: 'scripts/sites/roles_edit',
        scan_site: 'scripts/sites/scan',
        settings_site: 'scripts/sites/settings',
        not_found: 'scripts/sites/not_found',
        not_authorized: 'scripts/sites/not_authorized',

        //components
        login_component: 'scripts/components/login_component',
        navigation_bar: 'scripts/components/navigation_bar',
        members: 'scripts/components/members',
        users: 'scripts/components/users',
        roles: 'scripts/components/roles',
        newsletter: 'scripts/components/newsletter',
        search_component: 'scripts/components/search',
        member_search: 'scripts/components/member-search'
    }
});

require(
    ['store', 'router', 'i18n', 'routes', 'messages', 'navigation_bar'],
    function(store, VueRouter, VueI18n, routes, messages, nav_bar) {
        Vue.use(VueRouter);
        Vue.use(BootstrapVue);
        Vue.use(VueI18n);
        Vue.use(VeeValidate);

        //register validator globaly
        VeeValidate.extend('required', VeeValidateRules.required);
        VeeValidate.extend('email', VeeValidateRules.email);
        Vue.component('ValidationProvider', VeeValidate.ValidationProvider);

        const router = new VueRouter({
            routes
        });

        router.beforeEach((to, from, next) => {
            if (to.matched.some(record => record.meta.requires_auth)) {
                // this route requires auth, check if logged in
                // if not, redirect to login page.
                if (store.getters.is_logged_in) {
                    if (to.matched.some(record => record.meta.roles) && to.matched[0].meta.roles.length == 1) {
                        if (to.matched[0].meta.roles.includes(store.getters.get_role)) {
                            next();
                        } else {
                            next({
                                path: '/not_authorized'
                            });
                        }

                    } else {
                        next();
                    }
                } else {
                    next({
                        path: '/login'
                    });
                }
            } else {
                next();
            }
        });

        const i18n = new VueI18n({
            locale: 'de',
            fallbackLocale: 'de',
            messages: messages('de')
        });

        let container = {
            data: function() {
                return {
                    show_nav_bar: this.$router.currentRoute.name != 'login'
                };
            },
            watch: {
                '$route' (to, from) {
                    this.show_nav_bar = to.name != 'login';
                }
            },
            components: {
                'nav-bar': nav_bar
            },
            template: `
                        <div>
                            <nav-bar v-if="show_nav_bar"></nav-bar>
                            <router-view></router-view>
                        </div>
                        `
        };

        const app = new Vue({
            router,
            store,
            BootstrapVue,
            i18n,
            VeeValidate,
            components: {
                'container': container
            },
            data: function() {
                return {
                    loading: true
                };
            },
            created() {
                this.$store.commit('set_token', localStorage.getItem('token'));
                this.$store.dispatch('check_logged_in');
                this.loading = false;
                this.$store.dispatch('create_member_filter_worker');
            },
            computed: {
                logged_in() {
                    return this.$store.getters.is_logged_in;
                }
            },
            watch: {
                logged_in(new_value, old_value) {
                    if (!new_value) {
                        this.$router.push('login');
                    }
                }
            }
        }).$mount('#root');

        window.app = app;
    });