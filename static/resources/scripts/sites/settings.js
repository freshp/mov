'use strict';

define(function(require, exports, module) {

    let container = {
        created() {
            this.downloading = true;
            this.$store.dispatch('get_settings', {})
                .then(function(response) {
                    if (response.status === 200) {
                        this.email = response.data.email_username;
                        this.password = response.data.email_password;
                        this.welcome_subject = response.data.welcome_subject;
                        this.welcome_text = response.data.welcome_text;
                        this.reminder_subject = response.data.reminder_subject;
                        this.reminder_text = response.data.reminder_text;
                        this.extend_subject = response.data.extend_subject;
                        this.extend_text = response.data.extend_text;

                        this.downloading = false;
                    }
                }.bind(this))
                .catch(function(error) {
                    console.log(error);
                    this.downloading = false;
                }.bind(this));
        },
        data: function() {
            return {
                error: true,
                email_error: true,
                saving: false,
                downloading: false,
                old_password: '',
                new_password: '',
                new_password_confirm: '',
                email: '',
                password: '',
                welcome_subject: '',
                welcome_text: '',
                reminder_subject: '',
                reminder_text: '',
                extend_subject: '',
                extend_text: '',
                file: ''
            };
        },
        computed: {
            is_admin_user: function() {
                return this.$store.getters.get_role == 'admin';
            }
        },
        methods: {
            import_file() {
                let reader = new FileReader();

                let dispatch = this.$store.dispatch;
                reader.onload = (function(theFile) {
                    return async function(e) {
                        let rows = this.result.split('\n');

                        let length = rows.length;
                        let current = 0;

                        dispatch("create_newsletter", { name: 'Medizin'}).then(function (result) {}).catch(function (error) {});

                        dispatch("create_role", { name: 'Wien'}).then(function (result) {}).catch(function (error) {});
                        dispatch("create_role", { name: 'Bozen'}).then(function (result) {}).catch(function (error) {});
                        dispatch("create_role", { name: 'Innsbruck'}).then(function (result) {}).catch(function (error) {});
                        dispatch("create_role", { name: 'Skolastabo'}).then(function (result) {}).catch(function (error) {});
                        dispatch("create_role", { name: 'Alumni'}).then(function (result) {}).catch(function (error) {});
                        dispatch("create_role", { name: 'Salzburg'}).then(function (result) {}).catch(function (error) {});
                        dispatch("create_role", { name: 'Padova'}).then(function (result) {}).catch(function (error) {});
                        dispatch("create_role", { name: 'Trient'}).then(function (result) {}).catch(function (error) {});
                        dispatch("create_role", { name: 'Verona'}).then(function (result) {}).catch(function (error) {});
                        dispatch("create_role", { name: 'Bologna'}).then(function (result) {}).catch(function (error) {});
                        dispatch("create_role", { name: 'AndereStädte'}).then(function (result) {}).catch(function (error) {});
                        dispatch("create_role", { name: 'Graz'}).then(function (result) {}).catch(function (error) {});

                        
                        for (let index = 0; index < length; index++) {
                            let element = rows[index];
                            if (index != 0) {
                                try {
                                    let columns = element.split(";");

                                    let temp = columns[31].replace(/\"\"/g, '"');
                                    let payments = JSON.parse(temp.substr(1, temp.length - 2))

                                    let years = [];
                                    for (let y of Object.keys(payments.payments) ) {
                                        years.push(Number(y));
                                    };

                                    let last = Math.max.apply(null, years);

                                    let city = columns[7].replace(/\"/g, '');
                                    let country = columns[8].replace(/\"/g, '');
                                    let street = columns[5].replace(/\"/g, '');
                                    let zip  = columns[6].replace(/\"/g, '');
                                    let email = columns[4].replace(/\"/g, '');
                                    let firstname = columns[2].replace(/\"/g, '');
                                    let lastname = columns[3].replace(/\"/g, '');
                                    let gender = columns[25].replace(/\"/g, '');
                                    let location = columns[19].replace(/\"/g, '').replace(/ /g,'');
                                    let membership_due = columns[32].replace(/\"/g, '').replace(/ /g, 'T') + '.000000';
                                    let member_since = payments.payments[last].replace(/\"/g, '').replace(/ /g, 'T') + '.000000';
                                    let newsletter = columns[22].replace(/\"/g, '');

                                    switch (location) {
                                        case 'Wien':
                                            location = 'Wien';
                                            break;
                                        case 'Bozen-allgemein':
                                            location = 'Bozen';
                                            break;
                                        case 'Bozen-UniBZ':
                                            location = 'Bozen';
                                        case 'Bozen':
                                            location = 'Bozen';
                                            break;
                                        case 'Bozen-unibz':
                                            location = 'Bozen';
                                            break;
                                        case 'Trient':
                                            location = 'Trient';
                                            break;
                                        case 'Graz':
                                            location = 'Graz';
                                            break;
                                    } 

                                    let member = {
                                        address: {
                                            city: city,
                                            country: country,
                                            street: street,
                                            zip: zip
                                        },
                                        send: !(email == '' || email == null),
                                        email: (email == '' || email == null) ? 'default@default.default' + index : email,
                                        firstname: (firstname == '' || firstname == null) ? 'default_firstname' : firstname,
                                        lastname: (lastname == '' || firstname == null) ? 'default_lastname' : lastname,
                                        gender: (gender == '' || gender == null) ? 'unknow' : gender,
                                        location: {
                                            name: (location == '' || location == null) ? 'Bolzen' : location
                                        },
                                        member_since: member_since,
                                        membership_due: membership_due
                                    };

                                
                                
                                    dispatch('create_member', member).then(function(result) {

                                        if (Number(newsletter) == 1) {
                                            dispatch(
                                            'add_newsletters_to_user',
                                            { id: result.data.id, data: [{ name: 'Medizin' }] })
                                            .then(function (response) { })
                                            .catch(function (error) {})
                                        }
                                        //console.log('succ');
                                    }).catch(function(err) {
                                        console.log(member);
                                    });

                                    if (index % 100 == 0) {
                                        await timeout(1000);
                                    }

                                } catch (err) {
                                    console.log(err);
                                }
                        
                            }

                        }
                    };
                })(this.file);

                reader.readAsText(this.file);
            },
            change_password() {
                if (!this.error) {
                    this.$store.dispatch('set_password', { old_password: this.old_password, new_password: this.new_password })
                        .then(function(response) {
                            if (response.status === 200) {
                                this.$bvToast.toast(this.$i18n.t('texts.element_saved_successfully', { element: 'Password' }), {
                                    title: this.$i18n.t('texts.saved'),
                                    variant: 'success',
                                    autoHideDelay: 5000,
                                    appendToast: true
                                });

                                this.old_password = '';
                                this.new_password = '';
                                this.new_password_confirm = '';
                            }
                        }.bind(this))
                        .catch(function(err) {
                            this.$bvToast.toast(this.$i18n.t('texts.element_not_saved', { element: 'Password' }), {
                                title: this.$i18n.t('texts.error_while_saving'),
                                autoHideDelay: 5000,
                                variant: 'danger',
                                appendToast: true
                            });
                        }.bind(this));
                }
            },
            validateState(state) {
                this.error = !state;
                return state;
            },
            validateRepeatState(state) {
                this.error = !(state && this.new_password === this.new_password_confirm);
                return !this.error;
            },
            validateEmailState(state) {
                this.email_error = !state;
                return state;
            },
            save_settings() {
                if (!this.email_error) {
                    this.saving = true;

                    let temp = {
                        email_username: this.email,
                        email_password: this.password,
                        welcome_subject: this.welcome_subject,
                        welcome_text: this.welcome_text,
                        reminder_subject: this.reminder_subject,
                        reminder_text: this.reminder_text,
                        extend_subject: this.extend_subject,
                        extend_text: this.extend_text
                    };

                    console.log(temp);

                    this.$store.dispatch('save_settings', temp)
                        .then(function(response) {

                            this.$bvToast.toast(this.$i18n.t('texts.settings_saved_successfully'), {
                                title: this.$i18n.t('texts.saved'),
                                variant: 'success',
                                autoHideDelay: 5000,
                                appendToast: true
                            });

                            setTimeout(function() {
                                this.saving = false;
                            }.bind(this), 1000);
                        }.bind(this)).catch(function(err) {
                            console.log(err);
                            this.saving = false;
                            this.$store.dispatch('download_users');
                            this.$bvToast.toast(this.$i18n.t('texts.settings_not_saved'), {
                                title: this.$i18n.t('texts.error_while_saving'),
                                autoHideDelay: 5000,
                                variant: 'danger',
                                appendToast: true
                            });
                        }.bind(this));
                } else {
                    this.$bvToast.toast(this.$i18n.t('texts.errors_in_input_fields'), {
                        title: this.$i18n.t('texts.errors'),
                        autoHideDelay: 5000,
                        variant: 'danger',
                        appendToast: true
                    });
                }
            },
            resend_emails () {
                this.$store.dispatch('resend_emails', {})
                    .then(function (result) {
                        this.$bvToast.toast(this.$i18n.t('texts.success'), {
                            title: this.$i18n.t('texts.success'),
                            autoHideDelay: 5000,
                            variant: 'success',
                            appendToast: true
                        });
                    }.bind(this))
                    .catch(function (error) {
                        this.$bvToast.toast(this.$i18n.t('texts.failed'), {
                            title: this.$i18n.t('texts.failed'),
                            autoHideDelay: 5000,
                            variant: 'danger',
                            appendToast: true
                        });
                    });
            }
        },
        template: ` 
                    <div class="m-1">
                        <b-card bg-variant="light" class="m-1">
                            <b-form @submit.prevent="save_settings" v-if="is_admin_user">
                                <b-card bg-variant="light" class="m-1">
                                    <b-form-group 
                                    label-cols-lg="3"
                                    :label="$t('texts.email_settings')"
                                    label-size="lg"
                                    label-class="font-weight-bold pt-0"
                                    class="mb-0"
                                    >
                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.email')"
                                        label-align-sm="right"
                                        label-for="email"
                                        >
                                            <ValidationProvider
                                            :rules="{ email: true }"
                                            v-slot="{ valid }"
                                            >
                                                <b-form-input 
                                                id="email" 
                                                name="email"
                                                v-model="email" 
                                                :state="validateEmailState(valid)"
                                                aria-describedby="email-live-feedback"></b-form-input>

                                                <b-form-invalid-feedback id="email-live-feedback">
                                                {{ $t('texts.required_email') }}
                                                </b-form-invalid-feedback>
                                            </ValidationProvider>

                                        </b-form-group>

                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.password')"
                                        label-align-sm="right"
                                        label-for="password"
                                        >
                                            <b-form-input id="password" type="password" v-model="password"></b-form-input>
                                        </b-form-group>

                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.welcome_subject')"
                                        label-align-sm="right"
                                        label-for="welcome-subject"
                                        >
                                            <b-form-input id="welcome-subject" v-model="welcome_subject"></b-form-input>
                                        </b-form-group>

                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.welcome_text')"
                                        label-align-sm="right"
                                        label-for="welcome-text"
                                        >
                                            <b-form-textarea 
                                                id="welcome-text" 
                                                rows="5"
                                                max-rows="15"
                                                v-model="welcome_text"></b-form-textarea>
                                        </b-form-group>

                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.reminder_subject')"
                                        label-align-sm="right"
                                        label-for="reminder-subject"
                                        >
                                            <b-form-input id="reminder-subject" v-model="reminder_subject"></b-form-input>
                                        </b-form-group>

                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.reminder_text')"
                                        label-align-sm="right"
                                        label-for="reminder-text"
                                        >
                                            <b-form-textarea 
                                                id="reminder-text"  
                                                rows="5"
                                                max-rows="15"
                                                v-model="reminder_text"></b-form-textarea>
                                        </b-form-group>

                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.extend_subject')"
                                        label-align-sm="right"
                                        label-for="extend-subject"
                                        >
                                            <b-form-input id="extend-subject" v-model="extend_subject"></b-form-input>
                                        </b-form-group>

                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.extend_text')"
                                        label-align-sm="right"
                                        label-for="extend-text"
                                        >
                                            <b-form-textarea 
                                                id="extend-text"  
                                                rows="5"
                                                max-rows="15"
                                                v-model="extend_text"></b-form-textarea>
                                        </b-form-group>
                                    </b-form-group>
                                    <b-button-group class="float-right m-1">
                                        <b-button variant="success" type="submit">{{ $t('texts.save') }}</b-button>
                                    </b-button-group>
                                </b-card>
                            </b-form>

                            <b-form @submit.prevent="change_password">
                                <b-card bg-variant="light" class="m-1">
                                    <b-form-group 
                                    label-cols-lg="3"
                                    :label="$t('texts.password')"
                                    label-size="lg"
                                    label-class="font-weight-bold pt-0"
                                    class="mb-0"
                                    >
                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.old_password')"
                                        label-align-sm="right"
                                        label-for="old_password"
                                        >
                                            <ValidationProvider
                                            :rules="{ required: true}"
                                            v-slot="{ valid }"
                                            >
                                                <b-form-input 
                                                id="old_password" 
                                                name="old_password"
                                                v-model="old_password"
                                                type="password"
                                                :state="validateState(valid)"
                                                aria-describedby="input-old-password-live-feedback"></b-form-input>
                                                <b-form-invalid-feedback id="input-old-password-live-feedback">
                                                    {{ $t('texts.required')  }}
                                                </b-form-invalid-feedback>
                                            </ValidationProvider>
                                        </b-form-group>

                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.new_password')"
                                        label-align-sm="right"
                                        label-for="new_password"
                                        >
                                            <ValidationProvider
                                            :rules="{ required: true}"
                                            v-slot="{ valid }"
                                            >
                                                <b-form-input 
                                                id="new_password" 
                                                name="new_password"
                                                v-model="new_password" 
                                                type="password"
                                                :state="validateState(valid)"
                                                aria-describedby="input-new-password-live-feedback"></b-form-input>
                                                <b-form-invalid-feedback id="input-new-password-live-feedback">
                                                    {{ $t('texts.required')  }}
                                                </b-form-invalid-feedback>
                                            </ValidationProvider>
                                        </b-form-group>

                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.repeat_password')"
                                        label-align-sm="right"
                                        label-for="new_password_confirm"
                                        >
                                            <ValidationProvider
                                            :rules="{ required: true}"
                                            v-slot="{ valid }"
                                            >
                                            <b-form-input 
                                            id="new_password_confirm" 
                                            name="new_password_confirm"
                                            v-model="new_password_confirm" 
                                            type="password"
                                            :state="validateRepeatState(valid)"
                                            aria-describedby="input-repeat-password-live-feedback"></b-form-input>
                                            <b-form-invalid-feedback id="input-repeat-password-live-feedback">
                                                {{ $t('texts.required_and_equal_password')  }}
                                            </b-form-invalid-feedback>
                                        </ValidationProvider>
                                        </b-form-group>
                                    </b-form-group>
                                    <b-button-group class="float-right m-1">
                                        <b-button variant="success" type="submit">{{ $t('texts.save') }}</b-button>
                                    </b-button-group>
                                </b-card>
                            </b-form>

                            <b-form @submit.prevent="import_file" v-if="is_admin_user">
                                <b-card bg-variant="light" class="m-1">
                                    <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.import')"
                                        label-align-sm="right"
                                        label-for="import_field"
                                        >
                                            <b-form-file
                                            id="import_field"
                                            name="import_field"
                                            v-model="file"
                                            :state="Boolean(file)"
                                            :placeholder="$t('texts.choose_or_drop')"
                                            :drop-placeholder="$t('texts.drop_file')"
                                        ></b-form-file>
                                    </b-form-group>
                                    <b-button-group class="float-right m-1">
                                        <b-button variant="success" type="submit">{{ $t('texts.import') }}</b-button>
                                        <b-button @click="resend_emails">{{ $t('texts.resend') }}</b-button>
                                    </b-button-group>
                                </b-card>
                            </b-form>
                        </b-card>
                    </div>
                    `
    };

    return container;
});