'use strict'

define(function (require, exports, module) {
    let users = require('users');
    let search = require('search_component');

    let user_site = {
        created () {
            this.$store.dispatch('download_users');
            this.$store.dispatch('download_roles');
        },
        components: {
            'user': users,
            'search': search
        },
        data: function () {
            return {
                users_data: []
            };
        },
        computed: {
            users() {
                return this.$store.getters.get_users;
            },
            columns() {
                let columns = [];
                columns.push({ value: 'id', text: this.$i18n.t('texts.id') });
                columns.push({ value: 'username', text: this.$i18n.t('texts.username') });
                columns.push({ value: 'email', text: this.$i18n.t('texts.email')});
                columns.push({ value: 'firstname', text: this.$i18n.t('texts.firstname') });
                columns.push({ value: 'lastname', text: this.$i18n.t('texts.lastname') });
                columns.push({ value: 'role', text: this.$i18n.t('texts.role') });

                return columns;
            },
            computed_users_data() {
                this.users_data = this.$store.getters.get_users_data;
            },
            data() {
                return this.users_data;
            }
        },
        methods: {
            search_filter(value) {
                this.users_data = this.$store.getters.get_users_data.filter(function (currentValue, index, arr) {
                    if (value.selected == 'role') {
                        return (currentValue[value.selected].name.toLocaleLowerCase() + "").startsWith(value.term.toLocaleLowerCase());
                    } else {
                        return (currentValue[value.selected] + "").toLocaleLowerCase().startsWith(value.term.toLocaleLowerCase());
                    }
                });
            },
            create() {
                this.$router.push({ name: 'users_edit' });
            }
        },
        template: `
                    <div class="m-1">
                        <div class="row m-1">
                            <div class="col-sm-1">
                                <b-button variant="success" @click.prevent="create">{{ $t('texts.create')}}</b-button>
                            </div>
                            <div class="col-sm-11">
                                <search :columns="columns" @search_input="search_filter" default="email" :show_lastname="false"></search>
                            </div>
                        </div>
                        <user :properties="users" :data="data" :refresh="computed_users_data"></user>
                    </div>
                    `
    };

    return user_site;
});