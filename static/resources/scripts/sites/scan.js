'use strict';

define(function(require, exports, module) {
  let scan_container = {
    data: function() {
      return {
        scanner: null,
      };
    },
    mounted: function() {
      let scanner =
          new Instascan.Scanner({video: document.getElementById('scanner')});

      scanner.addListener('scan', function(content) {
        console.log(content);

        this.$store.dispatch('check_membership', {id: content})
            .then(function(response) {
              this.$bvToast.toast(
                  (response.data.firstname + ' ' + response.data.lastname + ': ' + response.data.location.name + '--' + new Date(response.data.membership_due).toLocaleDateString()), {
                    title: this.$i18n.t('authenticated'),
                    variant: 'success',
                    toaster: 'b-toaster-top-full',
                    solid: false,
                    append: true
                  })
            }.bind(this))
            .catch(function(err) {
              this.$bvToast.toast(this.$i18n.t('texts.no_membership'), {
                title: this.$i18n.t('text.not_authenticated'),
                variant: 'danger',
                toaster: 'b-toaster-top-full',
                solid: false,
                append: true
              })
            }.bind(this));
        // TODO: show toast
      }.bind(this));

      Instascan.Camera.getCameras()
          .then(function(cameras) {
            if (cameras.length > 0) {

              scanner.start(cameras[cameras.length > 1 ? 1 : 0]);
            } else {
              console.error('No cameras found.');
            }
          }.bind(this))
          .catch(function(e) {
            console.error(e);
          });

      this.scanner = scanner;
    },
    destroyed: function() {
      this.scanner.stop()
    },
    data: function() {
      return {
        current: 0
      }
    },
    template: ` <div>
                        <div class="embed-responsive embed-responsive-1by1">
                            <video class="embed-responsive-item" id="scanner" playsinline></video>
                        </div>
                    </div>
                    `
  };

  return scan_container;
});