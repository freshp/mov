'use strict';

define(function (require, exports, module) {
    let dashboard = {
        created() {
            this.$store.dispatch('download_users');
            this.$store.dispatch('download_members');
            this.$store.dispatch('download_newsletters');
            this.$store.dispatch('download_roles');
        },
        computed: {
            get_total_members() {
                return this.$store.getters.get_members_data(false).length;
            },
            get_total_members_active() {
                return this.$store.getters.get_members_data(true).length;
            },
            get_data() {
                let data = {};

                let id = 0;
                for (let d of this.$store.getters.get_members_data(false)) {
                    if (data[d.location.name] == null) {
                        data[d.location.name] = {
                            id: id,
                            active: 0,
                            total: 0,
                            name: d.location.name
                        };

                        id++;
                    }

                    data[d.location.name].total++;

                    if (new Date(d.membership_due) > new Date()) {
                        data[d.location.name].active++;
                    }
                }

                return data;
            },
        },
        methods: {
            calc_perc: (a, b) => {
                if (b != 0) {
                    return ((a / b) * 100).toFixed(2);
                } else {
                    return "NA";
                }

            },
            trunc_number: (num) => {
                return Math.trunc(Number(num));
            }
        },
        template:   `
                    <div>
                        <b-card v-for="l in get_data" :key="l.id" bg-variant="light" class="m-1" align="center">
                            <template v-slot:header>
                                <h6 class="mb-0">{{ l.name }}</h6>
                            </template>
                            <h5 class="text-left">{{$t('texts.total_members')}}</h5>
                            <b-progress :max="get_total_members" variant="dark" show-progress animated height="2rem">
                                <b-progress-bar :value="trunc_number(l.total)">
                                    <div class="progress-bar-text" ><strong>{{ (calc_perc(l.total, get_total_members)) + "% " }}{{ trunc_number(l.total) }} / {{ trunc_number(get_total_members) }}</strong></div>
                                </b-progress-bar>
                            </b-progress>
                            <h5 class="text-left">{{ $t('texts.active_members') }}</h5>
                            <b-progress :value="l.active" :max="trunc_number(get_total_members_active)" variant="dark" show-progress animated :precision="2">
                                <b-progress-bar :value="l.active">
                                    <div class="progress-bar-text"><strong>{{ (calc_perc(l.active, get_total_members_active)) + "% " }}{{ trunc_number(l.active) }} / {{ trunc_number(get_total_members_active) }}</strong></div>
                                </b-progress-bar>
                            </b-progress>
                        </b-card>
                    </div>
                    `
    };

    return dashboard;
});