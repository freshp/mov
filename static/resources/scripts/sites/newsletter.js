'use strict'

define(function (require, exports, module) {
    let newsletter = require('newsletter');
    let search = require('search_component');

    let newsletter_site = {
        created() {
            this.$store.dispatch('download_newsletters');
        },
        components: {
            'newsletter': newsletter,
            'search': search
        },
        data: function () {
            return {
                newsletters_data: []
            };
        },
        computed: {
            newsletters() {
                return this.$store.getters.get_newsletters;
            },
            columns() {
                let columns = [];
                columns.push({ value: 'id', text: this.$i18n.t('texts.id') });
                columns.push({ value: 'name', text: this.$i18n.t('texts.name') });

                return columns;
            },
            computed_newsletter_data() {
                this.newsletters_data = this.$store.getters.get_newsletters_data;
            },
            data () {
                return this.newsletters_data;
            }
        },
        methods: {
            search_filter(value) {
                this.newsletters_data = this.$store.getters.get_newsletters_data.filter(function (currentValue, index, arr) {
                    return (currentValue[value.selected] + "").toLocaleLowerCase().startsWith(value.term.toLocaleLowerCase());
                });
            },
            create() {
                this.$router.push({ name: 'newsletters_edit' })
            }
        },
        template: `
                    <div class="m-1">
                        <div class="row m-1">
                            <div class="col-sm-1">
                                <b-button variant="success" @click.prevent="create">{{ $t('texts.create')}}</b-button>
                            </div>
                            <div class="col-sm-11">
                                <search :columns="columns" @search_input="search_filter" default="name" :show_lastname="false"></search>
                            </div>
                        </div>
                        <newsletter :properties="newsletters" :data="data" :refresh="computed_newsletter_data"></newsletter>
                    </div>
                    `
    };

    return newsletter_site;
});