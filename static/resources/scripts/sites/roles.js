'use strict';

define(function (require, exports, module) {
    let roles = require('roles');
    let search = require('search_component');

    let roles_site = {
        created() {
            this.$store.dispatch('download_roles');
        },
        components: {
            'roles': roles,
            'search': search
        },
        data: function () {
            return {
                roles_data: [],
                filter: {
                    selected: '',
                    term: ''
                }
            }
        },
        computed: {
            roles() {
                return this.$store.getters.get_roles;
            },
            columns() {
                let columns = [];
                columns.push({ value: 'id', text: this.$i18n.t('texts.id') });
                columns.push({ value: 'name', text: this.$i18n.t('texts.name') });

                return columns;
            },
            computed_roles_data() {
                this.roles_data = this.$store.getters.get_roles_data;
            },
            data() {
                return this.roles_data;
            }
        },
        methods: {
            search_filter(value) {
                this.roles_data = this.$store.getters.get_roles_data.filter(function (currentValue, index, arr) {
                    return (currentValue[value.selected] + "").toLocaleLowerCase().startsWith(value.term.toLocaleLowerCase());
                });
            },
            create() {
                this.$router.push({ name: 'roles_edit' });
            }
        },
        watch: {

        },
        template: `
                    <div class="m-1">
                        <div class="row m-1">
                            <div class="col-sm-1">
                                <b-button variant="success" @click.prevent="create">{{ $t('texts.create')}}</b-button>
                            </div>
                            <div class="col-sm-11">
                                <search :columns="columns" @search_input="search_filter" default="name" :show_lastname="false"></search>
                            </div>
                        </div>
                        <roles :properties="roles" :data="data" :refresh="computed_roles_data"></roles>
                    </div>
                    `
    };

    return roles_site;
});