define(function (requires, exports, module) {
    let not_authorized = {
        template: `<div class="text-center mt-5"><h3>{{ $t('texts.not_authorized') }}</h3></div>`
    };

    return not_authorized;
});