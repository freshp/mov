'use strict';

define(function (require, exports, module) {
    let send_email_link = {
        props: {
            to: {
                type: String,
                default: ''
            },
            emails: {
                type: Array,
                default: () => []
            }
        },
        computed: {
            get_link: function () {
                let temp = 'mailto:'+ this.$props.to +'?subject=&bcc='
                for (let e of this.$props.emails) {
                        temp += e + ';';
                }
                return temp;
            }
        },
        template: `<a :href="get_link">{{$t('texts.send_newsletter')}}</a>`
    }


    let newsletter_edit = {
        components: {
            'email-send': send_email_link
        },
        created() {
            if (this.$route.query.newsletter_id != null) {
                this.newsletter = this.$store.getters.get_newsletter_with_id(this.$route.query.newsletter_id);
                this.creating = false;

                this.downloading = true;
                this.$store.dispatch('get_newsletter', { id: this.$route.query.newsletter_id })
                    .then(function (response) {
                        if (response.status === 200) {
                            this.downloading = false;
                            this.newsletter = response.data;

                            this.$store.dispatch('get_newsletters_email', { name: this.newsletter.name})
                                .then(function (result) {
                                    this.emails = result.data;
                                }.bind(this))
                                .catch(function (error) {
                                    console.log(error)
                                });
                        }
                    }.bind(this))
                    .catch(function (err) {
                        this.downloading = false;
                        this.error = true;
                    }.bind(this));
            } else {
                this.creating = true;
            }
        },
        data: function () {
            return {
                saving: false,
                creating: true,
                deleting: false,
                downloading: false,
                error: true,
                newsletter: {
                    id: '',
                    email: '',
                    name: ''
                },
                emails: []
            };
        },
        methods: {
            create_newsletter() {
                if (!this.error) {
                    this.saving = true;

                    let action = this.creating ? 'create_newsletter' : 'update_newsletter';

                    this.$store.dispatch(action, this.newsletter)
                        .then(function (response) {


                            this.$store.dispatch('download_newsletters');
                            this.$bvToast.toast(this.$i18n.t('texts.element_saved_successfully', { element: this.newsletter.name }), {
                                title: this.$i18n.t('texts.saved'),
                                variant: 'success',
                                autoHideDelay: 5000,
                                appendToast: true
                            });

                            setTimeout(function () {
                                this.saving = false;
                                this.$router.push({ name: 'newsletters' });
                            }.bind(this), 1000);
                        }.bind(this)).catch(function (err) {
                            console.log(err);
                            this.saving = false;
                            this.$store.dispatch('download_newsletters');
                            this.$bvToast.toast(this.$i18n.t('texts.element_not_saved', { element: this.newsletter.name }), {
                                title: this.$i18n.t('texts.error_while_saving'),
                                autoHideDelay: 5000,
                                variant: 'danger',
                                appendToast: true
                            });
                        }.bind(this));
                }
            },
            initiate_delete() {
                this.deleting = true;
            },
            cancel_delete() {
                this.deleting = false;
            },
            delete_newsletter() {
                this.saving = true;
                this.$store.dispatch('delete_newsletter', this.newsletter)
                    .then(function (response) {


                        this.$store.dispatch('download_newsletters');
                        this.$bvToast.toast(this.$i18n.t('texts.element_deleted_successfully', { element: this.newsletter.name }), {
                            title: this.$i18n.t('texts.deleted'),
                            variant: 'success',
                            autoHideDelay: 5000,
                            appendToast: true
                        });

                        setTimeout(function () {
                            this.saving = false;
                            this.deleting = false;
                            this.$router.push({ name: 'newsletters' });
                        }.bind(this), 1000);
                    }.bind(this)).catch(function (err) {
                        console.log(err);
                        this.saving = false;
                        this.deleting = false;
                        this.$store.dispatch('download_roles');
                        this.$bvToast.toast(this.$i18n.t('texts.element_not_deleted', { element: this.newsletter.name }), {
                            title: this.$i18n.t('texts.error_while_deleting'),
                            autoHideDelay: 5000,
                            variant: 'danger',
                            appendToast: true
                        });
                    }.bind(this));
            },
            validateState(error) {
                if (error) {
                    this.error = false;
                    return true;
                } else {
                    this.error = true;
                    return false;
                }
            },
        },
        template: `
                    <div>
                        <b-card bg-variant="light" class="m-1">
                            <div class="text-center" v-if="downloading">
                                <b-spinner variant="success" type="grow"></b-spinner>
                            </div>
                            <b-form @submit.prevent="create_newsletter">
                                <b-card bg-variant="light" class="m-1" v-show="!downloading">
                                    <b-form-group 
                                    label-cols-lg="3"
                                    :label="$t('texts.newsletter')"
                                    label-size="lg"
                                    label-class="font-weight-bold pt-0"
                                    class="mb-0"
                                    >
                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.name')"
                                        label-align-sm="right"
                                        label-for="nested-newsletter-name"
                                        >
                                        <ValidationProvider
                                        rules="required"
                                        v-slot="{ valid }"
                                        >
                                            <b-form-input 
                                                id="nested-newsletter-name"
                                                name="nested-newsletter-name" 
                                                v-model="newsletter.name"
                                                :state="validateState(valid)"
                                                aria-describedby="input-name-live-feedback">
                                            </b-form-input>
                                            <b-form-invalid-feedback id="input-name-live-feedback">
                                                {{ $t('texts.required') }}
                                            </b-form-invalid-feedback>
                                        </ValidationProvider>
                                        </b-form-group>
                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.email')"
                                        label-align-sm="right"
                                        label-for="nested-newsletter-email"
                                        >
                                        <ValidationProvider
                                        rules="required"
                                        v-slot="{ valid }"
                                        >
                                            <b-form-input 
                                                id="nested-newsletter-email"
                                                name="nested-newsletter-email" 
                                                v-model="newsletter.email"
                                                :state="validateState(valid)"
                                                aria-describedby="input-name-live-feedback">
                                            </b-form-input>
                                            <b-form-invalid-feedback id="input-name-live-feedback">
                                                {{ $t('texts.required') }}
                                            </b-form-invalid-feedback>
                                        </ValidationProvider>
                                        </b-form-group>
                                    </b-form-group>
                                    <b-button-group class="float-right m-1" v-if="!deleting">
                                        <b-button v-if="!creating" variant="outline-primary"><email-send :to="newsletter.email" :emails="emails"></email-send></b-button>
                                        <b-button variant="danger" @click.prevent="initiate_delete" v-if="!creating">{{ $t('texts.delete') }}</b-button>
                                        <b-button variant="success" type="submit" :disabled="error || saving" >
                                            {{ $t('texts.save') }}
                                        </b-button>
                                    </b-button-group>
                                    <b-button-group class="float-right m-1" v-if="deleting" >
                                        <b-button variant="danger" @click.prevent="cancel_delete" :disabled="saving">{{ $t('texts.no') }}</b-button>
                                        <b-button variant="success" @click.prevent="delete_newsletter" :disabled="saving">{{ $t('texts.yes') }}</b-button>
                                    </b-button-group>
                                    <span class="float-right m-1 vcenter" v-if="deleting" style="padding-top: 5px;">{{ $t('texts.you_sure') }}</span>
                                </b-card>
                            </b-form>
                        </b-card>
                    </div>`
    };

    return newsletter_edit;
});