'use strict';

define(function (require, exports, module) {
    let login_component = require('login_component');
    let store = require('store');

    let login_page = {
        created() {
            if (this.$store.getters.is_logged_in == "true") {
                this.$router.push('dashboard');
            }
        },
        components: {
            'login': login_component
        },
        computed: {
            login_component: function () {
                return store.getters.get_login_component
            }
        },
        template:   `
                        <div class="m-5">
                            <login :properties="login_component" class="mt-5" ></login>
                        </div>
                    `
    };

    return login_page;
});