'use strict';

define(function (require, exports, module) {
    let send_email_link = {
        props: {
            to: {
                type: String,
                default: ''
            },
            emails: {
                type: Array,
                default: () => []
            }
        },
        computed: {
            get_link: function () {
                let temp = 'mailto:'+ this.$props.to +'?subject=&bcc='
                for (let e of this.$props.emails) {
                        temp += e + ';';
                }
                return temp;
            }
        },
        template: `<a :href="get_link">{{$t('texts.send_newsletter')}}</a>`
    }

    let roles_edit = {
        components: {
            'email-send': send_email_link
        },
        created() {
            if (this.$route.query.role_id != null) {
                this.creating = false;

                this.downloading = true;
                this.$store.dispatch('get_role', { id: this.$route.query.role_id })
                    .then(function (response) {
                        if (response.status === 200) {
                            this.downloading = false;
                            this.role = response.data;

                            this.$store.dispatch('get_roles_email', { name: this.role.name })
                                .then(function (result) {
                                    this.emails = result.data;
                                }.bind(this))
                                .catch(function (error) {
                                    console.log(error)
                                });
                        }
                    }.bind(this))
                    .catch(function (err) {
                        this.downloading = false;
                        this.error = true;
                    }.bind(this));
            } else {
                this.creating = true;
            }
        },
        data: function () {
            return {
                creating: true,
                saving: false,
                deleting: false,
                downloading: false,
                error: true,
                role: {
                    id: '',
                    name: '',
                    email: ''
                },
                emails: []
            };
        },
        methods: {
            create_role() {
                if (!this.error) {
                    this.saving = true;

                    let action = this.creating ? 'create_role' : 'update_role';

                    this.$store.dispatch(action, this.role)
                        .then(function (response) {


                            this.$store.dispatch('download_roles');
                            this.$bvToast.toast(this.$i18n.t('texts.element_saved_successfully', { element: this.role.name }), {
                                title: this.$i18n.t('texts.saved'),
                                variant: 'success',
                                autoHideDelay: 5000,
                                appendToast: true
                            });

                            setTimeout(function () {
                                this.saving = false;
                                this.$router.push({ name: 'roles' });
                            }.bind(this), 1000);
                        }.bind(this)).catch(function (err) {
                            console.log(err);
                            this.saving = false;
                            this.$store.dispatch('download_roles');
                            this.$bvToast.toast(this.$i18n.t('texts.element_not_saved', { element: this.role.name }), {
                                title: this.$i18n.t('texts.error_while_saving'),
                                autoHideDelay: 5000,
                                variant: 'danger',
                                appendToast: true
                            });
                        }.bind(this));
                };
            },
            initiate_delete() {
                this.deleting = true;
            },
            cancel_delete() {
                this.deleting = false;
            },
            delete_role() {
                this.$store.dispatch('delete_role', this.role)
                    .then(function (response) {
                        this.$store.dispatch('download_roles');
                        this.$bvToast.toast(this.$i18n.t('texts.element_deleted_successfully', { element: this.role.name }), {
                            title: this.$i18n.t('texts.deleted'),
                            variant: 'success',
                            autoHideDelay: 5000,
                            appendToast: true
                        });

                        setTimeout(function () {
                            this.deleting = false;
                            this.saving = false;
                            this.$router.push({ name: 'roles' });
                        }.bind(this), 1000);
                    }.bind(this)).catch(function (err) {
                        console.log(err);
                        this.deleting = false;
                        this.saving = false;
                        this.$store.dispatch('download_roles');
                        this.$bvToast.toast(this.$i18n.t('texts.element_not_deleted', { element: this.role.name }), {
                            title: this.$i18n.t('texts.error_while_deleting'),
                            autoHideDelay: 5000,
                            variant: 'danger',
                            appendToast: true
                        });
                    }.bind(this));
            },
            validateState(error) {
                if (error) {
                    this.error = false;
                    return true;
                } else {
                    this.error = true;
                    return false;
                }
            }
        },
        template: `
                    <div>
                        <b-card bg-variant="light" class="m-1">
                            <div class="text-center" v-if="downloading">
                                <b-spinner variant="success" type="grow"></b-spinner>
                            </div>
                            <b-form @submit.prevent="create_role">
                                <b-card bg-variant="light" class="m-1" v-show="!downloading">
                                    <b-form-group 
                                    label-cols-lg="3"
                                    :label="$t('texts.role')"
                                    label-size="lg"
                                    label-class="font-weight-bold pt-0"
                                    class="mb-0"
                                    >
                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.name')"
                                        label-align-sm="right"
                                        label-for="nested-role"
                                        >
                                        <ValidationProvider
                                        :rules="{ required: true }"
                                        v-slot="{ valid }"
                                        >
                                            <b-form-input 
                                                id="nested-role" 
                                                name="nested-role"
                                                v-model="role.name" 
                                                :state="validateState(valid)"
                                                aria-describedby="input-name-live-feedback">
                                                </b-form-input>
                                            <b-form-invalid-feedback id="input-name-live-feedback">
                                                {{ $t('texts.required') }}
                                            </b-form-invalid-feedback>
                                        </ValidationProvider>
                                        </b-form-group>

                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.email')"
                                        label-align-sm="right"
                                        label-for="nested-roles-email"
                                        >
                                        <ValidationProvider
                                        rules="required"
                                        v-slot="{ valid }"
                                        >
                                            <b-form-input 
                                                id="nested-roles-email"
                                                name="nested-roles-email" 
                                                v-model="role.email"
                                                :state="validateState(valid)"
                                                aria-describedby="input-name-live-feedback">
                                            </b-form-input>
                                            <b-form-invalid-feedback id="input-name-live-feedback">
                                                {{ $t('texts.required') }}
                                            </b-form-invalid-feedback>
                                        </ValidationProvider>
                                        </b-form-group>

                                    </b-form-group>
                                    <b-button-group class="float-right m-1" v-if="!deleting">
                                        <b-button v-if="!creating" variant="outline-primary" ><email-send :emails="emails" :to="role.email"></email-send></b-button>
                                        <b-button variant="danger" @click.prevent="initiate_delete" v-if="!creating">{{ $t('texts.delete') }}</b-button>
                                        <b-button variant="success" type="submit" :disabled="error || saving">{{ $t('texts.save') }}</b-button>
                                    </b-button-group>  
                                    <b-button-group class="float-right m-1" v-if="deleting" >
                                        <b-button variant="danger" @click.prevent="cancel_delete" :disabled="saving">{{ $t('texts.no') }}</b-button>
                                        <b-button variant="success" @click.prevent="delete_role" :disabled="saving">{{ $t('texts.yes') }}</b-button>
                                    </b-button-group>
                                    <span class="float-right m-1 vcenter" v-if="deleting" style="padding-top: 5px;">{{ $t('texts.you_sure') }}</span>      
                                </b-card>
                            </b-form>
                        </b-card>
                    </div>`
    };

    return roles_edit;
});