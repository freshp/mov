define(function (require, exports, module) {
    let not_found = {
        template: `<div class="text-center mt-5"><h3>{{ $t('texts.not_found') }}</h3></div>`
    };

    return not_found;
});