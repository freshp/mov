'use strict';

define(function (require, exports, module) {

  let format_date = function (date) {
    let temp = date.getFullYear() + '-' +
      ((Number(date.getMonth()) < 9) ? '0' + (date.getMonth() + 1) :
        date.getMonth() + 1) +
      '-' +
      ((Number(date.getDate()) < 9) ? '0' + date.getDate() : date.getDate());

    return temp;
  };

  let get_date_in_one_year = function () {
    var date = new Date();
    date.setFullYear(date.getFullYear() + 1);
    let temp = format_date(date);
    return temp;
  }

  let members_edit = {
    created() {
      this.$store.dispatch('download_newsletters');
      this.$store.dispatch('download_roles');
      if (this.$route.query.member_id != null) {
        this.creating = false;
        this.downloading = true;
        this.$store.dispatch('get_member', { id: this.$route.query.member_id })
          .then(function (response) {
            if (response.status === 200) {
              this.downloading = false;
              this.member = response.data;

              this.member.member_since =
                format_date(new Date(response.data.member_since));
              this.member.membership_due =
                format_date(new Date(response.data.membership_due));

              this.newsletters = response.data.newsletters == null ?
                [] :
                response.data.newsletters;
            }
          }.bind(this))
          .catch(function (err) {
            this.downloading = false;
            this.error = true;
          }.bind(this));
      } else {
        this.creating = true;
      }
    },
    data: function () {
      return {
        creating: true,
        saving: false,
        deleting: false,
        downloading: false,
        error: true,
        member: {
          id: '',
          member_id: '',
          email: '',
          title: '',
          firstname: '',
          lastname: '',
          gender: '',
          location: { id: '', name: '', email: 'default@default.default' },
          address: { id: '', street: '', zip: '', city: '', country: '' },
          newsletters: [],
          member_since: format_date(new Date()),
          membership_due: get_date_in_one_year(),
          created: '',
          changed: '',
          send: true,
          info: ''
        },
        newsletter: { name: '' },
        newsletters: [],
        delete_newsletters: []
      };
    },
    computed: {
      member_id: function () {
        return this.$route.query.member_id;
      },
      location_options() {
        return this.$store.getters.get_roles_as_select_options;
      },
      gender_options() {
        return this.$store.getters.get_genders;
      },
      newsletter_options() {
        return this.$store.getters.get_newsletters_as_options;
      }
    },
    methods: {
      add_newsletter() {
        for (let n of this.member.newsletters) {
          if (n.name === this.newsletter.name) {
            return;
          }
        }
        this.newsletters.push({ name: this.newsletter.name });
      },
      delete_newsletter(name) {
        for (let n of this.member.newsletters) {
          if (n.name === name) {
            this.delete_newsletters.push({ name: n.name });
            this.newsletters = this.newsletters.filter(function (element) {
              if (element.name == name) {
                return false;
              } else {
                return true;
              }
            }.bind(this));
            return;
          }
        }
      },
      create_member() {
        if (!this.error) {
          this.saving = true;

          let action = this.creating ? 'create_member' : 'update_member';

          let temp_member = Object.assign({}, this.member);

          temp_member.member_since =
            this.member.member_since + 'T00:00:00.000000';
          temp_member.membership_due =
            this.member.membership_due + 'T00:00:00.000000';

          this.$store.dispatch(action, temp_member)
            .then(function (response) {
              this.member.id = response.data.id;
              this.creating = false;


              this.$store.dispatch('download_members');
              this.$bvToast.toast(
                this.$i18n.t(
                  'texts.element_saved_successfully',
                  { element: this.member.lastname }),
                {
                  title: this.$i18n.t('texts.saved'),
                  variant: 'success',
                  autoHideDelay: 5000,
                  appendToast: true
                });

              this.$store
                .dispatch(
                  'add_newsletters_to_user',
                  { id: this.member.id, data: this.newsletters })
                .then(function (response) {
                  this.$store
                    .dispatch('delete_newsletters_from_user', {
                      id: this.member.id,
                      data: this.delete_newsletters
                    })
                    .then(function (response) {
                      this.$bvToast.toast(
                        this.$i18n.t(
                          'texts.element_saved_successfully',
                          { element: 'Newsletters' }),
                        {
                          title: this.$i18n.t('texts.saved'),
                          variant: 'success',
                          autoHideDelay: 5000,
                          appendToast: true
                        });
                      setTimeout(function () {
                        this.saving = false;
                        this.$router.push({ name: 'members' });
                      }.bind(this), 1000);
                    }.bind(this))
                    .catch(function (err) {
                      console.log(err);
                      this.$bvToast.toast(
                        this.$i18n.t(
                          'texts.element_not_saved',
                          { element: 'Newsletters' }),
                        {
                          title:
                            this.$i18n.t('texts.error_while_saving'),
                          autoHideDelay: 5000,
                          variant: 'danger',
                          appendToast: true
                        });
                    }.bind(this));
                }.bind(this))
                .catch(function (err) {
                  this.$bvToast.toast(
                    this.$i18n.t(
                      'texts.element_not_saved',
                      { element: 'Newsletters' }),
                    {
                      title: this.$i18n.t('texts.error_while_saving'),
                      autoHideDelay: 5000,
                      variant: 'danger',
                      appendToast: true
                    });
                }.bind(this));
            }.bind(this))
            .catch(function (err) {
              this.saving = false;
              this.$store.dispatch('download_members');

              if (err.message.indexOf("501") == -1) {
                this.$bvToast.toast(
                  this.$i18n.t(
                    'texts.element_not_saved',
                    { element: this.member.lastname }),
                  {
                    title: this.$i18n.t('texts.error_while_saving'),
                    autoHideDelay: 5000,
                    variant: 'danger',
                    appendToast: true
                  });
              } else {
                this.$bvToast.toast(
                  this.$i18n.t(
                    'texts.element_not_saved',
                    { element: this.member.lastname }),
                  {
                    title: this.$i18n.t('texts.duplicated_email'),
                    autoHideDelay: 5000,
                    variant: 'danger',
                    appendToast: true
                  });
              }
            }.bind(this));
        };
      },
      initiate_delete() {
        this.deleting = true;
      },
      cancel_delete() {
        this.deleting = false;
      },
      delete_member() {
        this.$store.dispatch('delete_member', this.member)
          .then(function (response) {
            this.$store.dispatch('download_members');
            this.$bvToast.toast(
              this.$i18n.t(
                'texts.element_deleted_successfully',
                { element: this.member.lastname }),
              {
                title: this.$i18n.t('texts.deleted'),
                variant: 'success',
                autoHideDelay: 5000,
                appendToast: true
              });

            setTimeout(function () {
              this.deleting = false;
              this.saving = false;
              this.$router.push({ name: 'members' });
            }.bind(this), 1000);
          }.bind(this))
          .catch(function (err) {
            console.log(err);
            this.deleting = false;
            this.saving = false;
            this.$store.dispatch('download_members');
            this.$bvToast.toast(
              this.$i18n.t(
                'texts.element_not_deleted',
                { element: this.members.lastname }),
              {
                title: this.$i18n.t('texts.error_while_deleting'),
                autoHideDelay: 5000,
                variant: 'danger',
                appendToast: true
              });
          }.bind(this));
      },
      validateState(error) {
        if (error) {
          this.error = false;
          return true;
        } else {
          this.error = true;
          return false;
        }
      },
      extend_membership() {
        this.$store.dispatch('extend_membership', { id: this.member.id })
          .then(function (response) {
            this.$store.dispatch('get_member', { id: this.member.id })
              .then(function (response) {
                if (response.status === 200) {
                  this.downloading = false;
                  this.member = response.data;

                  this.member.member_since =
                    format_date(new Date(response.data.member_since));
                  this.member.membership_due =
                    format_date(new Date(response.data.membership_due));

                  this.newsletters = response.data.newsletters == null ?
                    [] :
                    response.data.newsletters;
                }
              }.bind(this))
              .catch(function (err) {
                this.downloading = false;
                this.error = true;
              }.bind(this));


            this.$bvToast.toast(
              this.$i18n.t(
                'texts.membership_extended',
                { element: this.member.lastname }),
              {
                title: this.$i18n.t('texts.extended'),
                autoHideDelay: 5000,
                variant: 'success',
                appendToast: true
              });
          }.bind(this)).catch(function (error) {
            this.$bvToast.toast(
              this.$i18n.t(
                'texts.membership_not_extended',
                { element: this.member.lastname }),
              {
                title: this.$i18n.t('texts.not_extended'),
                autoHideDelay: 5000,
                variant: 'success',
                appendToast: true
              });
            console.log(error);
          }.bind(this));
      },
      resend_email() {
        this.$store.dispatch('resend_email', { id: this.member.id })
          .then(function (response) {
            this.$bvToast.toast(
              this.$i18n.t(
                'texts.success'),
              {
                title: this.$i18n.t('texts.success'),
                autoHideDelay: 5000,
                variant: 'success',
                appendToast: true
              });
          }.bind(this)).catch(function (error) {
            this.$bvToast.toast(
              this.$i18n.t(
                'texts.errors',
                { element: this.member.lastname }),
              {
                title: this.$i18n.t('texts.errors'),
                autoHideDelay: 5000,
                variant: 'danger',
                appendToast: true
              });
          }.bind(this));
      }
    },
    template: `
                    <div>
                        <b-card bg-variant="light" class="m-1">
                            <div class="text-center" v-if="downloading">
                                <b-spinner variant="success" type="grow"></b-spinner>
                            </div>
                            <b-form @submit.prevent="create_member">
                            <b-card bg-variant="light" class="m-1" v-if="!downloading">
                                <b-form-group 
                                label-cols-lg="3"
                                :label="$t('texts.location')"
                                label-size="lg"
                                label-class="font-weight-bold pt-0"
                                class="mb-0"
                                >
                                    <b-form-group
                                    label-cols-sm="3"
                                    :label="$t('texts.location')"
                                    label-align-sm="right"
                                    label-for="nested-location"
                                    >
                                        <ValidationProvider
                                        :rules="{ required: true }"
                                        v-slot="{ valid }"
                                        >
                                            <b-form-select 
                                            id="nested-location" 
                                            name="nested-location"
                                            v-model="member.location.name" 
                                            :options="location_options" 
                                            :readonly="saving"
                                            :state="validateState(valid || member.location.name != '')"
                                            aria-describedby="input-location-live-feedback"></b-form-select>
                                            <b-form-invalid-feedback id="input-location-live-feedback">
                                                {{ $t('texts.required') }}
                                            </b-form-invalid-feedback>
                                        </ValidationProvider>
                                    </b-form-group>
                                </b-form-group>
                            </b-card>

                            <b-card bg-variant="light" class="m-1">
                                <b-form-group
                                label-cols-lg="3"
                                :label="$t('texts.user')"
                                label-size="lg"
                                label-class="font-weight-bold pt-0"
                                class="mb-0"
                                >
                                    <b-form-group
                                    v-if="!creating"
                                    label-cols-sm="3"
                                    :label="$t('texts.member_id')"
                                    label-align-sm="right"
                                    label-for="nested-member-id"
                                    >
                                        <b-form-input id="nested-member-id" v-model="member.member_id" v-if="!creating" readonly></b-form-input>
                                    </b-form-group>

                                    <b-form-group
                                    label-cols-sm="3"
                                    :label="$t('texts.title')"
                                    label-align-sm="right"
                                    label-for="nested-title"
                                    >
                                        <b-form-input id="nested-title" v-model="member.title"></b-form-input>
                                    </b-form-group>

                                    <b-form-group
                                    label-cols-sm="3"
                                    :label="$t('texts.email')"
                                    label-align-sm="right"
                                    label-for="nested-email"
                                    >
                                        <ValidationProvider
                                        :rules="{ required: true, email: true }"
                                        v-slot="{ valid }"
                                        >
                                            <b-form-input 
                                            id="nested-email" 
                                            type="email" 
                                            v-model="member.email" 
                                            :state="validateState(valid)"
                                            aria-describedby="input-email-live-feedback"></b-form-input>
                                            <b-form-invalid-feedback id="input-email-live-feedback">
                                                {{ $t('texts.required_email') }}
                                            </b-form-invalid-feedback>
                                        </ValidationProvider>
                                    </b-form-group>

                                    <b-form-group
                                    label-cols-sm="3"
                                    :label="$t('texts.firstname')"
                                    label-align-sm="right"
                                    label-for="nested-firstname"
                                    >
                                        <ValidationProvider
                                        :rules="{ required: true }"
                                        v-slot="{ valid }"
                                        >
                                            <b-form-input 
                                            id="nested-firstname" 
                                            name="nested-firstname"
                                            v-model="member.firstname"
                                            :state="validateState(valid)"
                                            aria-describedby="input-firstname-live-feedback"></b-form-input>
                                            <b-form-invalid-feedback id="input-firstname-live-feedback">
                                                {{ $t('texts.required') }}
                                            </b-form-invalid-feedback>
                                        </ValidationProvider>
                                    </b-form-group>

                                    <b-form-group
                                    label-cols-sm="3"
                                    :label="$t('texts.lastname')"
                                    label-align-sm="right"
                                    label-for="nested-lastname"
                                    >
                                        <ValidationProvider
                                        :rules="{ required: true }"
                                        v-slot="{ valid }"
                                        >
                                            <b-form-input 
                                            id="nested-lastname" 
                                            v-model="member.lastname"
                                            :state="validateState(valid)"
                                            aria-describedby="input-lastname-live-feedback"></b-form-input>
                                            <b-form-invalid-feedback id="input-lastname-live-feedback">
                                                {{ $t('texts.required') }}
                                            </b-form-invalid-feedback>
                                        </ValidationProvider>
                                    </b-form-group>

                                    <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.gender')"
                                        label-align-sm="right"
                                        label-for="nested-gender"
                                        >
                                            <ValidationProvider
                                            :rules="{ required: true}"
                                            v-slot="{ valid }"
                                            >
                                                <b-form-select 
                                                id="nested-gender" 
                                                name="nested-gender"
                                                v-model="member.gender" 
                                                :options="gender_options"
                                                :state="validateState(valid)"
                                                aria-describedby="input-gender-live-feedback"></b-form-select>
                                                <b-form-invalid-feedback id="input-gender-live-feedback">
                                                    {{ $t('texts.required')  }}
                                                </b-form-invalid-feedback>
                                            </ValidationProvider>
                                        </b-form-group>

                                    <b-form-group
                                    label-cols-sm="3"
                                    :label="$t('texts.registered_since')"
                                    label-align-sm="right"
                                    label-for="nested-member-since"
                                    >
                                        <b-form-input id="nested-member-since" type="date" v-model="member.member_since" :readonly="!creating" ></b-form-input>
                                    </b-form-group>

                                    <b-form-group
                                    label-cols-sm="3"
                                    :label="$t('texts.valid_member_to')"
                                    label-align-sm="right"
                                    label-for="nested-valid-member-to"
                                    >
                                        <b-form-input id="nested-valid-member-to" type="date" v-model="member.membership_due" :readonly="!creating" ></b-form-input>
                                    </b-form-group>


                                </b-form-group>
                                <b-button-group class="float-right m-1" v-if="!creating">
                                    <b-button @click="resend_email" >{{ $t('texts.resend') }}</b-button>
                                    <b-button variant="success" @click="extend_membership" >{{ $t('texts.extend_membership') }}</b-button>
                                </b-button-group>
                            </b-card>

                            <b-card bg-variant="light" class="m-1">
                                <b-form-group
                                label-cols-lg="3"
                                :label="$t('texts.address')"
                                label-size="lg"
                                label-class="font-weight-bold pt-0"
                                class="mb-0"
                                >
                                    <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.street')"
                                        label-align-sm="right"
                                        label-for="nested-street"
                                    >
                                        <b-form-input id="nested-street" v-model="member.address.street" :readonly="saving"></b-form-input>
                                    </b-form-group>
                                
                                    <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.city')"
                                        label-align-sm="right"
                                        label-for="nested-city"
                                    >
                                        <b-form-input id="nested-city" v-model="member.address.city" :readonly="saving"></b-form-input>
                                    </b-form-group>
                                
                                    <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.zip')"
                                        label-align-sm="right"
                                        label-for="nested-zip"
                                    >
                                        <b-form-input id="nested-zip" v-model="member.address.zip" :readonly="saving" ></b-form-input>
                                    </b-form-group>
                                
                                    <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.country')"
                                        label-align-sm="right"
                                        label-for="nested-country"
                                    >
                                        <b-form-input id="nested-country" v-model="member.address.country" :readonly="saving" ></b-form-input>
                                    </b-form-group>
                            </b-form-group>
                        </b-card>
                        <b-card bg-variant="light" class="m-1" >
                            <b-list-group horizontal="md" class="flex-column align-items-start">
                                <b-list-group-item v-for="n in newsletters" :key="n.id" style="width: 100%;">
                                    <b-row>
                                        <b-button style="width: 100%;" @click.prevent="delete_newsletter(n.name)">
                                            {{ n.name }}
                                        </b-button>
                                    </b-row>
                                </b-list-group-item>
                                <b-list-group-item style="width: 100%;">
                                    <b-row>
                                        <b-col cols="10">
                                            <b-form-select 
                                            id="nested-newsletter" 
                                            name="nested-newsletter"
                                            v-model="newsletter.name" 
                                            :options="newsletter_options" ></b-form-select>
                                        </b-col>
                                        <b-col cols="2">
                                            <b-button variant="success" @click.prevent="add_newsletter" :disabled="this.newsletter.name === ''" style="width: 100%;">
                                                {{ $t('texts.add') }}
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </b-button>
                                        </b-col>
                                    </b-row>
                                </b-list-group-item>
                            </b-list-group>
                        </b-card>

                        <b-card bg-variant="light" class="m-1">
                                <b-form-group
                                label-cols-lg="3"
                                :label="$t('texts.info')"
                                label-size="lg"
                                label-class="font-weight-bold pt-0"
                                class="mb-0"
                                >
                          <b-form-input id="info" v-model="member.info" :readonly="saving"></b-form-input>
                        </b-card>

                    <b-button-group class="float-right m-1" v-if="!deleting">
                        <b-button variant="danger" @click.prevent="initiate_delete" >{{ $t('texts.delete') }}</b-button>
                        <b-button variant="success" type="submit">{{ $t('texts.save') }} </b-button>
                    </b-button-group>
                    <b-button-group class="float-right m-1" v-if="deleting" >
                        <b-button variant="danger" @click.prevent="cancel_delete" :disabled="saving">{{ $t('texts.no') }}</b-button>
                        <b-button variant="success" @click.prevent="delete_member" :disabled="saving">{{ $t('texts.yes') }}</b-button>
                    </b-button-group>
                    <span class="float-right m-1 vcenter" v-if="deleting" style="padding-top: 5px;">{{ $t('texts.you_sure') }}</span>

                    </b-form>
                    </b-card>
                    </div>
                    `
  }

  return members_edit;
});