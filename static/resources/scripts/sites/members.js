'use strict';

define(function (require, exports, module) {
    //let store = require('store');

    let members = require('members');
    let search = require('member_search');

    let dashboard_site = {
        created() {
            this.$store.dispatch('download_members');
            this.$store.dispatch('download_roles');
        },
        components: {
            'members': members,
            'search': search
        },
        data: function () {
            return {
                members_data: [],
            };
        },
        computed: {
            members() {
                return this.$store.getters.get_members;
            },
            columns() {
                let columns = [];
                columns.push({ value: 'gender', text: this.$i18n.t('texts.gender') });
                columns.push({ value: 'email', text: this.$i18n.t('texts.email') });
                columns.push({ value: 'name', text: this.$i18n.t('texts.name') });
                columns.push({ value: 'location', text: this.$i18n.t('texts.location') });
                columns.push({ value: 'valid_member_to', text: this.$i18n.t('texts.valid_member_to') });

                return columns;

            },
            computed_member_data() {
                this.members_data = this.$store.getters.get_members_data(this.selected);
            },
            data() {
                return this.members_data;
            }
        },
        methods: {
            search_filter(value) {
                let vm = this;
                vm.$store.dispatch('filter_members', value).then((result) => {
                    vm.members_data = result;
                });
            },
            create() {
                this.$router.push({ name: 'members_edit' });
            }
        },
        template: `
                    <div class="m-1">
                        <div class="row m-1">
                            <div class="col-sm-1">
                                <b-button variant="success" @click.prevent="create">{{ $t('texts.create') }}</b-button>
                            </div>
                            <div class="col-sm-11">
                                <search :columns="columns" @search_input="search_filter" default="name" :show_lastname="true"></search>
                            </div>
                        </div>
                        <members :properties="members" :data="data" :refresh="computed_member_data"></members>
                    </div>
                    `
    };

    return dashboard_site;
});