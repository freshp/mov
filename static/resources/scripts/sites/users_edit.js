'use strict';

define(function (require, exports, module) {
    let users_edit = {
        created() {
            this.$store.dispatch('download_roles');
            if (this.$route.query.user_id != null) {
                this.creating = false;
                this.downloading = true;
                this.$store.dispatch('get_user', { id: this.$route.query.user_id })
                    .then(function (response) {
                        if (response.status === 200) {
                            this.downloading = false;
                            this.user = response.data;
                        }
                    }.bind(this))
                    .catch(function (err) {
                        this.downloading = false;
                        this.error = true;
                    }.bind(this));
            } else {
                this.creating = true;
            }
        },
        data: function () {
            return {
                creating: true,
                saving: false,
                deleting: false,
                downloading: false,
                error: true,
                user: {
                    id: '',
                    email: '',
                    username: '',
                    title: '',
                    firstname: '',
                    lastname: '',
                    gender: '',
                    role: { id: '', name: '', email: 'bz@asus.sh' },
                    address: { id: '', street: '', zip: '', city: '', country: '' },
                    created: '',
                    changed: ''
                },
                password: '',
                repeat_password: ''
            };
        },
        computed: {
            user_id: function () {
                return this.$route.query.user_id;
            },
            roles_options() {
                return this.$store.getters.get_roles_as_select_options;
            },
            gender_options() {
                return this.$store.getters.get_genders;
            }
        },
        methods: {
            create_user() {
                if (!this.error) {
                    this.saving = true;

                    let action = this.creating ? 'create_user' : 'update_user';

                    let temp_user = this.user;
                    if(action == 'create_user') {
                        temp_user.password = this.password;
                    }

                    this.$store.dispatch(action, temp_user)
                        .then(function (response) {


                            this.$store.dispatch('download_users');
                            this.$bvToast.toast(this.$i18n.t('texts.element_saved_successfully', { element: this.user.username }), {
                                title: this.$i18n.t('texts.saved'),
                                variant: 'success',
                                autoHideDelay: 5000,
                                appendToast: true
                            });

                            setTimeout(function () {
                                this.saving = false;
                                this.$router.push({ name: 'users' });
                            }.bind(this), 1000);
                        }.bind(this)).catch(function (err) {
                            console.log(err);
                            this.saving = false;
                            this.$store.dispatch('download_users');
                            this.$bvToast.toast(this.$i18n.t('texts.element_not_saved', { element: this.user.username }), {
                                title: this.$i18n.t('texts.error_while_saving'),
                                autoHideDelay: 5000,
                                variant: 'danger',
                                appendToast: true
                            });
                        }.bind(this));
                };
            },
            initiate_delete() {
                this.deleting = true;
            },
            cancel_delete() {
                this.deleting = false;
            },
            delete_user() {
                this.$store.dispatch('delete_user', this.user)
                    .then(function (response) {
                        this.$store.dispatch('download_users');
                        this.$bvToast.toast(this.$i18n.t('texts.element_deleted_successfully', { element: this.user.username }), {
                            title: this.$i18n.t('texts.deleted'),
                            variant: 'success',
                            autoHideDelay: 5000,
                            appendToast: true
                        });

                        setTimeout(function () {
                            this.deleting = false;
                            this.saving = false;
                            this.$router.push({ name: 'users' });
                        }.bind(this), 1000);
                    }.bind(this)).catch(function (err) {
                        console.log(err);
                        this.deleting = false;
                        this.saving = false;
                        this.$store.dispatch('download_users');
                        this.$bvToast.toast(this.$i18n.t('texts.element_not_deleted', { element: this.user.username }), {
                            title: this.$i18n.t('texts.error_while_deleting'),
                            autoHideDelay: 5000,
                            variant: 'danger',
                            appendToast: true
                        });
                    }.bind(this));
            },
            validateState(error) {
                if (error) {
                    this.error = false;
                    return true;
                } else {
                    this.error = true;
                    return false;
                }
            },
            validateStatePassword(error) {
                if (error && this.password == this.repeat_password) {
                    this.error = false;
                    return true;
                } else {
                    this.error = true;
                    return false;
                }
            }
        },
        template: `
                    <div>
                        <b-card bg-variant="light" class="m-1">
                            <div class="text-center" v-if="downloading">
                                <b-spinner variant="success" type="grow"></b-spinner>
                            </div>
                            <b-form @submit.prevent="create_user">
                                <b-card bg-variant="light" class="m-1" v-if="!downloading">
                                    <b-form-group 
                                    label-cols-lg="3"
                                    :label="$t('texts.role')"
                                    label-size="lg"
                                    label-class="font-weight-bold pt-0"
                                    class="mb-0"
                                    >
                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.role')"
                                        label-align-sm="right"
                                        label-for="nested-roles"
                                        >
                                        <ValidationProvider
                                        :rules="{ required: true }"
                                        v-slot="{ valid }"
                                        >
                                            <b-form-select 
                                            id="nested-roles" 
                                            name="nested-roles"
                                            v-model="user.role.name" 
                                            :options="roles_options"
                                            :state="validateState(valid || user.role.name != '')"
                                            aria-describedby="input-role-live-feedback">
                                            </b-form-select>

                                            <b-form-invalid-feedback id="input-role-live-feedback">
                                                {{ $t('texts.required') }}
                                            </b-form-invalid-feedback>
                                        </ValidationProvider>
                                        </b-form-group>
                                    </b-form-group>
                                </b-card>

                                <b-card bg-variant="light" class="m-1">
                                    <b-form-group
                                    label-cols-lg="3"
                                    :label="$t('texts.user')"
                                    label-size="lg"
                                    label-class="font-weight-bold pt-0"
                                    class="mb-0"
                                    >

                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.email')"
                                        label-align-sm="right"
                                        label-for="nested-email"
                                        >
                                            <ValidationProvider
                                            :rules="{ required: true, email: true }"
                                            v-slot="{ valid }"
                                            >
                                                <b-form-input 
                                                id="nested-email" 
                                                name="nested-email"
                                                v-model="user.email"
                                                :state="validateState(valid)"
                                                aria-describedby="input-email-live-feedback"
                                                ></b-form-input>
                                                <b-form-invalid-feedback id="input-email-live-feedback">
                                                    {{ $t('texts.required_email')  }}
                                                </b-form-invalid-feedback>
                                            </ValidationProvider>
                                        </b-form-group>

                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.username')"
                                        label-align-sm="right"
                                        label-for="nested-username"
                                        >
                                            <ValidationProvider
                                            :rules="{ required: true}"
                                            v-slot="{ valid }"
                                            >
                                                <b-form-input 
                                                id="nested-username" 
                                                name="nested-username"
                                                v-model="user.username"
                                                :state="validateState(valid)"
                                                aria-describedby="input-username-live-feedback"
                                                ></b-form-input>
                                                <b-form-invalid-feedback id="input-username-live-feedback">
                                                    {{ $t('texts.required')  }}
                                                </b-form-invalid-feedback>
                                            </ValidationProvider>
                                        </b-form-group>

                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.firstname')"
                                        label-align-sm="right"
                                        label-for="nested-firstname"
                                        >
                                            <ValidationProvider
                                            :rules="{ required: true}"
                                            v-slot="{ valid }"
                                            >
                                                <b-form-input 
                                                id="nested-firstname"
                                                name="nested-firstname" 
                                                v-model="user.firstname"
                                                :state="validateState(valid)"
                                                aria-describedby="input-firstname-live-feedback"
                                                ></b-form-input>
                                                <b-form-invalid-feedback id="input-firstname-live-feedback">
                                                    {{ $t('texts.required')  }}
                                                </b-form-invalid-feedback>
                                            </ValidationProvider>
                                        </b-form-group>

                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.lastname')"
                                        label-align-sm="right"
                                        label-for="nested-lastname"
                                        >
                                            <ValidationProvider
                                            :rules="{ required: true}"
                                            v-slot="{ valid }"
                                            >
                                                <b-form-input 
                                                id="nested-lastname" 
                                                name="nested-lastname"
                                                v-model="user.lastname"
                                                :state="validateState(valid)"
                                                aria-describedby="input-lastname-live-feedback" ></b-form-input>
                                                <b-form-invalid-feedback id="input-lastname-live-feedback">
                                                    {{ $t('texts.required')  }}
                                                </b-form-invalid-feedback>
                                            </ValidationProvider>
                                        </b-form-group>

                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.title')"
                                        label-align-sm="right"
                                        label-for="nested-title"
                                        >
                                            <b-form-input id="nested-title" v-model="user.title" ></b-form-input>
                                        </b-form-group>

                                        <b-form-group
                                        label-cols-sm="3"
                                        :label="$t('texts.gender')"
                                        label-align-sm="right"
                                        label-for="nested-gender"
                                        >
                                            <ValidationProvider
                                            :rules="{ required: true}"
                                            v-slot="{ valid }"
                                            >
                                                <b-form-select 
                                                id="nested-gender" 
                                                name="nested-gender"
                                                v-model="user.gender" 
                                                :options="gender_options"
                                                :state="validateState(valid)"
                                                aria-describedby="input-gender-live-feedback"></b-form-select>
                                                <b-form-invalid-feedback id="input-gender-live-feedback">
                                                    {{ $t('texts.required')  }}
                                                </b-form-invalid-feedback>
                                            </ValidationProvider>
                                        </b-form-group>

                                        <b-form-group
                                        v-if="creating"
                                        label-cols-sm="3"
                                        :label="$t('texts.password')"
                                        label-align-sm="right"
                                        label-for="nested-password"
                                        >
                                            <ValidationProvider
                                            :rules="{ required: true}"
                                            v-slot="{ valid }"
                                            >
                                                <b-form-input 
                                                id="nested-password" 
                                                name="nested-password"
                                                type="password" 
                                                v-model="password" 
                                                :state="validateState(valid)"
                                                aria-describedby="input-password-live-feedback"></b-form-input>
                                                <b-form-invalid-feedback id="input-password-live-feedback">
                                                {{ $t('texts.required')  }}
                                            </b-form-invalid-feedback>
                                        </ValidationProvider>
                                        </b-form-group>

                                        <b-form-group
                                        v-if="creating"
                                        label-cols-sm="3"
                                        :label="$t('texts.repeat_password')"
                                        label-align-sm="right"
                                        label-for="nested-repeat-password"
                                        >
                                            <ValidationProvider
                                            :rules="{ required: true}"
                                            v-slot="{ valid }"
                                            >
                                                <b-form-input 
                                                id="nested-repeat-password" 
                                                name="nested-repeat-password"
                                                type="password" 
                                                v-model="repeat_password" 
                                                :state="validateStatePassword(valid)"
                                                aria-describedby="input-repeat-password-live-feedback"></b-form-input>
                                                <b-form-invalid-feedback id="input-repeat-password-live-feedback">
                                                    {{ $t('texts.required_and_equal_password')  }}
                                                </b-form-invalid-feedback>
                                            </ValidationProvider>
                                        </b-form-group>
                                    </b-form-group>
                                </b-card>

                                <b-card bg-variant="light" class="m-1">
                                    <b-form-group
                                    label-cols-lg="3"
                                    :label="$t('texts.address')"
                                    label-size="lg"
                                    label-class="font-weight-bold pt-0"
                                    class="mb-0"
                                    >
                                        <b-form-group
                                            label-cols-sm="3"
                                            :label="$t('texts.street')"
                                            label-align-sm="right"
                                            label-for="nested-street"
                                        >
                                            <b-form-input id="nested-street" v-model="user.address.street"></b-form-input>
                                        </b-form-group>
                                    
                                        <b-form-group
                                            label-cols-sm="3"
                                            :label="$t('texts.city')"
                                            label-align-sm="right"
                                            label-for="nested-city"
                                        >
                                            <b-form-input id="nested-city" v-model="user.address.city" ></b-form-input>
                                        </b-form-group>
                                    
                                        <b-form-group
                                            label-cols-sm="3"
                                            :label="$t('texts.zip')"
                                            label-align-sm="right"
                                            label-for="nested-zip"
                                        >
                                            <b-form-input id="nested-zip" v-model="user.address.zip"></b-form-input>
                                        </b-form-group>
                                    
                                        <b-form-group
                                            label-cols-sm="3"
                                            :label="$t('texts.country')"
                                            label-align-sm="right"
                                            label-for="nested-country"
                                        >
                                            <b-form-input id="nested-country" v-model="user.address.country"></b-form-input>
                                        </b-form-group>
                                    </b-form-group>
                                </b-card>
                                <b-button-group class="float-right m-1" v-if="!deleting">
                                    <b-button variant="danger" @click.prevent="initiate_delete" v-if="!creating" :disabled="saving">{{ $t('texts.delete') }}</b-button>
                                    <b-button variant="success" type="submit">
                                        {{ $t('texts.save') }}
                                    </b-button>
                                </b-button-group>
                                <b-button-group class="float-right m-1" v-if="deleting" >
                                    <b-button variant="danger" @click.prevent="cancel_delete" :disabled="saving">{{ $t('texts.no') }}</b-button>
                                    <b-button variant="success" @click.prevent="delete_user" :disabled="saving">{{ $t('texts.yes') }}</b-button>
                                </b-button-group>
                                <span class="float-right m-1 vcenter" v-if="deleting" style="padding-top: 5px;">{{ $t('texts.you_sure') }}</span>
                            </b-form>
                        </b-card>
                    </div>`
    };

    return users_edit;
});