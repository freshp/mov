define([], function(require, factory) {
    'use strict';
    
    return function (file_text) {
        let lines = file_text.split('\n');

        let result = [];

        lines.forEach(function (element) {
            let columns = element.split(';');

            let member = {
                firstname: columns[0],
                lastname: columns[1],
                email: columns[2],
                gender: columns[3],
                membership_since: columns[4],
                membership_due: columns[5],
                address: {
                    name: columns[6]
                },
                location: {
                    name: columns[7]
                }
            };

            result.push(member);
        });

        return result;
    };
});