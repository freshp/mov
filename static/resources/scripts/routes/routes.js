define(function (require, exports, module) {
    let login_site = require('login_site');
    let dashboard = require('dashboard_site');
    let members = require('members_site');
    let members_edit = require('members_edit_site');
    let users = require('users_site');
    let users_edit = require('users_edit_site');
    let newsletter = require('newsletter_site');
    let newsletter_edit = require('newsletter_edit_site');
    let roles = require('roles_site');
    let roles_edit = require('roles_edit_site');
    let scan = require('scan_site');
    let settings = require('settings_site');

    let not_found = require('not_found');
    let not_authorized = require('not_authorized');

    const routes = [
        { path: '/', redirect: '/login' },

        { path: '/login', name: 'login', component: login_site, meta: { requires_auth: false, roles: [] } },
        { path: '/dashboard', name: 'dashboard', component: dashboard, meta: { requires_auth: true, roles: [] } },
        { path: '/members', name: 'members', component: members, meta: { requires_auth: true, roles: [] } },
        { path: '/edit/members', name: 'members_edit', component: members_edit, meta: { requires_auth: true, roles: [] } },
        { path: '/users', name: 'users', component: users, meta: { requires_auth: true, roles: ["admin"] } },
        { path: '/edit/users', name: 'users_edit', component: users_edit, meta: { requires_auth: true, roles: ["admin"] } },
        { path: '/newsletters', name: 'newsletters', component: newsletter, meta: { requires_auth: true, roles: [] } },
        { path: '/edit/newsletters', name: 'newsletters_edit', component: newsletter_edit, meta: { requires_auth: true, roles: [] } },
        { path: '/roles', name: 'roles', component: roles, meta: { requires_auth: false, roles: [] } },
        { path: '/edit/roles', name: 'roles_edit', component: roles_edit, meta: { requires_auth: true, roles: [] } },
        { path: '/scan', name: 'scan', component: scan, meta: { requires_auth: true, roles: [] } },
        { path: '/settings', name: 'settings', component: settings, meta: { requires_auth: true, roles: [] } },
        { path: '/not_found', name: 'not_found', component: not_found, meta: { requires_auth: false } },
        { path: '/not_authorized', name: 'not_authorized', component: not_authorized, meta: { requires_auth: false } },

        //has to be last
        { path: '*', redirect: '/not_found' }

    ];

    return routes;
});