define(function (require, exports, module) {
    let nav_element = {
        props: ['href', 'text'],
        methods: {
            link_click(e) {
                this.$emit('link_click', e);
            }
        },
        template: `
                    <li>
                        <a class="nav-link" @click="link_click" :href="href">{{text}}</a>
                    </li>
                    `
    };

    let navigation_bar = {
        components: {
            'nav-element': nav_element
        },
        computed: {
            show_tab: function() {
                return this.$store.getters.get_role == 'admin';
            }
        },
        methods: {
            logout() {
                this.$store.dispatch('logout').then(function(r) {
                    this.$router.push('login');
                }.bind(this)).catch(function (error) {
                    console.log(error)
                });
            }
        },
        template: `
                        <div>
                            <b-navbar toggleable="sm" type="light" variant="light">
                                <b-navbar-brand href="#/dashboard" >{{ $t('texts.dashboard') }}</b-navbar-brand>
                                <b-navbar-toggle target="nav-collapse"></b-navbar-toggle>
                                <b-collapse id="nav-collapse" is-nav>
                                    <b-navbar-nav>
                                        <nav-element href="#/members" :text="$t('texts.members')" />
                                        <nav-element href="#/users" :text="$t('texts.users')" v-if="show_tab"/>
                                        <nav-element href="#/newsletters" :text="$t('texts.newsletter')" />
                                        <nav-element href="#/roles" :text="$t('texts.roles')" />
                                        <nav-element href="#/scan" :text="$t('texts.scan')" />
                                        <nav-element href="#/settings" :text="$t('texts.settings')"></nav-element>
                                        <nav-element href="#/login" :text="$t('texts.logout')" @link_click.prevent="logout"></nav-element>
                                    </b-navbar-nav>
                                </b-collapse>
                            </b-navbar>
                        </div>
                    `
    };

    return navigation_bar;
});