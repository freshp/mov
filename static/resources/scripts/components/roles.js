define(function (require, exports, module) {
    let container = {
        props: ['properties', 'data'],
        data: function () {
            return {
                perPage: 20,
                currentPage: 1,
                sortBy: 'id',
                sortDesc: false,
            };
        },
        computed: {
            rows() {
                return this.$props.data.length;
            },
            get_data() {
                return this.$props.data;
            },
            get_table_header() {
                return this.$props.properties.table_header;
            },
            get_fields() {
                return this.$props.properties.fields;
            }
        },
        methods: {
            row_clicked: function(item , index) {
                this.$router.push({ name: 'roles_edit', query: {
                    role_id: item.id
                }});
            }
        },
        template:   `
                    <div class="container-fluid">
                        <div class="overflow-auto">
                                
                            <b-table
                                id="my-table"

                                :hover="true"
                                :responsive="true"

                                :items="get_data"

                                :fields="get_fields"

                                :per-page="perPage"
                                :current-page="currentPage"

                                
                                :sort-by.sync="sortBy"
                                :sort-desc.sync="sortDesc"

                                @row-clicked="row_clicked"

                                small
                            >
                                <!-- A custom formatted header cell for field 'name' -->
                                <template v-slot:head(id)="data">
                                    <em>{{ $t('texts.id') }}</em>
                                </template>

                                <template v-slot:head(name)="data">
                                    <em>{{ $t('texts.name') }}</em>
                                </template>

                            </b-table>

                            <b-pagination
                                align="center"
                                class="pagination-centered"
                                v-model="currentPage"
                                :total-rows="rows"
                                :per-page="perPage"
                                aria-controls="my-table"
                            ></b-pagination>
                         </div>
                     </div>
                    `
    };

    return container;
});