'use strict';
define(function (require, exports, module) {
    let search_component = {
        props: ['columns', 'default', 'show_lastname'],
        data: function () {
            return {
                selected:  this.$props.default,
                search_term: '',
                second_term: '',
                current_timeout: null
            };
        },
        methods: {
            key_up: function () {

                if (this.current_timeout != null) {
                    clearTimeout(this.current_timeout);
                    this.current_timeout = null;
                } 
                //wait a second before sending search event
                this.current_timeout = setTimeout(function () {
                    this.$emit('search_input', {
                        sec_term: this.second_term,
                        selected: this.selected,
                        term: this.search_term,
                    });
                    this.current_timeout = null;
                }.bind(this), 700);

            },
            get_first_name_placeholder: function (selected) {
                if (selected == 'name' && this.$props.show_lastname) {
                    return this.$i18n.t('texts.firstname');
                } else {
                    return '';
                }
            },
            get_last_name_placeholder: function (selected) {
                if (selected == 'name') {
                    return this.$i18n.t('texts.lastname');
                } else {
                    return '';
                }
            }
        },
        template: `
                    <div>
                        <b-input-group>
                            <b-input-group-prepend>
                                <b-form-select v-model="selected" :options="columns"></b-form-select>
                            </b-input-group-prepend>

                            <b-form-input type="text" @keyup="key_up" v-model="search_term" :placeholder="get_first_name_placeholder(selected)"></b-form-input>

                            <b-form-input v-if="selected == 'name' && show_lastname" type="text" @keyup="key_up" v-model="second_term" :placeholder="get_last_name_placeholder(selected)" ></b-form-input>
                        </b-input-group>
                    </div>
                    `
    };

    return search_component;
});