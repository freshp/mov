define(function(require, exports, module) {
    let container = {
        props: ['properties', 'data'],
        data: function() {
            return {
                perPage: 20,
                currentPage: 1,
                sortBy: 'membership_due',
                sortDesc: true,
            };
        },
        computed: {
            rows() {
                return this.$props.data.length;
            },
            get_data() {
                return this.$props.data.map(function (element) {
                    let temp = element;

                    if (new Date(temp.membership_due) < new Date()) {
                        temp._rowVariant = 'danger';
                    }

                    return Object.freeze(temp);
                })
            },
            get_table_header() {
                return this.$props.properties.table_header;
            },
            get_fields() {
                return this.$props.properties.fields;
            }
        },
        methods: {
            row_clicked: function(item, index) {
                this.$router.push({
                    name: 'members_edit',
                    query: {
                        member_id: item.id
                    }
                });
            }
        },
        template: `
                    <div class="container-fluid">
                        <div class="overflow-auto">
                        
                            <b-table
                                id="my-table"

                                :hover="true"
                                :responsive="true"

                                :items="get_data"

                                :fields="get_fields"

                                :per-page="perPage"
                                :current-page="currentPage"

                                
                                :sort-by.sync="sortBy"
                                :sort-desc.sync="sortDesc"

                                @row-clicked="row_clicked"

                                small
                            >
                                <template v-slot:head(gender)="data">
                                    <em>{{ $t('texts.gender') }}</em>
                                </template>

                                <template v-slot:head(email)="data">
                                    <em>{{ $t('texts.email') }}</em>
                                </template>

                                <template v-slot:head(firstname)="data">
                                    <em>{{ $t('texts.firstname') }}</em>
                                </template>

                                <template v-slot:head(lastname)="data">
                                    <em>{{ $t('texts.lastname') }}</em>
                                </template>

                                <template v-slot:head(location.name)="data">
                                    <em>{{ $t('texts.location') }}</em>
                                </template>

                                <template v-slot:head(membership_due)="data">
                                    <em>{{ $t('texts.valid_member_to')}}</em>
                                </template>

                                <template v-slot:cell(membership_due)="data">
                                    {{ (new Date(data.value).toLocaleDateString()) }}
                                </template>

                            </b-table>

                            <b-pagination
                                align="center"
                                class="pagination-centered"
                                v-model="currentPage"
                                :total-rows="rows"
                                :per-page="perPage"
                                aria-controls="my-table"
                            ></b-pagination>
                        </div>
                    </div>
                    `
    };

    return container;
});