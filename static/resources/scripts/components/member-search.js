'use strict';
define(function (require, exports, module) {
    let search_component = {
        props: ['columns', 'default', 'show_lastname'],
        created: function () {
            this.$store.dispatch('download_roles');
        },
        data: function () {
            return {
                selected: this.$props.default,
                selected_show: 0,
                selected_location: null,
                search_term: '',
                second_term: '',
                current_timeout: null
            };
        },
        computed: {
            get_locations: function () {
                let temp = this.$store.getters.get_roles.data.map(function (element) {
                    return {
                        value: element.id,
                        text: element.name,
                    };
                });

                temp.push({
                    value: null,
                    text: this.$i18n.t('texts.select_location')
                });
                return temp;
            }
        },
        methods: {
            key_up: function () {

                if (this.current_timeout != null) {
                    clearTimeout(this.current_timeout);
                    this.current_timeout = null;
                }
                //wait a second before sending search event
                this.current_timeout = setTimeout(function () {
                    this.$emit('search_input', {
                        sec_term: this.second_term,
                        selected: this.selected,
                        location: this.selected_location,
                        term: this.search_term,
                        show_members: this.selected_show
                    });
                    this.current_timeout = null;
                }.bind(this), 700);

            },
            get_first_name_placeholder: function (selected) {
                if (selected == 'name' && this.$props.show_lastname) {
                    return this.$i18n.t('texts.firstname');
                } else {
                    return '';
                }
            },
            get_last_name_placeholder: function (selected) {
                if (selected == 'name') {
                    return this.$i18n.t('texts.lastname');
                } else {
                    return '';
                }
            },
        },
        template: `
                    <div>
                        <b-input-group>
                            <b-input-group-prepend>
                                <b-form-select v-model="selected" :options="columns"></b-form-select>
                            </b-input-group-prepend>

                            <b-form-input type="text" @keyup="key_up" v-model="search_term" :placeholder="get_first_name_placeholder(selected)"></b-form-input>
                            <b-form-input v-if="selected == 'name' && show_lastname" type="text" @keyup="key_up" v-model="second_term" :placeholder="get_last_name_placeholder(selected)" ></b-form-input>
                            <b-form-select v-model="selected_location" :options="get_locations" @change="key_up"></b-form-select>

                        </b-input-group>
                        <b-form-group>
                            <b-form-radio-group  v-model="selected_show" @input="key_up" name="radio-sub-component">
                                <b-form-radio name="some-radios" :value="0">{{$t('texts.show_all_members')}}</b-form-radio>
                                <b-form-radio name="some-radios" :value="1">{{$t('texts.members_only')}}</b-form-radio>
                                <b-form-radio name="some-radios" :value="2">{{$t('texts.not_active_members_only')}}</b-form-radio>
                            </b-form-radion-group>
                        </b-form-group>
                    </div>
                    `
    };

    return search_component;
});