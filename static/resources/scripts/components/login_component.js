'use strict';
define(function (require, exports, module) {
    let axios = require('axios');

    let icon = {
        props: ['icon'],
        template: `
                <div class="fadeIn first">
                    <img :src="icon" id="icon" alt="icon" src="/resources/images/logo.svg" />
                </div>       
        `
    };

    let username = {
        props: ['properties'],
        data: function() {
            return {
                username: ''
            };
        },
        methods: {
            keyup () {
                this.$emit('keyup', this.username);
            }
        },
        template:   `<div>
                        <label for="username-input" >{{ $t('texts.username') }}</label>
                        <input id="username-input" v-model="username" @keyup="keyup" type="text" class="form-control" name="login" :placeholder="properties.placeholder" />
                    </div>`
    };

    let password = {
        props: ['properties'],
        data: function() {
            return {
                password: ''
            };
        },
        methods: {
            keyup() {
                this.$emit('keyup', this.password);
            }
        },
        template: ` <div>
                        <label for="password-input">{{ $t('texts.password') }}</label>
                        <input id="password-input" v-model="password" @keyup="keyup" type="password" class="form-control" name="login" :placeholder="properties.placeholder" />
                    </div>`
    };

    let submit = {
        props: {
            show: {
                type: Boolean,
                default: false
            }
        },
        template: `<b-button type="submit" class="btn btn-primary"><b-spinner v-if="show" small type="grow"></b-spinner>{{ $t('texts.login') }}</b-button>`
    }

    let form = {
        props: ['properties'],
        components: {
            'username': username,
            'password': password,
            'submit': submit
        },
        data: function() {
            return {
                loggin_in: false,
                username: '',
                password: ''
            };
        },
        methods: {
            set_username (username) {
                this.username = username;
            },
            set_password (password) {
                this.password = password;
            },
            login() {
                let payload = {
                    username: this.username,
                    password: this.password
                };

                this.loggin_in = true;
                axios.post('/login', payload).then((response) => {
                    if(response.status === 200) {
                        this.$store.commit('set_token', response.data.token);
                        this.$store.commit('set_role', response.data.role);
                        this.$store.commit('set_logged_in', true);
                        this.$router.push("dashboard");
                    } else {
                        this.$store.commit('set_logged_in', false);
                        
                        this.$bvToast.toast(this.$i18n.t('texts.could_not_log_in'), {
                            title: this.$i18n.t('texts.error_logging_in'),
                            autoHideDelay: 5000,
                            variant: 'danger',
                            appendToast: true
                        });
                    }
                    this.loggin_in = false;
                }).catch((error) => {
                    console.log(error);
                    this.loggin_in = false;

                    this.$bvToast.toast(this.$i18n.t('texts.could_not_log_in'), {
                        title: this.$i18n.t('texts.error_logging_in'),
                        autoHideDelay: 5000,
                        variant: 'danger',
                        appendToast: true
                    });
                });
            }
        },
        template: `
                <div class="container-fluid card p-3">
                    <form @submit.prevent="login" >
                        <div class="form-group">
                            <username :properties="properties.username" @keyup="set_username"/>
                        </div>
                        <div class="form-group">
                            <password :properties="properties.password" @keyup="set_password" />
                        </div>
                        <submit :show="loggin_in"/>
                    </form>           
                </div>
        `
    };

    let login =  {
        props: ['properties'],
        components: {
            'custom-form': form,
            'icon': icon
        },
        template: `
                <div class="wrapper fadeInDown">
                    <div class="text-center">
                        <icon class="w-25 h-25 m-auto" />
                    </div>
                    <custom-form :properties="properties" />
                </div>
        `
    };

    return login;

});

let a = `
  <!-- Remind Passowrd -->
  <div id="formFooter">
    <a class="underlineHover" href="#">Forgot Password?</a>
  </div>
`