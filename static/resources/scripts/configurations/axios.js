'use strict';

define(function (require, exports, module) {
    return function (token) {
        return {
            headers: { 'Authorization': "bearer " + token }
        };
    }
});