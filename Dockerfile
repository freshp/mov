FROM ubuntu:18.04
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    ca-certificates curl file \
    build-essential \
    autoconf automake autotools-dev libtool xutils-dev \
    libssl-dev \
    gcc-multilib \
    wget gnupg libssl-dev apt-utils openssl pkg-config curl
#    apt-utils libmysqlclient-dev mysql-client libmysqlcppconn-dev 

RUN touch /etc/apt/sources.list.d/pgdg.list
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main" > /etc/apt/sources.list.d/pgdg.list
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
RUN apt-get update
RUN apt-get install --no-install-recommends -y postgresql-client-11 libpq-dev postgresql-server-dev-11

# install toolchain
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | \
    sh -s -- --default-toolchain nightly -y

ENV PATH=/root/.cargo/bin:$PATH

# install nodejs and package
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash
RUN apt-get install  --no-install-recommends -y nodejs
RUN apt-get update 
RUN npm install apple_wallet_cmd -g

ARG MYSQL_ROOT_PASSWORD
ARG MYSQL_DATABASE
ARG DATABASE_URL
ARG ROCKET_DATABASES
ARG PROPERTIES_FOLDER
ARG WALLET_PASSWORD

ENV MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD
ENV MYSQL_DATABASE=$MYSQL_DATABASE
ENV DATABASE_URL=$DATABASE_URL
ENV ROCKET_DATABASES=$ROCKET_DATABASES
ENV PROPERTIES_FOLDER=$PROPERTIES_FOLDER
ENV WALLET_PASSWORD=$WALLET_PASSWORD

RUN cargo install cargo-watch
RUN cargo install catflap
RUN cargo install diesel_cli --no-default-features --features postgres
RUN mkdir -p /code
COPY . /code

RUN mkdir -p /etc/mov/wallet
COPY ./wallet/ /etc/mov/wallet/

RUN mkdir -p /tmp/

WORKDIR /code
RUN mkdir -p /release

EXPOSE 8000

CMD ["/bin/bash", "-c", "sleep 1m; (diesel migration run && cargo run --release)"]

